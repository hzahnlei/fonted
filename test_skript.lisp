(def! font1 (load_font! "fonts/holgers_serif_cp850_08x16.fonted"))
(def! font2 (load_font! "fonts/holgers_sans_serif_cp850_08x16.fonted"))
(def! font3 (load_font! "fonts/holgers_computer_cp850_08x16.fonted"))
(def! font4 (load_font! "fonts/holgers_template_cp850_08x16.fonted"))

(map font1 invert_glyph!) | (count)
font2 | (filter ascii_num?) | (map invert_glyph!) | (count)
font3 | (filter ascii_lower_alpha?) | (map flip_glyph_horizontally!) | (count)
font4 | (filter ascii_upper_alpha?) | (map scroll_glyph_right!) | (count)

(save_font! font1 "font1.fonted")
(save_font! font2 "font2.fonted")
(save_font! font3 "font3.fonted")
(save_font! font4 "font4.fonted")

;; Fonts can be  merged into one new font.  But all fonts to be merged  need to
;; have the same glyph height and width.
(def! x (load_font! "fonts/holgers_serif_cp850_08x16.fonted"))
(def! y (load_font! "fonts/holgers_sans_serif_cp850_08x16.fonted"))
(def! z (merged_font x y))
(save_font! z "merged.fonted")
(export_binary_font! z "merged") ;; Automatically adds ".bin" suffix.

;; Create a new font and populate with glyphs by copying from other font.
(def! s (load_font! "fonts/holgers_computer_cp850_08x16.fonted"))
(def! d (new_font glyph_count: 128 glyph_width: 8 glyph_height: 16
                  name: "Computer Derived" author: "Holger Zahnleiter"
		  codepage: "ASCII" description: "Computer font" ))
(map (range 32 126) (fun i (copy_glyph! s,i d,i)))
(save_font! d "computer_derived.fonted")
