# fonted - Bitmap Font Editor

Create your own bitmap fonts for vintage computers.

2023-09-14, Holger Zahnleiter

## Introduction

For one of my projects (TODO link to serial terminal) I needed a 8×16 bitmap font.
I was searching the web for bitmap font editors but was not really satisfied with what I found.
Some where available on Windows only.
Some where available on Linux and macOS but coud only do 8×8 fonts and so on.

So, I decided to built my own bitmap font editor.
It is supposed to support different glyph sizes.
It should be multi-platform and open source - of cause.

I was first thinking about a portable console application built with C++/Ncurses or Go/Bubble Tea.
However, I need to display pixel perfect renderings of the font in the making.
Therefore I reasoned that I would need a graphical library.
Dear ImGui was on my list for some time.
I finally have an excuse for using ImGui for my first time.

## Building from Source

The font editor can easily built from the source code contained herein.
This requires the following tools:

* A modern C++ compiler, that supports C++ 20 and onwards.
* CMake, version 3.20 or newer.
* Conan, 2.0.10 or newer.
* GNU Make 3.81 or newer.

On the shell run `make dependencies`.
This will instruct Conan to download the libraries which the font editor is depending on.
(For example, ImGui, Catch2 etc.)

Then execute `make build`.
This should build the executable, if CMake and Clang/GCC are set up properly.
The binary will be written to `./build/Debug/src`.

You can execute the binary by running `make run`.
This will launch the font editor with a blank font.
If you pass a name for a font file, then the editor will load and display that file: `./build/Debug/src/fonted <file name>`.

You can have the bounding box of the custom widgets drawn by defining `FONTED_DEBUG_DRAWING` for the build.

You can also make a **release build** without debug symbols by typing `make release-build`.
The binary will be written to `./build/Release/src`.
For running it type `./build/Release/src/fonted`.

## User's Guide

### Command Line Use

Assuming the Fonted executable (release or debug, it's up to you) is on the `PATH`, then it can be launched by typing `fonted`.
The editor window will open with a blank font of a default dimension of 256 glyphs where each glyph is 8 pixels wide and 16 heigh.

Or, execute `fonted new --width <width> --height <height> --count <count>` to open the editor on a blank font with custom dimensions.

You can also invoke `fonted <filename>` to open the editor on an existing font file.

Just run `fonted help` to see all the commands and options supported on command line.
(For example, `fonted` can be used to transform font file formats.
Albeit, currently only Fonted's native and binary formats are supported.)

You can run `fonted repl` this will open a scripting console that allows you to edit fonts with a scripting language.
For example you may want to scroll all glyphs of a font to the right.
Doing this manually in the editor is tedious, but easy with a script.
See details on the scripting language below.

### GUI Use

The editor consist of one single window only.
There are no menus, wizards etc.
There is not much to it.
Fonted is a very simple bitmap font editor.

These are the elements of the Fonted editor:

- The glyph picker: Pick a glyph for editing by left-clicking with the mouse.
  The picked glyph becomes the "active" glyph.
  Use `CTRL-C` and `CTRL-V` for copy/paste the active glyph.
- The glyph editor: Edit the "active" glyph by left-clicking pixels in the glyph editor.
  Clicking pixels does toggle them.
- Use the scoll controls (mouse click) to scroll the active glyph in the cardinal directions.
- Use the other buttons (mouse click) to flip the active glyph horizontally/vertically.
  You may also invert all pixels, clear all pixels or fill all pixels.
- Click on "load" or "save" to load an existing font or to save the font.
  You may also press `F1` or `F2` respectively instead of clicking the respective button.

<img src="pictures/main_screen.png" width="100%" alt="Main Editor Window" />

At the bottom of the editor window you find a text field for putting in scripting commands or small programs even.
Press the "Execute" botton to run you commands.
You can observe the effect on the glyphs on screen.
See details on the scripting language below.

Here is an example for a font that was loaded by a scripting command.
See the command details at the bottom of the screen shot.

<img src="pictures/font_loaded_by_script.png" width="100%" alt="Font loaded by script" />

Here is another example of some characters inverted under script control.

<img src="pictures/font_inverted_by_script.png" width="100%" alt="Font loaded by script" />

## Scripting Language

The scripting language is based on Lisp with some syntactic sugar.
For details on the scripting engine and language see my other project [app/SL](https://gitlab.com/hzahnlei/appsl).

Here are the extensions to the underlying scripting language for use with Fonted:

- When running from command line (**headless**) and within the editor (**GUI**)
  - `new_font` creates a new, empty font to a certain specification.
    ```lisp
    (def! my_font (new_font glyph_count: 256 glyph_width: 8 glyph_height: 12
                            name: "Robot" author: "Pete Park"
                            codepage: "CP850" description: "Pretty font" ))
    ```
    You do not have to use named parameters though.
    This will achieve the same but maybe in a less sef-explanatory way:
    ```lisp
    (def! x (new_font 256 8 16 "Test" "Holger Zahnleiter" "ASCII" "Font for testing"))
    (save_font! x "test.fonted")
    ```
    Now run Fontend and see an empty font: `fonted test.fonted`
  - `load_font!` loads a native Fonted font file.
     The syntax is like this `(load_font! <name>)`, where `<name>` is the name of the file to be loaded.
     For example try:
     ```
     (load_font! "fonts/holgers_serif_cp850_08x16.fonted")
     ```
     To keep the font for editing, you have to assign it to a variable like so:
     ```lisp
     (def! my_font (load_font! "fonts/holgers_serif_cp850_08x16.fonted"))
     ```
     Now you can refer to it by the name of `my_font`.
  - `save_font!` saves a font as a native Fonted font file.
    Here is an example:
    ```lisp
    (save_font my_font "test.fonted")
    ```
  - Given a font `my_font` you can iterate it with the usual Lisp methods such as `map` and `filter`.
    Here are some of the predefined predicate functions to be used by `filter`:
    `ascii_num?`, `ascii_alpha?`, `ascii_alphanum?` etc.
    Here is an example of using `filter`:
    ```lisp
    my_font | (filter ascii_num?) | (count)  // gives 10
    ```
    By the way: The scripting language allows composing functions by use of the pipe syntax (`|`).
  - The functions `invert_glyph!`, `clear_glyph!`, `set_glyph!`, `flip_glyph_horizontally!`,`flip_glyph_vertically!`,`scroll_glyph_left!`,`scroll_glyph_right!`,`scroll_glyph_up!` and ,`scroll_glyph_down!` can be used to manipulate glyphs.
    Therefore, iterate over all glyphs, filter those you want and than apply these destructive functions.
    Destructive means the have side-effects and change the font.
    Therefore their names are bearing an exclamation mark (`!`).
    Here is an example of only inverting the ASCII digit glyphs:
    ```lisp
    my_font | (filter ascii_num?) | (map invert_glyph!) | (count)  // gives 10
    ```
  - The functions `copy_glyph!` copies one glyph to another within the same font or to another font.
    This can be used to selectively copy single or multiple glyphs:
    ```lisp
    // Assuming you run these from within Fonted's GUI
    // Copy single glyph, applying function directly using record syntax for arguments
    (copy_glyph! from: (font: _font id: 80) to: (font: _font id: 64))
    // You can achieve the same using less verbose list syntax for arguments
    (copy_glyph! from: [_font 80] to: [_font 64])
    // And this can be even shortend further using the const operator and skipping named arguments
    (copy_glyph! _font,80 _font,64)
    // Copy a whole gange of glyphs whithin one font, notice hof _font is implied for
    // the second argument to copy_glyph!
    (map (range 0 9) (fun i (copy_glyph! [_font (+ i 48)] (+ i 64))))
    ```
  - The `merge_fonts` function does not work on single glyphs but on complete fonts.
    It merges multiple fonts into one new font.
    However, all glyphs must be of the same size.
    You cannot merge an 8×8 font and an 8×16 font for example.
    ```lisp
    (def! font4 (merge_fonts font1 font2 font3))
    ```
  - The `export_binary_font!` function exports a given font as a raw binary file.
    Such files can be programmed into EPROMs to be used as character generators.
    This is how it looks:
    ```lisp
    (export_binary_font! my_font "rom.bin")
    ```
- When running from within the editor (**GUI**)
  - The predefined variable `_font` denotes the font, that is currently presented on screen.
  - It can be loaded by invoking the `load!` function.
    The font `_font` is implied by this function.
  - It can be saved by invoking the `save!` function.
    The font `_font` is implied by this function.
  - In addition to the font on screen, further fonts can be loaded and manipulatated, although not visible on screen.
    For example, you may pick specific glyphs from other fonts into the the font on screen.
    Use `load_font!` and `save_font!` to load and save additional fonts.

## Application Architecture

This project is supposed to be a quick one.
I did not want to over-engineer it.
Nor do I have plans for factoring out code into some universal ImGui library.
Nevertheless, I tried to follow a clean architecture approach to cleanly separate aspects of the program and to achieve testability.

<img src="pictures/clean%20architecture.svg" width="50%" alt="Clean Architecture Approach" />

## ImGui and openGL Tips

* Making window the same size as main window: https://github.com/ocornut/imgui/issues/2349
* Remove window decorations: https://stackoverflow.com/questions/75567117/make-imgui-window-undraggable-and-unresizable
* Drawing lines and shapes: https://github.com/ocornut/imgui/issues/2342
* Make main window (OS window) NOT resizable: https://discourse.dearimgui.org/t/os-window-size-adaptive-to-imgui-window-size/58/2
* Learning opneGL:
  * https://learnopengl.com/Getting-started/Textures
  * https://github.com/ocornut/imgui/wiki/Image-Loading-and-Displaying-Examples

## Acknowledgements

I am thankfully using many free and/or open source libraries, tools, and services.

-  [Dear ImGui](https://github.com/ocornut/imgui)
-  [Visual Studio Code](https://code.visualstudio.com)
-  [C++ Extension for VSC](https://github.com/Microsoft/vscode-cpptools.git)
-  [CMake Extension for VSC](https://github.com/microsoft/vscode-cmake-tools)
-  [Test Extension for VSC](https://github.com/matepek/vscode-catch2-test-adapter.git)
-  [CMake](https://cmake.org)
-  [GNU Make](https://www.gnu.org/software/make/)
-  [Conan](https://conan.io)
-  [GCC](https://gcc.gnu.org)
-  [Clang](https://clang.llvm.org)
-  [GitLab](https://gitlab.com)
-  [Catch2](https://github.com/catchorg/Catch2)
-  [{fmt}](https://github.com/fmtlib/fmt)
-  Some I may not even be aware of.


## Disclaimer

This is a private, free, open source and non-profit project (see LINCENSE file).
Use on your own risk.
I am not liable for any damage caused.
I am a private person, not associated with any companies, or organizations I may have mentioned herein.
