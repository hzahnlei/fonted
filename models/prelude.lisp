;; ****************************************************************************
;;
;;     ////////  |  fonted - Bitmap Font Editor
;;    //         |
;;   //////      |  Create your own bitmap fonts for vintage computers.
;;  //           |
;; //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
;;
;; ****************************************************************************

;;
;; GENERAL INTRODUCTION
;;
;; Glyph objects are records with a 'font' and an 'id' field.  The ID is an in-
;; dex into the font and "points" to a specific glyph within that font. You may
;; use the term "character code" synonymously.
;;

(def! FIRST_ASCII 0)
(def! LAST_ASCII 127)
(def! ascii? (fun glyph
                  (and (>= glyph.id FIRST_ASCII)
                       (=< glyph.id LAST_ASCII) ) ))

(def! FIRST_ASCII_NUM 48)
(def! LAST_ASCII_NUM 57)
(def! ascii_num? (fun glyph
                      (and (>= glyph.id FIRST_ASCII_NUM)
                           (=< glyph.id LAST_ASCII_NUM) ) ))

(def! FIRST_ASCII_UPPER_ALPHA 65)
(def! LAST_ASCII_UPPER_ALPHA 90)
(def! ascii_upper_alpha? (fun glyph
                              (and (>= glyph.id FIRST_ASCII_UPPER_ALPHA)
                                   (=< glyph.id LAST_ASCII_UPPER_ALPHA) ) ))

(def! FIRST_ASCII_LOWER_ALPHA 97)
(def! LAST_ASCII_LOWER_ALPHA 122)
(def! ascii_lower_alpha? (fun glyph
                              (and (>= glyph.id FIRST_ASCII_LOWER_ALPHA)
                                   (=< glyph.id LAST_ASCII_LOWER_ALPHA) ) ))

(def! ascii_alpha? (fun glyph
                        (or (ascii_upper_alpha? glyph)
                            (ascii_lower_alpha? glyph) ) ))

(def! ascii_alphanum? (fun glyph
                           (or (ascii_alpha? glyph)
                               (ascii_num? glyph) ) ))

(def! ASCII_NUL   0)
(def! ASCII_SOH   1)
(def! ASCII_STX   2)
(def! ASCII_ETX   3)
(def! ASCII_EOT   4)
(def! ASCII_ENQ   5)
(def! ASCII_ACK   6)
(def! ASCII_BEL   7)
(def! ASCII_BS    8)
(def! ASCII_TAB   9)
(def! ASCII_LF   10)
(def! ASCII_VT   11)
(def! ASCII_FF   12)
(def! ASCII_CR   13)
(def! ASCII_SO   14)
(def! ASCII_SI   15)
(def! ASCII_DLE  16)
(def! ASCII_DC1  17)
(def! ASCII_DC2  18)
(def! ASCII_DC3  19)
(def! ASCII_DC4  20)
(def! ASCII_NAK  21)
(def! ASCII_SYN  22)
(def! ASCII_ETB  23)
(def! ASCII_CAN  24)
(def! ASCII_EM   25)
(def! ASCII_SUB  26)
(def! ASCII_ESC  27)
(def! ASCII_FS   28)
(def! ASCII_GS   29)
(def! ASCII_RS   30)
(def! ASCII_US   31)
(def! ASCII_DEL 127)
(def! ascii_control? (fun glyph
                          (or (and (>= glyph.id ASCII_NUL)
                                   (=< glyph.id ASCII_US) )
                              (= glyph.id ASCII_DEL) ) ))

(def! ASCII_SPACE 32)
(def! ascii_space? (fun glyph (= glyph.id ASCII_SPACE)))
(def! ascii_whitespace? (fun glyph
                             (or (= glyph.id ASCII_TAB)
                                 (= glyph.id ASCII_SPACE) ) ))

(def! FIRST_PRINTABLE_ASCII ASCII_SPACE)
(def! LAST_PRINTABLE_ASCII  126)
(def! ascii_printable? (fun glyph
                            (or (= glyph.id FIRST_PRINTABLE_ASCII)
                                (= glyph.id LAST_PRINTABLE_ASCII) ) ))

(def! CP850_ä  132) 
(def! CP850_Ä  142)
(def! CP850_ö  148) 
(def! CP850_Ö  153) 
(def! CP850_ü  129) 
(def! CP850_Ü  154)
(def! CP850_sz 225)				       

;; The range-checks above  only work on glyph objects.  However, there might be
;; cases where one wants to enumerate  glyphs by their ID (aka code) which is a
;; plane integer number.  Therefore we might want to use this range check func-
;; tion in the future.  There are test cases to make sure  this works as expec-
;; ted.
(def! glyph? (fun g (tagged? g with: Fonted_Glyph)))
(def! glyph_in_range? (fun [glyph lower upper]
                           (case (glyph? glyph)   -> (and (>= glyph.id lower)
                                                          (=< glyph.id upper) )
                                 (Integer? glyph) -> (and (>= glyph lower)
                                                          (=< glyph upper) )
                                 default          -> (Error "Expected glyph or integer." nil) ) ))
;; Here are examples for how to use the above function:
;; (glyph_in_range? 3   1 10)
;; (glyph_in_range? 100 1 10)
;; _font | (filter (fun glyph (glyph_in_range? glyph 1 10)))
;; _font | (filter ascii_num?) | (map glyph?)
