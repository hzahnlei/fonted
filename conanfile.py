# *****************************************************************************
#
#     ////////  |  fonted - Bitmap Font Editor
#    //         |
#   //////      |  Create your own bitmap fonts for vintage computers.
#  //           |
# //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

import os

from conan import ConanFile
from conan.tools.cmake import cmake_layout
from conan.tools.files import copy


class ImGuiExample(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeDeps", "CMakeToolchain"
    requires = eval(os.environ["SOURCE_DEPENDENCIES"])
    test_requires = eval(os.environ["TEST_DEPENDENCIES"])

    def generate(self):
        copy(
            self,
            "*glfw*",
            os.path.join(self.dependencies["imgui"].package_folder, "res", "bindings"),
            os.path.join(self.source_folder, "bindings"),
        )
        copy(
            self,
            "*opengl3*",
            os.path.join(self.dependencies["imgui"].package_folder, "res", "bindings"),
            os.path.join(self.source_folder, "bindings"),
        )

    def layout(self):
        cmake_layout(self)
