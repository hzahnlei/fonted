# *****************************************************************************
#
#     ////////  |  fonted - Bitmap Font Editor
#    //         |
#   //////      |  Create your own bitmap fonts for vintage computers.
#  //           |
# //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

cmake_minimum_required (VERSION 3.20)

project (fonted CXX)

add_subdirectory (src)

install (TARGETS fonted DESTINATION "."
         RUNTIME DESTINATION bin
         ARCHIVE DESTINATION lib
         LIBRARY DESTINATION lib)
