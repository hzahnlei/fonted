# *****************************************************************************
#
#     ////////  |  fonted - Bitmap Font Editor
#    //         |
#   //////      |  Create your own bitmap fonts for vintage computers.
#  //           |
# //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

import os
from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps
from conan.tools.files import copy


class FontedRecipe(ConanFile):
    name = "fonted"
    version = os.environ["FULLY_QUALIFIED_VERSION"]
    package_type = "application"

    # Optional metadata
    license = "MIT"
    author = "Holger Zahnleiter opensource@holger.zahnleiter.org"
    url = "https://gitlab.com/hzahnlei/fonted"
    description = "Create your own bitmap fonts for vintage computers."
    topics = ("font editor", "bitmap font", "c++", "c++", "imgui", "dear imgui")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "models/*", "templates/*", "bindings/*"

    requires = eval(os.environ["SOURCE_DEPENDENCIES"])

    def layout(self):
        cmake_layout(self)

    def generate(self):
        # Copy bindings to source for build.
        copy(
            self,
            "*glfw*",
            os.path.join(self.dependencies["imgui"].package_folder, "res", "bindings"),
            os.path.join(self.source_folder, "bindings"),
        )
        copy(
            self,
            "*opengl3*",
            os.path.join(self.dependencies["imgui"].package_folder, "res", "bindings"),
            os.path.join(self.source_folder, "bindings"),
        )
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(
            self,
            pattern="*.hpp",
            src=os.path.join(self.source_folder, "generated"),
            dst=os.path.join(self.package_folder, "include"),
        )
        # Copy bindings to package for delvery.
        copy(
            self,
            pattern="*.*",
            src=os.path.join(self.source_folder, "bindings"),
            dst=os.path.join(self.package_folder, "bindings"),
        )
        cmake = CMake(self)
        cmake.install()
