# *****************************************************************************
#
#     ////////  |  fonted - Bitmap Font Editor
#    //         |
#   //////      |  Create your own bitmap fonts for vintage computers.
#  //           |
# //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

import os
from conan import ConanFile
from conan.tools.build import can_run


class AppPackageTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    def requirements(self):
        self.requires(self.tested_reference_str)

    def test(self):
        if can_run(self):
            self.run("fonted help", env="conanrun")
