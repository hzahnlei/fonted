// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "use_stl.hpp"

namespace fonted
{

	inline auto require(const bool condition, char const *const message) -> void
	{
		if (!condition)
			throw runtime_error{message};
	}

} // namespace fonted
