// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "use_stl.hpp"

namespace fonted
{

	using Pixel_Count = size_t;
	using Pixel_Row = Pixel_Count;
	using Pixel_Column = Pixel_Count;
	using Glyph_Width = Pixel_Count;
	using Glyph_Height = Pixel_Count;

	using Glyph_Count = size_t;
	using Glyph_ID = Glyph_Count;

	static constexpr auto DEFAULT_GLYPH_WIDTH{8U};
	static constexpr auto MIN_GLYPH_WIDTH{1U};
	static constexpr auto MAX_GLYPH_WIDTH{64U};

	static constexpr auto DEFAULT_GLYPH_HEIGHT{16U};
	static constexpr auto MIN_GLYPH_HEIGHT{1U};
	static constexpr auto MAX_GLYPH_HEIGHT{64U};

	static constexpr auto DEFAULT_GLYPH_COUNT{256U};
	static constexpr auto MIN_GLYPH_COUNT{1U};
	static constexpr auto MAX_GLYPH_COUNT{65536U};

	struct Font_Dimensions final
	{
		Glyph_Width glyph_width;
		Glyph_Height glyph_height;
		Glyph_Count glyph_count;

		Font_Dimensions(const Glyph_Width width = DEFAULT_GLYPH_WIDTH,
		                const Glyph_Height height = DEFAULT_GLYPH_HEIGHT,
		                const Glyph_Count = DEFAULT_GLYPH_COUNT);
	};

	struct Pixel_Coordinate final
	{
		Pixel_Row row;
		Pixel_Column column;
	};

	class Column_Model final
	{
	    private:
		bool m_value = false;

	    public:
		[[nodiscard]] auto is_set() const -> bool;
		auto toggle() -> void;
		auto set(const bool = true) -> void;
		auto clear() -> void;

		[[nodiscard]] auto operator==(const Column_Model &) const -> bool = default;
	};

	class Row_Model final
	{
	    private:
		std::vector<Column_Model> m_columns;

	    public:
		explicit Row_Model(const Pixel_Column);

		[[nodiscard]] auto column(const Pixel_Column) -> Column_Model &;
		[[nodiscard]] auto column(const Pixel_Column) const -> const Column_Model &;
		[[nodiscard]] auto column_count() const noexcept -> Pixel_Count;
		template <class ACTION> auto for_each_column(const ACTION &action) const -> void
		{
			for (const auto &column : m_columns)
			{
				action(column);
			}
		}

		auto scroll_left() -> void;
		auto scroll_right() -> void;
		auto invert() -> void;

		auto clear() -> void;
		auto set() -> void;

		auto flip() -> void;
		auto exchange_columns(const Pixel_Column source, const Pixel_Column destination) -> void;

		[[nodiscard]] auto operator==(const Row_Model &) const -> bool = default;
	};

	inline auto Row_Model::column_count() const noexcept -> Pixel_Count
	{
		return static_cast<Pixel_Count>(m_columns.size());
	}

	class Glyph_Model final
	{
	    private:
		std::vector<Row_Model> m_rows;

	    public:
		Glyph_Model(const Pixel_Row, const Pixel_Column);

		[[nodiscard]] auto row(const Pixel_Row) -> Row_Model &;
		[[nodiscard]] auto row(const Pixel_Row) const -> const Row_Model &;
		template <class ACTION> auto for_each_row(const ACTION &action) const -> void
		{
			for (const auto &row : m_rows)
			{
				action(row);
			}
		}

		auto scroll_up() -> void;
		auto scroll_left() -> void;
		auto scroll_right() -> void;
		auto scroll_down() -> void;
		auto invert() -> void;
		auto flip_horizontally() -> void;
		auto exchange_rows(const Pixel_Row source, const Pixel_Row destination) -> void;
		auto flip_vertically() -> void;

		auto copy(const Glyph_Model &) -> void;

		auto clear() -> void;
		auto set() -> void;

		[[nodiscard]] auto operator==(const Glyph_Model &) const -> bool = default;
	};

	class Domain_Model
	{
	    public:
		virtual ~Domain_Model() = default;

		virtual auto font_dimensions() const noexcept -> const Font_Dimensions & = 0;

		virtual auto set_font_name(const std::string &) -> void = 0;
		virtual auto font_name() const noexcept -> const std::string & = 0;
		virtual auto set_author_name(const std::string &) -> void = 0;
		virtual auto author_name() const noexcept -> const std::string & = 0;
		virtual auto set_code_page(const std::string &) -> void = 0;
		virtual auto code_page() const noexcept -> const std::string & = 0;
		virtual auto set_description(const std::string &) -> void = 0;
		virtual auto description() const noexcept -> const std::string & = 0;
		using Time_Point = std::chrono::system_clock::time_point;
		virtual auto creation_date() const noexcept -> const Time_Point & = 0;
		virtual auto update_date() const noexcept -> const Time_Point & = 0;
		virtual auto set_has_been_updated() -> void = 0;
		using Revision_Count = std::size_t;
		virtual auto revision_count() const noexcept -> Revision_Count = 0;

		virtual auto glyph(const Glyph_ID) -> Glyph_Model & = 0;
		virtual auto glyph(const Glyph_ID) const -> const Glyph_Model & = 0;
		using Immutable_Glyph_Fun = std::function<void(const Glyph_ID, const Glyph_Model &)>;
		virtual auto for_each_glyph(const Immutable_Glyph_Fun &) const -> void = 0;
	};

	class Font_Model final : public Domain_Model
	{
	    public:
		using Now_Function = std::function<Time_Point()>;

	    private:
		Font_Dimensions m_font_spec;
		const Now_Function m_now;

		std::vector<Glyph_Model> m_glyphs;

		std::string m_name{""};
		std::string m_author{""};
		std::string m_code_page{""};
		std::string m_description{""};
		Time_Point m_creation_date;
		Time_Point m_update_date;
		Revision_Count m_revision_count{1U};

	    public:
		Font_Model(const Font_Dimensions &, const Now_Function & = system_clock::now);
		Font_Model(const Font_Dimensions &, const Now_Function &, const Time_Point &creation_date,
		           const Time_Point &updated, const Revision_Count);
		~Font_Model() override = default;

		[[nodiscard]] auto font_dimensions() const noexcept -> const Font_Dimensions & override;

		auto set_font_name(const std::string &) -> void override;
		[[nodiscard]] auto font_name() const noexcept -> const std::string & override;
		auto set_author_name(const std::string &) -> void override;
		[[nodiscard]] auto author_name() const noexcept -> const std::string & override;
		auto set_code_page(const std::string &) -> void override;
		[[nodiscard]] auto code_page() const noexcept -> const std::string & override;
		auto set_description(const std::string &) -> void override;
		[[nodiscard]] auto description() const noexcept -> const std::string & override;
		[[nodiscard]] auto creation_date() const noexcept -> const Time_Point & override;
		[[nodiscard]] auto update_date() const noexcept -> const Time_Point & override;
		auto set_has_been_updated() -> void override;
		[[nodiscard]] auto revision_count() const noexcept -> Revision_Count override;

		[[nodiscard]] auto glyph(const Glyph_ID) -> Glyph_Model & override;
		[[nodiscard]] auto glyph(const Glyph_ID) const -> const Glyph_Model & override;
		auto for_each_glyph(const Immutable_Glyph_Fun &) const -> void override;
	};

	inline auto Font_Model::font_dimensions() const noexcept -> const Font_Dimensions & { return m_font_spec; }

	inline auto Font_Model::font_name() const noexcept -> const std::string & { return m_name; }

	inline auto Font_Model::author_name() const noexcept -> const std::string & { return m_author; }

	inline auto Font_Model::code_page() const noexcept -> const std::string & { return m_code_page; }

	inline auto Font_Model::description() const noexcept -> const std::string & { return m_description; }

	inline auto Font_Model::creation_date() const noexcept -> const Time_Point & { return m_creation_date; }

	inline auto Font_Model::update_date() const noexcept -> const Time_Point & { return m_update_date; }

	inline auto Font_Model::revision_count() const noexcept -> Revision_Count { return m_revision_count; }

} // namespace fonted
