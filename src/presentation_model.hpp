// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "domain_model.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>
#include <fmt/core.h>

namespace fonted
{

	using appsl::Stack_Frame;

	using Glyph_Column_Count = Glyph_Count;
	using Glyph_Column = Glyph_Count;
	using Glyph_Row = Glyph_Count;

	static constexpr auto DEFAULT_ROW_COUNT{16U};
	static constexpr auto MIN_ROW_COUNT{1U};
	static constexpr auto MAX_ROW_COUNT{256U};

	static constexpr auto DEFAULT_COLUMN_COUNT{16U};
	static constexpr auto MIN_COLUMN_COUNT{1U};
	// There is a maximum of 65536 glyphs, so we need a maximum matrix of 256×256 to display all glyphs.
	static constexpr auto MAX_COLUMN_COUNT{MAX_GLYPH_COUNT / MAX_ROW_COUNT};

	struct Glyph_Picker_Spec final
	{
		Glyph_Row row_count;
		Glyph_Column column_count;

		Glyph_Picker_Spec(const Glyph_Row = DEFAULT_ROW_COUNT, const Glyph_Column = DEFAULT_COLUMN_COUNT);
	};

	struct Glyph_Coordinate final
	{
		Glyph_Column column; // X
		Glyph_Row row;       // Y
	};

	class Presentation_Model
	{
	    public:
		static constexpr auto MAX_FILE_NAME_SIZE{256U};
		static constexpr auto MAX_FONT_NAME_SIZE{80U};
		static constexpr auto MAX_AUTHOR_NAME_SIZE{80U};
		static constexpr auto MAX_CODE_PAGE_NAME_SIZE{40U};
		static constexpr auto MAX_DESCRIPTION_SIZE{1000U};
		static constexpr auto MAX_COMMAND_INPUT_SIZE{256U};
		static constexpr auto MAX_COMMAND_OUTPUT_SIZE{8192U};

		virtual ~Presentation_Model() = default;

		//---- Glyph picker ---------------------------------------------------
		virtual auto glyph_picker_specification() const noexcept -> const Glyph_Picker_Spec & = 0;
		virtual auto glyph_id(const Glyph_Coordinate &) const noexcept -> Glyph_ID = 0;
		virtual auto row_and_column(const Glyph_ID) const noexcept -> Glyph_Coordinate = 0;
		virtual auto copy_active_glyph() -> void = 0;
		virtual auto paste_to_active_glyph() -> void = 0;
		virtual auto copy_to_active_glyph(const Presentation_Model &, const Glyph_ID) -> void = 0;
		virtual auto glyph(const Glyph_ID) const -> Glyph_Model & = 0;
		//---- Glyph/font -----------------------------------------------------
		virtual auto font_dimensions() const noexcept -> const Font_Dimensions & = 0;
		virtual auto font_name() noexcept -> char * = 0;
		virtual auto author_name() noexcept -> char * = 0;
		virtual auto code_page_name() noexcept -> char * = 0;
		virtual auto description() noexcept -> char * = 0;
		virtual auto on_active_glyph_toggle(const Pixel_Coordinate &) -> void = 0;
		virtual auto is_set_on_active_glyph(const Pixel_Coordinate &) const -> bool = 0;
		virtual auto set_active_glyph(const Glyph_ID) -> void = 0;
		virtual auto active_glyph_id() const noexcept -> Glyph_ID = 0;
		virtual auto active_glyph_scroll_up() -> void = 0;
		virtual auto active_glyph_scroll_left() -> void = 0;
		virtual auto active_glyph_scroll_right() -> void = 0;
		virtual auto active_glyph_scroll_down() -> void = 0;
		virtual auto invert_active_glyph() -> void = 0;
		virtual auto clear_active_glyph() -> void = 0;
		virtual auto set_active_glyph() -> void = 0;
		virtual auto flip_active_glyph_horizontally() -> void = 0;
		virtual auto flip_active_glyph_vertically() -> void = 0;
		virtual auto have_glyphs_changed() const noexcept -> bool = 0;
		virtual auto has_whole_font_changed() const noexcept -> bool = 0;
		virtual auto acknowledge_changes() noexcept -> void = 0;
		virtual auto for_each_glyph(const Domain_Model::Immutable_Glyph_Fun &) const -> void = 0;
		//---- File related ---------------------------------------------------
		virtual auto file_name() noexcept -> char * = 0;
		virtual auto save() -> void = 0;
		virtual auto load() -> void = 0;
		virtual auto export_binary() -> void = 0;
		virtual auto clock() -> const Font_Model::Now_Function & = 0;
		//---- Scripting ----------------------------------------------
		virtual auto command_input() noexcept -> char * = 0;
		virtual auto command_output() noexcept -> char * = 0;
		virtual auto execute_command(Stack_Frame &) -> void = 0;
	};

	static constexpr auto FONTED_FILE_EXTENSION{"fonted"};

	/**
	 * @brief An implementation of Presentation_Model that mostly delegates to Domain_Model.
	 */
	class Delegating_Presentation_Model final : public Presentation_Model
	{
	    private:
		unique_ptr<Domain_Model> m_domain_model;
		const Font_Model::Now_Function m_now;

		Glyph_Picker_Spec m_glyph_picker;

		array<char, MAX_FILE_NAME_SIZE> m_file_name{""};

		enum class Changes
		{
			NONE,
			GLYPHS,
			FONT
		};
		Changes m_changes = Changes::NONE;

		Glyph_ID m_active_glyph_id = 0;

		unique_ptr<Glyph_Model> m_clip_board = nullptr;

		array<char, MAX_FONT_NAME_SIZE> m_font_name{""};
		array<char, MAX_AUTHOR_NAME_SIZE> m_author_name{""};
		array<char, MAX_CODE_PAGE_NAME_SIZE> m_code_page{""};
		array<char, MAX_DESCRIPTION_SIZE> m_description{""};
		array<char, MAX_COMMAND_INPUT_SIZE> m_command_input{""};
		array<char, MAX_COMMAND_OUTPUT_SIZE> m_command_output{""};

	    public:
		Delegating_Presentation_Model(const Glyph_Column_Count, unique_ptr<Domain_Model>,
		                              const string &file_name,
		                              const Font_Model::Now_Function & = system_clock::now);
		~Delegating_Presentation_Model() override = default;

		//---- Glyph picker ---------------------------------------------------
		[[nodiscard]] auto glyph_picker_specification() const noexcept -> const Glyph_Picker_Spec & override;
		[[nodiscard]] auto glyph_id(const Glyph_Coordinate &) const noexcept -> Glyph_ID override;
		[[nodiscard]] auto row_and_column(const Glyph_ID) const noexcept -> Glyph_Coordinate override;
		auto copy_active_glyph() -> void override;
		auto paste_to_active_glyph() -> void override;
		auto copy_to_active_glyph(const Presentation_Model &, const Glyph_ID) -> void override;
		auto glyph(const Glyph_ID) const -> Glyph_Model & override;
		//---- Glyph/font -----------------------------------------------------
		[[nodiscard]] auto font_dimensions() const noexcept -> const Font_Dimensions & override;
		[[nodiscard]] auto font_name() noexcept -> char * override;
		[[nodiscard]] auto author_name() noexcept -> char * override;
		[[nodiscard]] auto code_page_name() noexcept -> char * override;
		[[nodiscard]] auto description() noexcept -> char * override;
		auto on_active_glyph_toggle(const Pixel_Coordinate &) -> void override;
		[[nodiscard]] auto is_set_on_active_glyph(const Pixel_Coordinate &) const -> bool override;
		auto set_active_glyph(const Glyph_ID) -> void override;
		[[nodiscard]] auto active_glyph_id() const noexcept -> Glyph_ID override;
		auto active_glyph_scroll_up() -> void override;
		auto active_glyph_scroll_left() -> void override;
		auto active_glyph_scroll_right() -> void override;
		auto active_glyph_scroll_down() -> void override;
		auto invert_active_glyph() -> void override;
		auto clear_active_glyph() -> void override;
		auto set_active_glyph() -> void override;
		auto flip_active_glyph_horizontally() -> void override;
		auto flip_active_glyph_vertically() -> void override;
		[[nodiscard]] auto have_glyphs_changed() const noexcept -> bool override;
		[[nodiscard]] auto has_whole_font_changed() const noexcept -> bool override;
		auto acknowledge_changes() noexcept -> void override;
		auto for_each_glyph(const Domain_Model::Immutable_Glyph_Fun &) const -> void override;
		//---- File related ---------------------------------------------------
		[[nodiscard]] auto file_name() noexcept -> char * override;
		auto save() -> void override;
		auto load() -> void override;
		auto export_binary() -> void override;
		auto clock() -> const Font_Model::Now_Function & override;
		//---- Scripting ----------------------------------------------
		auto command_input() noexcept -> char * override;
		auto command_output() noexcept -> char * override;
		auto execute_command(Stack_Frame &) -> void override;

	    private:
		auto remember_only_glyphs_have_changed() noexcept -> void;
		auto remember_whole_font_has_changed() noexcept -> void;

		auto update_domain_model() -> void; // SAVE

		auto update_presentation_model(const string &) -> void; // LOAD
	};

	auto binary_file_name(const string &) -> string;

	//---- Glyph picker ---------------------------------------------------

	inline auto Delegating_Presentation_Model::glyph_picker_specification() const noexcept
			-> const Glyph_Picker_Spec &
	{
		return m_glyph_picker;
	}

	inline auto Delegating_Presentation_Model::glyph(const Glyph_ID glyph_id) const -> Glyph_Model &
	{
		return m_domain_model->glyph(glyph_id);
	}

	//---- Glyph/font -------------------------------------------------------------

	inline auto Delegating_Presentation_Model::font_dimensions() const noexcept -> const Font_Dimensions &
	{
		return m_domain_model->font_dimensions();
	}

	inline auto Delegating_Presentation_Model::font_name() noexcept -> char * { return begin(m_font_name); }

	inline auto Delegating_Presentation_Model::author_name() noexcept -> char * { return begin(m_author_name); }

	inline auto Delegating_Presentation_Model::code_page_name() noexcept -> char * { return begin(m_code_page); }

	inline auto Delegating_Presentation_Model::description() noexcept -> char * { return begin(m_description); }

	inline auto Delegating_Presentation_Model::active_glyph_id() const noexcept -> Glyph_ID
	{
		return m_active_glyph_id;
	}

	inline auto Delegating_Presentation_Model::for_each_glyph(const Domain_Model::Immutable_Glyph_Fun &f) const
			-> void
	{
		m_domain_model->for_each_glyph(f);
	}

	inline auto Delegating_Presentation_Model::have_glyphs_changed() const noexcept -> bool
	{
		return m_changes == Changes::GLYPHS;
	}

	inline auto Delegating_Presentation_Model::has_whole_font_changed() const noexcept -> bool
	{
		return m_changes == Changes::FONT;
	}

	inline auto Delegating_Presentation_Model::acknowledge_changes() noexcept -> void { m_changes = Changes::NONE; }

	//---- File related ---------------------------------------------------

	inline auto Delegating_Presentation_Model::file_name() noexcept -> char * { return begin(m_file_name); }

	inline auto Delegating_Presentation_Model::clock() -> const Font_Model::Now_Function & { return m_now; }

	//---- Scripting ------------------------------------------------------

	inline auto Delegating_Presentation_Model::command_input() noexcept -> char * { return begin(m_command_input); }

	inline auto Delegating_Presentation_Model::command_output() noexcept -> char *
	{
		return begin(m_command_output);
	}

	//---- Private stuff ----------------------------------------------------------

	inline auto Delegating_Presentation_Model::remember_only_glyphs_have_changed() noexcept -> void
	{
		if (m_changes == Changes::NONE)
		{
			m_changes = Changes::GLYPHS;
		}
	}

	inline auto Delegating_Presentation_Model::remember_whole_font_has_changed() noexcept -> void
	{
		m_changes = Changes::FONT;
	}

} // namespace fonted
