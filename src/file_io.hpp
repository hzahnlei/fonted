// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "domain_model.hpp"
#include "use_stl.hpp"

namespace fonted::native
{
	[[nodiscard]] auto load(const string &file_name, const Font_Model::Now_Function &) -> unique_ptr<Domain_Model>;
	auto store(const Domain_Model &, const string &file_name) -> void;
} // namespace fonted::native

namespace fonted::binary
{
	[[nodiscard]] auto load(const string &file_name, const Glyph_Width, const Glyph_Height,
	                        const optional<Glyph_Count>, const size_t offset, const Font_Model::Now_Function &)
			-> unique_ptr<Domain_Model>;
	auto store(const Domain_Model &, const string &file_name) -> void;
} // namespace fonted::binary
