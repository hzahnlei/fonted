// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "column_layout.hpp"
#include "use_stl.hpp"

namespace fonted
{

		Column_Layout::Column_Layout(const ID &id, const Surrounding_Border border_padding)
			: Base_Widget{id}, m_border_padding{border_padding}
	{
	}

	//---- Interface methods ------------------------------------------------------

	auto Column_Layout::widget_size() const -> Size
	{
		auto max_width{0.0f};
		auto sum_height{0.0f};
		for (const auto &child : m_children)
		{
			const auto child_size{child->widget_size()};
			max_width = (child_size.x > max_width) ? child_size.x : max_width;
			sum_height += child_size.y;
		}
		// Seems like ImGui always pads at the X axis, if ImGui::BeginChild is involved. Therefore, we add
		// style().WindowPadding.x to this widget's width.
		const auto padding_width{style().WindowPadding.x};
		const auto padding_height{style().WindowPadding.y};
		return Size{max_width + (pad_border() ? 2 * padding_width : 0) + padding_width,
		            sum_height + (pad_border() ? 2 * padding_height : 0)};
	}

	auto Column_Layout::operator()() -> void
	{
		if (ImGui::GetCurrentWindow()->SkipItems)
		{
			return;
		}

		const auto my_size{widget_size()};

#ifdef FONTED_DEBUG_DRAWING
		auto &window{*ImGui::GetCurrentWindow()};
		auto &draw_list{*window.DrawList};
		const ImRect bounding_box{window.DC.CursorPos, window.DC.CursorPos + my_size};
		draw_list.AddRectFilled(bounding_box.Min, bounding_box.Max, DEBUG_BACKGROUND_COLOR);
#endif

		ImGui::BeginChild(id().c_str(), my_size, NO_BORDER,
		                  pad_border() ? ImGuiWindowFlags_::ImGuiWindowFlags_AlwaysUseWindowPadding
		                               : ImGuiWindowFlags_::ImGuiWindowFlags_None);

		auto pos_y{ImGui::GetCursorPosY()};
		for (const auto &child : m_children)
		{
			const auto child_size{child->widget_size()};
			if (child_size.x < my_size.x)
			{
				const auto centering_pad{(my_size.x - child_size.x) / 2};
				ImGui::SetCursorPosX(ImGui::GetCursorPosX() + centering_pad);
			}
			ImGui::SetCursorPosY(pos_y);
			(*child)();
			pos_y += child_size.y;
		}

		ImGui::EndChild();

#ifdef FONTED_DEBUG_DRAWING
		draw_list.AddRect(bounding_box.Min, bounding_box.Max, DEBUG_BORDER_COLOR);
#endif
	}

} // namespace fonted
