// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "presentation_model.hpp"
#include "use_stl.hpp"
#include "widget_commons.hpp"
#include <imgui.h>
#include <imgui_internal.h>

namespace fonted
{
	class Glyph_Editor final : public Base_Widget
	{
	    public:
		[[nodiscard]] static auto required_widget_size(const Presentation_Model &) -> Size;
		[[nodiscard]] static auto row_and_column(const Presentation_Model &, const ImVec2 &)
				-> optional<Pixel_Coordinate>;

	    private:
		Presentation_Model &m_model;

	    public:
		Glyph_Editor(const ID &, Presentation_Model &);
		~Glyph_Editor() override = default;

		[[nodiscard]] auto widget_size() const -> Size override;
		auto operator()() -> void override;

	    private:
		auto draw(const ImRect &, ImGuiWindow &) const -> void;
		auto draw_border(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner, ImDrawList &) const
				-> void;
		auto draw_pixels(const ImVec2 &upper_left_corner, ImDrawList &) const -> void;
		auto draw_grid_overlay(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
		                       ImDrawList &) const -> void;
		auto draw_grid_horizontal_lines(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
		                                ImDrawList &) const -> void;
		auto draw_grid_vertical_lines(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corne,
		                              ImDrawList &r) const -> void;
		auto draw_cross_hair(const Pixel_Coordinate &pixel_coordinate, const ImRect &bounding_box,
		                     ImGuiWindow &window) const -> void;
	};

	inline auto Glyph_Editor::widget_size() const -> Size { return required_widget_size(m_model); }

} // namespace fonted
