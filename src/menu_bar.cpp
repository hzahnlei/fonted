// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "menu_bar.hpp"
#include "use_stl.hpp"
#include <fmt/core.h>
#include <imgui.h>
#include <imgui_internal.h>

namespace fonted
{

	Menu_Bar::Menu_Bar(const ID &id, Presentation_Model &presentation, Menu_Items &&menu_items, Stack_Frame &stack)
			: Base_Widget{id}, m_presentation_model{presentation}, m_menu_items{move(menu_items)},
			  m_stack{stack}
	{
		if (m_menu_items.size() > MAX_MENU_ITEMS)
		{
			throw runtime_error{"Too many menu items."};
		}
	}

	//---- Interface methods ------------------------------------------------------

	constexpr Widget::Size MENU_ITEM_SIZE{128, 26};

	auto Menu_Bar::widget_size() const -> Size
	{
		const auto padding_width{style().WindowPadding.x};
		const auto padding_height{style().WindowPadding.y};
		return Size{(MENU_ITEM_SIZE.x + padding_width) * MAX_MENU_ITEMS - padding_width,
		            MENU_ITEM_SIZE.y + 200 +
		                            (2 * padding_height)}; // 200 is a magic number for InputText fields. Find
		                                                   // an exact way for computing required height.
	}

	const static vector<string> ACTION_NAMES{"Load", "Save"};

	auto Menu_Bar::operator()() -> void
	{
		if (ImGui::GetCurrentWindow()->SkipItems)
		{
			return;
		}

#ifdef FONTED_DEBUG_DRAWING
		const auto &window{*ImGui::GetCurrentWindow()};
		auto &draw_list{*window.DrawList};
		const ImRect bounding_box{window.DC.CursorPos, window.DC.CursorPos + widget_size()};
		draw_list.AddRectFilled(bounding_box.Min, bounding_box.Max, DEBUG_BACKGROUND_COLOR);
#endif

		ImGui::BeginChild(id().c_str());

		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Button, BUTTON_OUTLINE_COLOR);
		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonHovered, BUTTON_HIGHLIGHT_COLOR);
		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonActive, BUTTON_PRESSED_COLOR);
		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, BUTTON_PRESSED_COLOR);

		auto i{0U};
		for (; i < m_menu_items.size(); i++)
		{
			const auto &menu_item{m_menu_items[i]};
			if (i > 0)
			{
				ImGui::SameLine();
			}
			if (ImGui::Button(menu_item.display_name.c_str(), MENU_ITEM_SIZE) ||
			    ImGui::IsKeyPressed(menu_item.key))
			{
				menu_item.action(m_presentation_model);
			}
		}
		for (; i < MAX_MENU_ITEMS; i++)
		{
			ImGui::SameLine();
			ImGui::Button(fmt::format("n/a").c_str(), MENU_ITEM_SIZE);
		}

		ImGui::PopStyleColor(4);

		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, CAPTION_TEXT_COLOR);
		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_FrameBg, INPUT_BOX_BACKGROUND_COLOR);

		ImGui::InputText("File Name", m_presentation_model.file_name(), Presentation_Model::MAX_FILE_NAME_SIZE);
		ImGui::InputText("Font Name", m_presentation_model.font_name(), Presentation_Model::MAX_FONT_NAME_SIZE);
		ImGui::InputText("Author", m_presentation_model.author_name(),
		                 Presentation_Model::MAX_AUTHOR_NAME_SIZE);
		ImGui::InputText("Code Page", m_presentation_model.code_page_name(),
		                 Presentation_Model::MAX_CODE_PAGE_NAME_SIZE);
		ImGui::InputText("Description", m_presentation_model.description(),
		                 Presentation_Model::MAX_DESCRIPTION_SIZE);

		ImGui::InputTextMultiline("Command", m_presentation_model.command_input(),
		                          Presentation_Model::MAX_COMMAND_INPUT_SIZE, ImVec2{0, 48});
		ImGui::SameLine();
		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Button, BUTTON_OUTLINE_COLOR);
		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonHovered, BUTTON_HIGHLIGHT_COLOR);
		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonActive, BUTTON_PRESSED_COLOR);
		ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, BUTTON_PRESSED_COLOR);
		if (ImGui::Button("Execute", MENU_ITEM_SIZE))
		{
			m_presentation_model.execute_command(m_stack);
		}
		ImGui::PopStyleColor(4);
		ImGui::InputTextMultiline("Result", m_presentation_model.command_output(),
		                          Presentation_Model::MAX_COMMAND_OUTPUT_SIZE, ImVec2{0, 48});

		ImGui::PopStyleColor(2);

		ImGui::EndChild();

#ifdef FONTED_DEBUG_DRAWING
		draw_list.AddRect(bounding_box.Min, bounding_box.Max, DEBUG_BORDER_COLOR);
#endif
	}

} // namespace fonted
