// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "fonted_view.hpp"

#include "../bindings/imgui_impl_glfw.h"
#include "../bindings/imgui_impl_opengl3.h"
#include "use_stl.hpp"
#include "widget_commons.hpp"
#include <fmt/core.h>
#include <imgui.h>

namespace fonted
{

	[[nodiscard]] inline auto font_texture_dimensions(const Presentation_Model &presentation,
	                                                  const ImTextureID id = nullptr)
	{
		return View_Model::Font_Texture_Specification{
				.id = id,
				.width = presentation.font_dimensions().glyph_width,
				.height = presentation.font_dimensions().glyph_height *
		                          presentation.font_dimensions().glyph_count,
		};
	}

	GL3_View_Model::GL3_View_Model(Presentation_Model &presentation)
			: m_presentation_model(presentation), m_font_texture_spec{font_texture_dimensions(presentation)}
	{
		init_glfw();
		init_glew();
		init_imgui();
		init_font_texture();
	}

	GL3_View_Model::~GL3_View_Model()
	{
		cleanup_font_texture();
		cleanup_imgui();
		cleanup_glew();
		cleanup_glfw();
	}

	auto GL3_View_Model::should_close() const -> bool { return glfwWindowShouldClose(m_window); }

	auto GL3_View_Model::begin_frame() -> void
	{
		glfwPollEvents();
		glClearColor(0.45f, 0.55f, 0.60f, 1.00f);
		glClear(GL_COLOR_BUFFER_BIT);
		// feed inputs to dear imgui, start new frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		// Font texture needs to be rebuilt on every model change. It does not automatically follow changes to
		// the memory buffer. The buffer is in the computers main memory while the actual texture memory resides
		// in the graphics memory of the graphics card.
		if (m_presentation_model.have_glyphs_changed())
		{
			m_presentation_model.acknowledge_changes();
			rebuild_font_texture();
		}
		else if (m_presentation_model.has_whole_font_changed())
		{
			m_presentation_model.acknowledge_changes();
			build_new_font_texture();
		}
	}

	auto GL3_View_Model::end_frame() -> void
	{
		// Render dear imgui into screen
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		int display_w;
		int display_h;
		glfwGetFramebufferSize(m_window, &display_w, &display_h);
		glViewport(0, 0, display_w, display_h);
		glfwSwapBuffers(m_window);
	}

	auto GL3_View_Model::uv_coordinates(const Glyph_ID glyph_id) const -> ImRect
	{
		const auto y{1.0f / static_cast<float>(m_presentation_model.font_dimensions().glyph_count)};
		const ImVec2 upper_left{0.0f, y * static_cast<float>(glyph_id)};
		const ImVec2 lower_right{1.0f, y * static_cast<float>(glyph_id + 1)};
		return ImRect{upper_left, lower_right};
	}

	auto GL3_View_Model::set_window_size(const Width_Pixels width, const Height_Pixels height) -> void
	{
		glfwSetWindowSize(m_window, width, height);
	}

	static auto glfw_error_callback(const int error, const char *description)
	{
		std::cerr << "Glfw Error " << error << ": " << description << '\n';
	}

	auto GL3_View_Model::init_glfw() -> void
	{
		// Setup window
		glfwSetErrorCallback(glfw_error_callback);
		if (!glfwInit())
		{
			throw std::runtime_error{"Failed to initialize Glfw."};
		}

		// Decide GL+GLSL versions
#if __APPLE__
		// GL 3.2 + GLSL 150
		m_glsl_version = "#version 150";
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // 3.2+ only
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);           // Required on Mac
#else
		// GL 3.0 + GLSL 130
		m_glsl_version = "#version 130";
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
#endif

		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		// Create window with graphics context and some reasonable initial size. However, the window will be
		// resized to fit the content anyways.
		m_window = glfwCreateWindow(1200, 800, "fonted - Bitmap Font Editor", nullptr, nullptr);
		if (m_window == nullptr)
		{
			throw std::runtime_error{"Failed to create Glfw window."};
		}
		glfwMakeContextCurrent(m_window);
		glfwSwapInterval(1); // Enable vsync
	}

	auto GL3_View_Model::init_glew() -> void
	{
		if (glewInit() != GLEW_OK)
		{
			throw std::runtime_error{"Failed to initialize OpenGL loader (Glew)."};
		}
		int screen_width;
		int screen_height;
		glfwGetFramebufferSize(m_window, &screen_width, &screen_height);
		glViewport(0, 0, screen_width, screen_height);
	}

	auto GL3_View_Model::init_imgui() -> void
	{
		// Setup Dear ImGui context
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGuiIO &io = ImGui::GetIO();
		io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
		io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;  // Enable Gamepad Controls
		// Setup Platform/Renderer bindings
		ImGui_ImplGlfw_InitForOpenGL(m_window, true);
		ImGui_ImplOpenGL3_Init(m_glsl_version.c_str());
	}

	constexpr auto COLOR_CHANNEL_COUNT = 3U;

	[[nodiscard]] inline auto
	required_font_texture_memory_size(const View_Model::Font_Texture_Specification &texture)
	{
		return texture.width * texture.height * COLOR_CHANNEL_COUNT;
	}

	auto GL3_View_Model::init_font_texture() -> void
	{
		m_buffer_size = required_font_texture_memory_size(m_font_texture_spec);
		m_buffer = new Byte[m_buffer_size];
		std::memset(m_buffer, 0, m_buffer_size);
		glGenTextures(1, (GLuint *)(&m_font_texture_spec.id));
		glBindTexture(GL_TEXTURE_2D, (intptr_t)(void *)(m_font_texture_spec.id));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		// Prepare texture so that the font is getting displayed. Subsequently the texture will only rebuilt on
		// font changes.
		rebuild_font_texture();
	}

	auto GL3_View_Model::cleanup_font_texture() -> void { delete[] m_buffer; }

	auto GL3_View_Model::cleanup_imgui() const -> void
	{
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();
	}

	auto GL3_View_Model::cleanup_glew() -> void
	{
		glfwDestroyWindow(m_window);
		m_window = nullptr;
	}

	auto GL3_View_Model::cleanup_glfw() const -> void { glfwTerminate(); }

	auto GL3_View_Model::rebuild_font_texture() -> void
	{
		const auto bytes_per_glyp_row{m_presentation_model.font_dimensions().glyph_width * COLOR_CHANNEL_COUNT};
		const auto bytes_per_glyph{bytes_per_glyp_row * m_presentation_model.font_dimensions().glyph_height};
		m_presentation_model.for_each_glyph(
				[&](const auto glyph_id, const auto &glyph_model)
				{
					const auto glyph_offset{bytes_per_glyph * glyph_id};
					for (auto pixel_row = 0U;
			                     pixel_row < m_presentation_model.font_dimensions().glyph_height;
			                     pixel_row++)
					{
						const auto row_offset{pixel_row * bytes_per_glyp_row};
						for (auto pixel_col = 0U;
				                     pixel_col < m_presentation_model.font_dimensions().glyph_width;
				                     pixel_col++)
						{
							const auto buffer_addr_offset{
									glyph_offset + row_offset +
									(pixel_col * COLOR_CHANNEL_COUNT)};
							//+1 --> Skip R, only write G, skip B too
							m_buffer[buffer_addr_offset + 1] = static_cast<Byte>(
									glyph_model.row(pixel_row).column(pixel_col)
													.is_set()
											? 255
											: 64);
						}
					}
				});
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, static_cast<GLsizei>(m_font_texture_spec.width),
		             static_cast<GLsizei>(m_font_texture_spec.height), 0, GL_RGB, GL_UNSIGNED_BYTE, m_buffer);
	}

	auto GL3_View_Model::build_new_font_texture() -> void
	{
		delete[] m_buffer;
		m_font_texture_spec = font_texture_dimensions(m_presentation_model, m_font_texture_spec.id);
		m_buffer_size = required_font_texture_memory_size(m_font_texture_spec);
		m_buffer = new Byte[m_buffer_size];
		std::memset(m_buffer, 0, m_buffer_size);
		rebuild_font_texture();
	}

} // namespace fonted
