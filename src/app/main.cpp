// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "fonted_app.hpp"
#include <iostream>
#include <junkbox/junkbox.hpp>

enum class Exit_Code : int
{
	OK = 0,
	ERROR
};

/**
 * @brief Only handle command line options and than hand over to application.
 *
 * @details Purpose of main is only to handle aspects on its own level of abstraction, that is command line options,
 * environment variables, application wiring and exit codes. Everything else is handled by the application.
 *
 * @return int OK=0 in case of success, ERROR=1 else.
 */
auto main(const int arg_count, const char **args) -> int
{
	try
	{
		fonted::Fonted_App app{yacl::program_name(arg_count, args)};
		app.run(yacl::list_of_arguments(arg_count, args), std::cin, std::cout, std::cerr);
		return static_cast<int>(Exit_Code::OK);
	}
	catch (const junkbox::Base_Exception &cause)
	{
		std::cerr << cause.pretty_message() << '\n';
		return static_cast<int>(Exit_Code::ERROR);
	}
	catch (const std::exception &cause)
	{
		std::cerr << cause.what() << '\n';
		return static_cast<int>(Exit_Code::ERROR);
	}
	catch (...)
	{
		std::cerr << "An unknown exception has occurred.\n";
		return static_cast<int>(Exit_Code::ERROR);
	}
}
