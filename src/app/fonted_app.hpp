// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "domain_model.hpp"
#include <yacl/yacl.hpp>

namespace fonted
{

	using yacl::Base_Application;
	using yacl::Command_Line_Parser;
	using yacl::Command_Line_Parser_Builder;

	class Fonted_App final : public Base_Application
	{

	    private:
		const Font_Model::Now_Function &m_now;

	    public:
		static const string CAPTION;
		static const string DESCRIPTION;
		static const string VERSION;
		static const string COPYRIGHT;

		Fonted_App(const string &, const Font_Model::Now_Function & = system_clock::now);
		~Fonted_App() override = default;

	    private:
		auto command_line_parser(const string &program_name) const -> Command_Line_Parser;

		auto register_edit_font_command(Command_Line_Parser_Builder &) const -> void;
		auto register_new_command(Command_Line_Parser_Builder &) const -> void;
		auto register_export_command(Command_Line_Parser_Builder &) const -> void;
		auto register_import_binary_command(Command_Line_Parser_Builder &) const -> void;
		auto register_script_command(Command_Line_Parser_Builder &) const -> void;
		auto register_repl_command(Command_Line_Parser_Builder &) const -> void;
	};

} // namespace fonted
