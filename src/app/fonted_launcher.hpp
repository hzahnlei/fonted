// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "domain_model.hpp"
#include "presentation_model.hpp"
#include "use_stl.hpp"

// Helper functions to start the fonte editor in different ways.

namespace fonted
{

	/**
	 * @brief Start the font editor with a blank new font and default dimenstions.
	 */
	auto run_fonted_on_blank_default_font() -> void;

	/**
	 * @brief Start the font editor and open an existing font file for editing.
	 */
	auto run_fonted_on_existing_font(const string &file_name, const Font_Model::Now_Function &) -> void;

	/**
	 * @brief Start the font editor with a blank new font and custom dimenstions.
	 */
	auto run_fonted_on_blank_custom_font(const Glyph_Width, const Glyph_Height, const Glyph_Count) -> void;

	/**
	 * @brief Run a script.
	 */
	auto run_fonted_script(const string &script_file_name, const Font_Model::Now_Function &) -> void;

	/**
	 * @brief Open a shell for interactive scripting.
	 */
	auto run_repl(const string &script_file_name, const Font_Model::Now_Function &, istream &in, ostream &out,
	              ostream &err) -> void;

} // namespace fonted
