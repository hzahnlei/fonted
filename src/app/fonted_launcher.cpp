// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "fonted_launcher.hpp"
#include "file_io.hpp"
#include "fonted_controller.hpp"
#include "fonted_view.hpp"
#include "scripting/scripting.hpp"
#include "use_stl.hpp"
#include "version.hpp"
#include <appsl/appsl.hpp>
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>
#include <yaml-cpp/yaml.h>

namespace fonted
{

	using namespace appsl;
	using namespace junkbox;

	auto run_fonted(shared_ptr<Delegating_Presentation_Model> presentation_model) -> void
	{
		GL3_View_Model view_model{*presentation_model};
		auto runtime{scripting::make_scripting_environment_gui(presentation_model)};
		Fonted_Controller font_editor{*presentation_model, view_model, *runtime};
		font_editor.run();
	}

	static const auto DEFAULT_FILE_NAME{fmt::format("./my_font.{}", FONTED_FILE_EXTENSION)};

	auto blank_presentation_model()
	{
		return make_unique<Delegating_Presentation_Model>(
				DEFAULT_COLUMN_COUNT, make_unique<Font_Model>(Font_Dimensions{}), DEFAULT_FILE_NAME);
	}

	auto run_fonted_on_blank_default_font() -> void { run_fonted(blank_presentation_model()); }

	auto presentation_model_from_file(const string &file_name, const Font_Model::Now_Function &now)
	{
		return make_unique<Delegating_Presentation_Model>(DEFAULT_COLUMN_COUNT, native::load(file_name, now),
		                                                  file_name);
	}

	auto run_fonted_on_existing_font(const string &file_name, const Font_Model::Now_Function &now) -> void
	{
		run_fonted(presentation_model_from_file(file_name, now));
	}

	auto blank_custom_presentation_model(const Glyph_Width width, const Glyph_Height height,
	                                     const Glyph_Count count)
	{
		return make_unique<Delegating_Presentation_Model>(
				DEFAULT_COLUMN_COUNT, make_unique<Font_Model>(Font_Dimensions{width, height, count}),
				DEFAULT_FILE_NAME);
	}

	auto run_fonted_on_blank_custom_font(const Glyph_Width width, const Glyph_Height height,
	                                     const Glyph_Count count) -> void
	{
		run_fonted(blank_custom_presentation_model(width, height, count));
	}

	auto run_fonted_script(const string &script_file_name, const Font_Model::Now_Function &now) -> void
	{
		auto runtime{scripting::make_scripting_environment_headless(now)};
		auto script{file::load_text_file(script_file_name)};
		execute_script(script, *runtime);
	}

	auto run_repl(const string &script_file_name, const Font_Model::Now_Function &now, istream &in, ostream &out,
	              ostream &err) -> void
	{
		auto runtime{scripting::make_scripting_environment_headless(now)};
		if (script_file_name != "")
		{
			auto script{file::load_text_file(script_file_name)};
			execute_script(script, *runtime);
		}
		rep_loop(fmt::format("Fonted Scripting Extension {}.{}.{}", fonted::VERSION_MAJOR,
		                     fonted::VERSION_MINOR, fonted::VERSION_PATCH),
		         in, out, err, *runtime);
	}

} // namespace fonted
