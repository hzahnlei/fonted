// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "fonted_app.hpp"
#include "file_io.hpp"
#include "fonted_launcher.hpp"
#include "use_stl.hpp"
#include "version.hpp"
#include <fmt/core.h>

namespace fonted
{

	using namespace yacl;

	const string Fonted_App::CAPTION{"\n"
	                                 "    ////////\n"
	                                 "   //                    //             //\n"
	                                 "  //////  ////  ///// //////  ////  /////\n"
	                                 " //     //  // //  //  //   //==  //  //\n"
	                                 "//      ////  //  //   ///  ////  /////\n"
	                                 "\n"};

	const string Fonted_App::DESCRIPTION{"Portable bitmap font editor built on Dear ImGui.\n"
	                                     "Create your own bitmap fonts for vintage computers."};

	const string Fonted_App::VERSION{
			fmt::format("{}.{}.{}", fonted::VERSION_MAJOR, fonted::VERSION_MINOR, fonted::VERSION_PATCH)};

	const string Fonted_App::COPYRIGHT{"(c) 2023, 2024 by Holger Zahnleiter, all rights reserved"};

	Fonted_App::Fonted_App(const string &program_name, const Font_Model::Now_Function &now)
			: Base_Application{command_line_parser(program_name)}, m_now{now}
	{
	}

	auto Fonted_App::command_line_parser(const string &program_name) const -> Command_Line_Parser
	{
		Command_Line_Parser_Builder cli{program_name, CAPTION};
		cli.version(VERSION)
				.copyright(COPYRIGHT)
				.author("Holger Zahnleiter")
				.license(License::MIT)
				.open_source_software_used("Dear ImGui")
				.open_source_software_used("openGL")
				.open_source_software_used("fmt")
				.open_source_software_used("Conan")
				.open_source_software_used("GCC")
				.open_source_software_used("Clang")
				.open_source_software_used("GNU Make")
				.open_source_software_used("CMake")
				.open_source_software_used("Catch 2")
				.open_source_software_used("C++ Junk Box")
				.open_source_software_used("C++ YACL");
		register_edit_font_command(cli);
		register_new_command(cli);
		register_export_command(cli);
		register_import_binary_command(cli);
		register_script_command(cli);
		register_repl_command(cli);
		return cli.description(DESCRIPTION);
	}

	auto Fonted_App::register_edit_font_command(Command_Line_Parser_Builder &cli) const -> void
	{
		// clang-format off
		cli.anonymous_command()
			.description("Start the fonted editor with a blank font with default dimensions, or open and "
					"edit an existing font file, if a file name is given.")
			.optional().positional().text_param("filename")
				.description("Name of font file to be opened.")
				.allow_empty_text()
				.allow_white_space()
				.defaults_to("")
			.action(
				[&](const auto &context, auto &, auto &, auto &)
				{
					const auto file_name{context.text_value_at(0)};
					if (file_name == "")
					{
						run_fonted_on_blank_default_font();
					}
					else
					{
						run_fonted_on_existing_font(file_name,m_now);
					}
				});
		// clang-format on
	}

	auto Fonted_App::register_new_command(Command_Line_Parser_Builder &commands) const -> void
	{
		// clang-format off
		commands.command("new")
			.description("Start the fonted editor with a blank font of the given dimensions.")
			.mandatory().int_param("width")
				.description("Width of each glyph in pixels.")
				.min(1)
				.max(64)
			.mandatory().int_param("height")
				.description("Height of each glyph in pixels.")
				.min(1)
				.max(64)
			.mandatory().int_param("count")
				.description("Number of glyphs in the font.")
				.min(1)
				.max(65535)
			.action(
				[](const auto &context, auto &, auto &, auto &)
				{
					const auto width = context.integer_value_of("width");
					const auto height = context.integer_value_of("height");
					const auto count = context.integer_value_of("count");
					run_fonted_on_blank_custom_font(width, height, count);
				});
		// clang-format on
	}

	auto Fonted_App::register_export_command(Command_Line_Parser_Builder &commands) const -> void
	{
		// clang-format off
		commands.command("export")
			.description("Export from fonted's native file format to a foreign format.")
			.mandatory().text_param("format")
				.description("Format to be exported.")
				.one_of("binary")
			.mandatory().text_param("source")
				.description("Name of the native font file to be exported.")
			.mandatory().text_param("destination")
				.description("Name of the foreign file to be exported to.")
			.action(
				[&](const auto &context, auto &, auto &, auto &)
				{
					// Ignore as we currently support only one format const auto format =
					// context.text_value_from("format");
					const auto source{context.text_value_of("source")};
					const auto destination{context.text_value_of("destination")};
					const auto native_font{native::load(source, m_now)};
					binary::store(*native_font, destination);
				});
		// clang-format on
	}

	auto Fonted_App::register_import_binary_command(Command_Line_Parser_Builder &commands) const -> void
	{
		// clang-format off
		commands.command("import")
			.description("Import from a foreign file format to fonted's native file format. "
				"The import function will will check the amount of binary data against "
				"the glyph width and hight and the expected number of glyphs.")
			.mandatory().text_param("format")
				.description("Format to be imported.")
				.one_of("binary")
			.mandatory().text_param("source")
				.description("Name of the foreign file to be imported.")
			.mandatory().text_param("destination")
				.description("Name of fonted's native file to be saved to.")
			.mandatory().int_param("width")
				.description("Glyph width in pixels.")
			.mandatory().int_param("height")
				.description("Glyph height in pixels.")
			.optional().int_param("count")
				.min(0).max(65535)
				.defaults_to(0)
				.description("Expected number of glyphs. "
					"Import is guessing the glyph count, if left out (or set to 0).")
			.optional().int_param("skip")
				.min(0)
				.defaults_to(0)
				.description("Skip first n bytes from binary. "
					"May be used to ignore header information in an otherwise binary/raw font file.")
			.action(
				[&](const auto &context, auto &, auto &, auto &)
				{
					// Ignore as we currently support only one format const auto format =
					// context.text_value_from("format");
					const auto source{context.text_value_of("source")};
					const auto destination{context.text_value_of("destination")};
					const auto width{context.integer_value_of("width")};
					const auto height{context.integer_value_of("height")};
					const auto count{context.integer_value_of("count")==0
							? nullopt
							: optional(context.integer_value_of("count"))};
					const auto skip{context.integer_value_of("skip")};
					const auto font{binary::load(source, width, height, count, skip, m_now)};
					native::store(*font,destination);
				});
		// clang-format on
	}

	auto Fonted_App::register_script_command(Command_Line_Parser_Builder &commands) const -> void
	{
		// clang-format off
		commands.command("run")
			.description("Load and execute a script to create, modify, import, export, etc. fonts.")
			.mandatory().text_param("script")
				.description("Name of the script file to be executed.")
			.action(
				[&](const auto &context, auto &, auto &, auto &)
				{
					// Ignore as we currently support only one format const auto format =
					// context.text_value_from("format");
					const auto script_file_name{context.text_value_of("script")};
					run_fonted_script(script_file_name, m_now);
				});
		// clang-format on
	}

	auto Fonted_App::register_repl_command(Command_Line_Parser_Builder &commands) const -> void
	{
		// clang-format off
		commands.command("repl")
			.description("Open a REPL shell for interactive scripting")
			.optional().text_param("script")
				.allow_empty_text()
				.defaults_to("")
				.description("Name of the script be executed before interaction with the interpreter.")
			.action(
				[&](const auto &context, auto &in, auto &out, auto &err)
				{
					const auto script_file_name{context.text_value_of("script")};
					run_repl(script_file_name, m_now, in, out, err);
				});
		// clang-format on
	}

} // namespace fonted
