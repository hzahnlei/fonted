// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "glyph_picker.hpp"
#include <fmt/core.h>

namespace fonted
{

	Glyph_Picker_Geometry::Glyph_Picker_Geometry(const Presentation_Model &model)
	{
		column_width = (model.font_dimensions().glyph_width > GLYPH_GRID_WIDTH)
		                               ? model.font_dimensions().glyph_width
		                               : GLYPH_GRID_WIDTH;
		row_height = GLYPH_GRID_HEIGHT + model.font_dimensions().glyph_height;
	}

	auto Glyph_Picker::character_caption(const Glyph_ID c) -> std::string
	{
		switch (c)
		{
		case 0:
			return "NUL";
		case 1:
			return "SOH";
		case 2:
			return "STX";
		case 3:
			return "ETX";
		case 4:
			return "EOT";
		case 5:
			return "ENQ";
		case 6:
			return "ACK";
		case 7:
			return "BEL";
		case 8:
			return "BS";
		case 9:
			return "TAB";
		case 10:
			return "LF";
		case 11:
			return "VT";
		case 12:
			return "FF";
		case 13:
			return "CR";
		case 14:
			return "SO";
		case 15:
			return "SI";
		case 16:
			return "DLE";
		case 17:
			return "DC1";
		case 18:
			return "DC2";
		case 19:
			return "DC3";
		case 20:
			return "DC4";
		case 21:
			return "NAK";
		case 22:
			return "SYN";
		case 23:
			return "ETB";
		case 24:
			return "CAN";
		case 25:
			return "EM";
		case 26:
			return "SUB";
		case 27:
			return "ESC";
		case 28:
			return "FS";
		case 29:
			return "GS";
		case 30:
			return "RS";
		case 31:
			return "US";
		case 127:
			return "DEL";
		default:
			return (c & 0b10000000) ? "---" : std::string{static_cast<char>(c)};
		}
	}

	auto Glyph_Picker::row_and_column(const Presentation_Model &model, const Glyph_Picker_Geometry &geometry,
	                                  const ImVec2 &mouse_pos) -> std::optional<Glyph_Coordinate>
	{
		for (auto row = 0U; row < model.glyph_picker_specification().row_count; row++)
		{
			for (auto column = 0U; column < model.glyph_picker_specification().column_count; column++)
			{
				const ImRect bounding_box{
						{static_cast<float>(column * geometry.column_width) + BORDER_THICKNESS +
				                                 GLYPH_PICKER_CAPTION_WIDTH,
				                 static_cast<float>(row * geometry.row_height) + BORDER_THICKNESS +
				                                 GLYPH_PICKER_CAPTION_HEIGHT},
						{static_cast<float>((column + 1) * geometry.column_width) +
				                                 BORDER_THICKNESS + GLYPH_PICKER_CAPTION_WIDTH,
				                 static_cast<float>((row + 1) * geometry.row_height) +
				                                 BORDER_THICKNESS + GLYPH_PICKER_CAPTION_HEIGHT}};
				if (bounding_box.Contains(mouse_pos))
				{
					return Glyph_Coordinate{.column = column, .row = row};
				}
			}
		}
		return {};
	}

	Glyph_Picker::Glyph_Picker(const ID &id, Presentation_Model &model, View_Model &view)
			: Base_Widget{id}, m_dimensions{model}, m_model{model}, m_view{view}
	{
	}

	//---- Interface methods ------------------------------------------------------

	auto Glyph_Picker::widget_size() const -> Size
	{
		m_dimensions = Glyph_Picker_Geometry(m_model);
		return Size{static_cast<float>(m_model.glyph_picker_specification().column_count *
		                               m_dimensions.column_width) +
		                            GLYPH_PICKER_CAPTION_WIDTH + (2 * BORDER_THICKNESS),
		            static_cast<float>(m_model.glyph_picker_specification().row_count *
		                               m_dimensions.row_height) +
		                            GLYPH_PICKER_CAPTION_HEIGHT + (2 * BORDER_THICKNESS)};
	}

	auto Glyph_Picker::operator()() -> void
	{
		auto &window{*ImGui::GetCurrentWindow()};
		if (window.SkipItems)
		{
			return;
		}
		const auto imgui_id{window.GetID(id().c_str())};
		const auto effective_size{ImGui::CalcItemSize(widget_size(), 0.0f, 0.0f)};
		const auto pos{window.DC.CursorPos};
		const ImRect bounding_box{pos, pos + effective_size};
		ImGui::ItemSize(effective_size);
		if (!ImGui::ItemAdd(bounding_box, imgui_id))
		{
			throw std::runtime_error{
					fmt::format("Failed to add glyph picker widget id='{}' to window.", id())};
		}
		bool hovered;
		bool held;
		const auto pressed{ImGui::ButtonBehavior(bounding_box, imgui_id, &hovered, &held,
		                                         ImGuiButtonFlags_::ImGuiButtonFlags_MouseButtonLeft)};
		const auto &g{*GImGui};
		IMGUI_TEST_ENGINE_ITEM_INFO(id, m_id.c_str(),
		                            g.LastItemData.StatusFlags); // This macro assumes g to be defined!!!
		if (pressed)
		{
			const auto effective_mouse_pos{ImGui::GetIO().MouseClickedPos[0] - bounding_box.Min};
			const auto glyph_coordinate{row_and_column(m_model, m_dimensions, effective_mouse_pos)};
			if (glyph_coordinate.has_value())
			{
				const auto glyph_id{m_model.glyph_id(*glyph_coordinate)};
				if (glyph_id < m_model.font_dimensions().glyph_count)
				{
					m_model.set_active_glyph(m_model.glyph_id(*glyph_coordinate));
				}
			}
		}
		else if (hovered)
		{
			const auto effective_mouse_pos{ImGui::GetIO().MousePos - bounding_box.Min};
			const auto glyph_coordinate{row_and_column(m_model, m_dimensions, effective_mouse_pos)};
			if (glyph_coordinate.has_value())
			{
				draw_cross_hair(*glyph_coordinate, bounding_box, window);
			}
		}
		draw(bounding_box, window);
	}

	//---- Helper methods ---------------------------------------------------------

	auto Glyph_Picker::draw(const ImRect &bounding_box, ImGuiWindow &window) -> void
	{
		auto &draw_list{*window.DrawList};
		const ImVec2 upper_left_corner{bounding_box.Min};
		const ImVec2 lower_right_corner{bounding_box.Max};
		draw_border(upper_left_corner, lower_right_corner, draw_list);
		draw_grid_overlay(upper_left_corner, lower_right_corner, draw_list);
		draw_row_captions(upper_left_corner, lower_right_corner, draw_list);
		draw_column_captions(upper_left_corner, lower_right_corner, draw_list);
		draw_glyphs(upper_left_corner, draw_list);
		draw_animations(upper_left_corner, draw_list);
	}

	auto Glyph_Picker::draw_border(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                               ImDrawList &draw_list) const -> void
	{
		draw_list.AddRect(upper_left_corner, lower_right_corner, BORDER_COLOR, SHARP_EDGES,
		                  ImDrawFlags_::ImDrawFlags_RoundCornersNone, BORDER_THICKNESS);
	}

	auto Glyph_Picker::draw_grid_overlay(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                                     ImDrawList &draw_list) const -> void
	{
		draw_row_separators(upper_left_corner, lower_right_corner, draw_list);
		draw_column_separators(upper_left_corner, lower_right_corner, draw_list);
	}

	auto Glyph_Picker::draw_row_separators(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                                       ImDrawList &draw_list) const -> void
	{
		for (auto row = 1U; row < m_model.glyph_picker_specification().row_count; row++)
		{
			const ImVec2 pos_from{upper_left_corner.x + BORDER_THICKNESS,
			                      upper_left_corner.y + BORDER_THICKNESS +
			                                      static_cast<float>(row * m_dimensions.row_height) +
			                                      GLYPH_PICKER_CAPTION_HEIGHT};
			const ImVec2 pos_to{lower_right_corner.x - BORDER_THICKNESS,
			                    upper_left_corner.y + BORDER_THICKNESS +
			                                    static_cast<float>(row * m_dimensions.row_height) +
			                                    GLYPH_PICKER_CAPTION_HEIGHT};
			draw_list.AddLine(pos_from, pos_to, SEPARATOR_COLOR, SEPARATOR_THICKNESS);
		}
	}

	auto Glyph_Picker::draw_column_separators(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                                          ImDrawList &draw_list) const -> void
	{
		for (auto column = 1U; column < m_model.glyph_picker_specification().column_count; column++)
		{
			const ImVec2 pos_from{upper_left_corner.x + BORDER_THICKNESS +
			                                      static_cast<float>(column * m_dimensions.column_width) +
			                                      GLYPH_PICKER_CAPTION_WIDTH,
			                      upper_left_corner.y + BORDER_THICKNESS};
			const ImVec2 pos_to{upper_left_corner.x + BORDER_THICKNESS +
			                                    static_cast<float>(column * m_dimensions.column_width) +
			                                    GLYPH_PICKER_CAPTION_WIDTH,
			                    lower_right_corner.y - BORDER_THICKNESS};
			draw_list.AddLine(pos_from, pos_to, SEPARATOR_COLOR, SEPARATOR_THICKNESS);
		}
	}

	auto Glyph_Picker::draw_row_captions(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                                     ImDrawList &draw_list) const -> void
	{
		const ImVec2 pos_from{upper_left_corner.x, upper_left_corner.y + GLYPH_PICKER_CAPTION_HEIGHT};
		const ImVec2 pos_to{lower_right_corner.x, upper_left_corner.y + GLYPH_PICKER_CAPTION_HEIGHT};
		draw_list.AddLine(pos_from, pos_to, BORDER_COLOR, BORDER_THICKNESS);
		for (auto row = 0U; row < m_model.glyph_picker_specification().row_count; row++)
		{
			const ImVec2 pos{upper_left_corner.x + BORDER_THICKNESS,
			                 upper_left_corner.y + BORDER_THICKNESS +
			                                 static_cast<float>(row * m_dimensions.row_height) +
			                                 GLYPH_PICKER_CAPTION_HEIGHT};
			const auto caption =
					fmt::format("{:#04x}", row * m_model.glyph_picker_specification().column_count);
			draw_list.AddText(pos, CAPTION_TEXT_COLOR, caption.c_str());
		}
	}

	auto Glyph_Picker::draw_column_captions(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                                        ImDrawList &draw_list) const -> void
	{
		const ImVec2 pos_from{upper_left_corner.x + GLYPH_PICKER_CAPTION_WIDTH, upper_left_corner.y};
		const ImVec2 pos_to{upper_left_corner.x + GLYPH_PICKER_CAPTION_WIDTH, lower_right_corner.y};
		draw_list.AddLine(pos_from, pos_to, BORDER_COLOR, BORDER_THICKNESS);
		for (auto col = 0U; col < m_model.glyph_picker_specification().column_count; col++)
		{
			const ImVec2 pos{upper_left_corner.x + BORDER_THICKNESS + GLYPH_PICKER_CAPTION_WIDTH +
			                                 static_cast<float>(col * m_dimensions.column_width) +
			                                 CAPTION_LEFT_DISTANCE,
			                 upper_left_corner.y + BORDER_THICKNESS};
			const auto caption = fmt::format("{:#04x}", col);
			draw_list.AddText(pos, CAPTION_TEXT_COLOR, caption.c_str());
		}
	}

	auto Glyph_Picker::draw_glyphs(const ImVec2 &upper_left_corner, ImDrawList &draw_list) const -> void
	{
		for (auto row = 0U; row < m_model.glyph_picker_specification().row_count; row++)
		{
			for (auto column = 0U; column < m_model.glyph_picker_specification().column_count; column++)
			{
				const auto glyph_id{m_model.glyph_id(Glyph_Coordinate{.column = column, .row = row})};
				if (glyph_id < m_model.font_dimensions().glyph_count)
				{
					//---- Reference glyph from ASCII character set
					const ImVec2 pos{upper_left_corner.x + BORDER_THICKNESS +
					                                 GLYPH_PICKER_CAPTION_WIDTH +
					                                 static_cast<float>(column *
					                                                    m_dimensions.column_width) +
					                                 (2 * CAPTION_LEFT_DISTANCE),
					                 upper_left_corner.y + BORDER_THICKNESS +
					                                 GLYPH_PICKER_CAPTION_HEIGHT +
					                                 static_cast<float>(row *
					                                                    m_dimensions.row_height)};
					draw_list.AddText(pos, ASCII_GLYPH_COLOR, character_caption(glyph_id).c_str());
					//---- Actual glyph
					const auto uv{m_view.uv_coordinates(glyph_id)};
					const auto from_pos{pos + ImVec2{10, 15}};
					const auto to_pos{from_pos +
					                  ImVec2{static_cast<float>(
										 m_model.font_dimensions().glyph_width),
					                         static_cast<float>(m_model.font_dimensions()
					                                                            .glyph_height)}};
					draw_list.AddImage(m_view.font_texture_specification().id, from_pos, to_pos,
					                   uv.Min, uv.Max);
				}
				else
				{
					const ImVec2 pos_from{
							upper_left_corner.x + BORDER_THICKNESS + GLYPH_GRID_WIDTH +
									static_cast<float>(column *
					                                                   m_dimensions.column_width),
							upper_left_corner.y + BORDER_THICKNESS + GLYPH_GRID_HEIGHT +
									static_cast<float>(row *
					                                                   m_dimensions.row_height)};
					const ImVec2 pos_to{pos_from.x + static_cast<float>(m_dimensions.row_height),
					                    pos_from.y + static_cast<float>(m_dimensions.column_width)};
					draw_list.AddLine(pos_from, pos_to, SEPARATOR_COLOR);
				}
			}
		}
	}

	constexpr auto AMINATION_LENGTH = 30U;

	auto Glyph_Picker::draw_animations(const ImVec2 &upper_left_corner, ImDrawList &draw_list) -> void
	{
		const auto frame_count{ImGui::GetFrameCount()};
		if (frame_count > static_cast<int>(m_frame_count + 2 * AMINATION_LENGTH))
		{
			m_frame_count = frame_count;
		}
		else if (frame_count > static_cast<int>(m_frame_count + AMINATION_LENGTH))
		{
			const auto glyph_coordinate{m_model.row_and_column(m_model.active_glyph_id())};
			const ImVec2 pos_from{upper_left_corner.x + BORDER_THICKNESS + GLYPH_PICKER_CAPTION_WIDTH +
			                                      static_cast<float>(glyph_coordinate.column *
			                                                         m_dimensions.column_width),
			                      upper_left_corner.y + BORDER_THICKNESS + GLYPH_PICKER_CAPTION_HEIGHT +
			                                      static_cast<float>(glyph_coordinate.row *
			                                                         m_dimensions.row_height)};
			const ImVec2 pos_to{upper_left_corner.x + BORDER_THICKNESS + GLYPH_PICKER_CAPTION_WIDTH +
			                                    static_cast<float>((glyph_coordinate.column + 1) *
			                                                       m_dimensions.column_width),
			                    upper_left_corner.y + BORDER_THICKNESS + GLYPH_PICKER_CAPTION_HEIGHT +
			                                    static_cast<float>((glyph_coordinate.row + 1) *
			                                                       m_dimensions.row_height)};
			draw_list.AddRectFilled(pos_from, pos_to, ACTIVE_GLYPH_HIGHLIGHT_COLOR, SHARP_EDGES,
			                        ImDrawFlags_::ImDrawFlags_RoundCornersNone);
			draw_list.AddRect(pos_from, pos_to, ACTIVE_GLYPH_FRAME_COLOR, SHARP_EDGES,
			                  ImDrawFlags_::ImDrawFlags_RoundCornersNone, ACTIVE_GLYPH_FRAME_THICKNESS);
		}
	}

	inline auto draw_vertical_cross_hair(const auto &upper_left_corner, const auto &lower_right_corner,
	                                     const auto &glyph_picker_dimensions, auto &draw_list,
	                                     const auto glyph_column)
	{
		const ImVec2 pos_from{
				upper_left_corner.x + BORDER_THICKNESS + GLYPH_PICKER_CAPTION_WIDTH +
						static_cast<float>(glyph_column * glyph_picker_dimensions.column_width),
				upper_left_corner.y + BORDER_THICKNESS};
		const ImVec2 pos_to{upper_left_corner.x + BORDER_THICKNESS + GLYPH_PICKER_CAPTION_WIDTH +
		                                    static_cast<float>((glyph_column + 1) *
		                                                       glyph_picker_dimensions.column_width),
		                    lower_right_corner.y - BORDER_THICKNESS};
		draw_list.AddRectFilled(pos_from, pos_to, GLYPH_CROSS_HAIR_COLOR, SHARP_EDGES,
		                        ImDrawFlags_::ImDrawFlags_RoundCornersNone);
	}

	inline auto draw_horizontal_cross_hair(const auto &upper_left_corner, const auto &lower_right_corner,
	                                       const auto &glyph_picker_dimensions, auto &draw_list,
	                                       const auto glyph_row)
	{
		const ImVec2 pos_from{
				upper_left_corner.x + BORDER_THICKNESS,
				upper_left_corner.y + BORDER_THICKNESS + GLYPH_PICKER_CAPTION_HEIGHT +
						static_cast<float>(glyph_row * glyph_picker_dimensions.row_height)};
		const ImVec2 pos_to{lower_right_corner.x - BORDER_THICKNESS,
		                    upper_left_corner.y + BORDER_THICKNESS + GLYPH_PICKER_CAPTION_HEIGHT +
		                                    static_cast<float>((glyph_row + 1) *
		                                                       glyph_picker_dimensions.row_height)};
		draw_list.AddRectFilled(pos_from, pos_to, GLYPH_CROSS_HAIR_COLOR, SHARP_EDGES,
		                        ImDrawFlags_::ImDrawFlags_RoundCornersNone);
	}

	auto Glyph_Picker::draw_cross_hair(const Glyph_Coordinate &glyph_coordinate, const ImRect &bounding_box,
	                                   ImGuiWindow &window) const -> void
	{
		auto &draw_list{*window.DrawList};
		const ImVec2 upper_left_corner{bounding_box.Min};
		const ImVec2 lower_right_corner{bounding_box.Max};
		draw_vertical_cross_hair(upper_left_corner, lower_right_corner, m_dimensions, draw_list,
		                         glyph_coordinate.column);
		draw_horizontal_cross_hair(upper_left_corner, lower_right_corner, m_dimensions, draw_list,
		                           glyph_coordinate.row);
	}

} // namespace fonted
