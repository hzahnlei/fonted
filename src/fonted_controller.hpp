// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "column_layout.hpp"
#include "fonted_view.hpp"
#include "glyph_edit_controls.hpp"
#include "glyph_editor.hpp"
#include "glyph_picker.hpp"
#include "icon_button.hpp"
#include "menu_bar.hpp"
#include "presentation_model.hpp"
#include "row_layout.hpp"
#include "use_stl.hpp"

namespace fonted
{

	class Fonted_Controller final
	{
	    private:
		Presentation_Model &m_presentation_model;
		View_Model &m_view_model;
		std::unique_ptr<Widget> m_main_view = nullptr;
		Stack_Frame &m_stack;

	    public:
		Fonted_Controller(Presentation_Model &, View_Model &, Stack_Frame &);
		~Fonted_Controller() = default;
		auto run() -> void;

	    private:
		auto init_widgets() -> void;
		[[nodiscard]] auto should_run() const noexcept -> bool;
		auto resize_window() noexcept -> void;
	};

	inline auto Fonted_Controller::should_run() const noexcept -> bool { return !m_view_model.should_close(); }

	inline auto Fonted_Controller::resize_window() noexcept -> void
	{
		const auto required_size{m_main_view->widget_size()};
		const auto &style{ImGui::GetStyle()};
		// Widgets padded because of ImGui::Begin(), so add extra size to what is required by the widgets.
		m_view_model.set_window_size(
				static_cast<View_Model::Width_Pixels>(required_size.x + (2 * style.WindowPadding.x)),
				static_cast<View_Model::Height_Pixels>(required_size.y + (2 * style.WindowPadding.y)));
	}

} // namespace fonted
