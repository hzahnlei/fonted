// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "row_layout.hpp"
#include <fmt/core.h>
#include <imgui.h>

namespace fonted
{

	Row_Layout::Row_Layout(const ID &id, const Surrounding_Border border_padding)
			: Base_Widget{id}, m_border_padding{border_padding}
	{
	}

	//---- Interface methods ------------------------------------------------------

	auto Row_Layout::widget_size() const -> Size
	{
		auto sum_width{0.0f};
		auto max_height{0.0f};
		for (const auto &child : m_children)
		{
			const auto child_size{child->widget_size()};
			sum_width += child_size.x;
			max_height = (child_size.y > max_height) ? child_size.y : max_height;
		}
		// Seems like ImGui always pads at the X axis, if ImGui::BeginChild is involved. Therefore, we add
		// style().WindowPadding.x to this widget's width.
		const auto padding_width{style().WindowPadding.x};
		const auto padding_height{style().WindowPadding.y};
		return Size{sum_width + (pad_border() ? 2 * padding_width : 0) + padding_width,
		            max_height + (pad_border() ? 2 * padding_height : 0)};
	}

	auto Row_Layout::operator()() -> void
	{
		if (ImGui::GetCurrentWindow()->SkipItems)
		{
			return;
		}

		const auto my_size{widget_size()};

#ifdef FONTED_DEBUG_DRAWING
		auto &window{*ImGui::GetCurrentWindow()};
		auto &draw_list{*window.DrawList};
		const ImRect bounding_box{window.DC.CursorPos, window.DC.CursorPos + my_size};
		draw_list.AddRectFilled(bounding_box.Min, bounding_box.Max, DEBUG_BACKGROUND_COLOR);
#endif

		ImGui::BeginChild(id().c_str(), my_size, NO_BORDER,
		                  pad_border() ? ImGuiWindowFlags_::ImGuiWindowFlags_AlwaysUseWindowPadding
		                               : ImGuiWindowFlags_::ImGuiWindowFlags_None);
		auto i{0U};
		for (const auto &child : m_children)
		{
			if (i > 0)
			{
				ImGui::SameLine();
			}
			(*child)();
			i++;
		}
		ImGui::EndChild();

#ifdef FONTED_DEBUG_DRAWING
		draw_list.AddRect(bounding_box.Min, bounding_box.Max, DEBUG_BORDER_COLOR);
#endif
	}

} // namespace fonted
