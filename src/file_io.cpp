// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "file_io.hpp"

#include "binary_representation.hpp"
#include "native_representation.hpp"
#include "use_stl.hpp"

namespace fonted::native
{

	auto load(const string &file_name, const Font_Model::Now_Function &now) -> unique_ptr<Domain_Model>
	{
		return native::font_model(YAML::LoadFile(file_name), now);
	}

	auto store(const Domain_Model &font, const string &file_name) -> void
	{
		ofstream file;
		file.open(file_name);
		file << native::representation(font) << '\n';
		file.close();
	}

} // namespace fonted::native

namespace fonted::binary
{

	auto load(const string &file_name, const Glyph_Width width, const Glyph_Height height,
	          const optional<Glyph_Count> count, const size_t offset, const Font_Model::Now_Function &now)
			-> unique_ptr<Domain_Model>
	{
		ifstream file{file_name, ios::in | ios::binary};
		Bytes bytes;
		char byte{0U};
		while (!file.eof())
		{
			file.read(&byte, sizeof(Byte));
			if (!file.eof())
			{
				bytes.emplace_back((Byte)byte);
			}
		}
		return binary::font_model(bytes, width, height, count, offset, now);
	}

	auto store(const Domain_Model &font, const string &file_name) -> void
	{
		ofstream file;
		file.open(file_name);
		for (const auto byte : binary::representation(font))
		{
			file << (unsigned char)byte;
		}
		file.close();
	}

} // namespace fonted::binary
