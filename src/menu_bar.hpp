// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "menu_bar.hpp"
#include "presentation_model.hpp"
#include "use_stl.hpp"
#include "widget_commons.hpp"
#include <appsl/appsl.hpp>

namespace fonted
{

	using appsl::Stack_Frame;

	using Key_Action = function<void(Presentation_Model &)>;

	struct Menu_Item final
	{
		string display_name;
		ImGuiKey key;
		ImGuiKey modifier = ImGuiMod_None;
		Key_Action action;
	};

	using Menu_Items = vector<Menu_Item>;

	class Menu_Bar final : public Base_Widget
	{
	    public:
		static constexpr auto MAX_MENU_ITEMS{8U};

	    private:
		Presentation_Model &m_presentation_model;
		Menu_Items m_menu_items;
		Stack_Frame &m_stack;

	    public:
		Menu_Bar(const ID &, Presentation_Model &, Menu_Items &&, Stack_Frame &);
		~Menu_Bar() override = default;

		[[nodiscard]] auto widget_size() const -> Size override;
		auto operator()() -> void override;
	};

} // namespace fonted
