// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "glyph_editor.hpp"
#include <fmt/core.h>

namespace fonted
{

	auto Glyph_Editor::required_widget_size(const Presentation_Model &model) -> Size
	{
		const auto glyp_width{Widget::Width(model.font_dimensions().glyph_width)};
		const auto glyp_height{Widget::Width(model.font_dimensions().glyph_height)};
		return Size{(glyp_width * PIXEL_BOX_SIZE) + (2 * BORDER_THICKNESS),
		            (glyp_height * PIXEL_BOX_SIZE) + (2 * BORDER_THICKNESS)};
	}

	auto Glyph_Editor::row_and_column(const Presentation_Model &model, const ImVec2 &mouse_pos)
			-> std::optional<Pixel_Coordinate>
	{
		for (auto row = 0U; row < model.font_dimensions().glyph_height; row++)
		{
			const auto row_pixels{static_cast<float>(row)};
			for (auto column = 0U; column < model.font_dimensions().glyph_width; column++)
			{
				const auto column_pixels{static_cast<float>(column)};
				const ImRect bounding_box{{(column_pixels * PIXEL_BOX_SIZE) + BORDER_THICKNESS,
				                           (row_pixels * PIXEL_BOX_SIZE) + BORDER_THICKNESS},
				                          {((column_pixels + 1) * PIXEL_BOX_SIZE) + BORDER_THICKNESS,
				                           ((row_pixels + 1) * PIXEL_BOX_SIZE) + BORDER_THICKNESS}};
				if (bounding_box.Contains(mouse_pos))
				{
					return Pixel_Coordinate{row, column};
				}
			}
		}
		return {};
	}

	Glyph_Editor::Glyph_Editor(const ID &id, Presentation_Model &model) : Base_Widget{id}, m_model{model} {}

	//---- Interface methods ------------------------------------------------------

	auto Glyph_Editor::operator()() -> void
	{
		auto &window{*ImGui::GetCurrentWindow()};
		if (window.SkipItems)
		{
			return;
		}

		const auto imgui_id{window.GetID(id().c_str())};
		const auto effective_size{ImGui::CalcItemSize(widget_size(), 0.0f, 0.0f)};
		const auto pos{window.DC.CursorPos};

		const ImRect bounding_box{pos, pos + effective_size};

#ifdef FONTED_DEBUG_DRAWING
		auto &draw_list{*window.DrawList};
		draw_list.AddRectFilled(bounding_box.Min, bounding_box.Max, DEBUG_BACKGROUND_COLOR);
#endif

		ImGui::ItemSize(effective_size);
		if (!ImGui::ItemAdd(bounding_box, imgui_id))
		{
			throw std::runtime_error{
					fmt::format("Failed to add glyph editor widget id='{}' to window.", id())};
		}
		bool hovered;
		bool held;
		const auto pressed{ImGui::ButtonBehavior(bounding_box, imgui_id, &hovered, &held,
		                                         ImGuiButtonFlags_::ImGuiButtonFlags_MouseButtonLeft)};
		const auto &g{*GImGui};
		IMGUI_TEST_ENGINE_ITEM_INFO(imgui_id, id().c_str(),
		                            g.LastItemData.StatusFlags); // This macro assumes g to be defined!!!
		if (pressed)
		{
			const auto effective_mouse_pos{ImGui::GetIO().MouseClickedPos[0] - bounding_box.Min};
			const auto pixel{row_and_column(m_model, effective_mouse_pos)};
			if (pixel.has_value())
			{
				m_model.on_active_glyph_toggle(*pixel);
			}
		}
		else if (hovered)
		{
			const auto effective_mouse_pos{ImGui::GetIO().MousePos - bounding_box.Min};
			const auto pixel_coordinate{row_and_column(m_model, effective_mouse_pos)};
			if (pixel_coordinate.has_value())
			{
				draw_cross_hair(*pixel_coordinate, bounding_box, window);
			}
		}
		draw(bounding_box, window);

#ifdef FONTED_DEBUG_DRAWING
		draw_list.AddRect(bounding_box.Min, bounding_box.Max, DEBUG_BORDER_COLOR);
#endif
	}

	//---- Helper methods ---------------------------------------------------------

	auto Glyph_Editor::draw(const ImRect &bounding_box, ImGuiWindow &window) const -> void
	{
		auto &draw_list{*window.DrawList};
		const ImVec2 upper_left_corner{bounding_box.Min};
		const ImVec2 lower_right_corner{bounding_box.Max};
		draw_border(upper_left_corner, lower_right_corner, draw_list);
		draw_pixels(upper_left_corner, draw_list);
		draw_grid_overlay(upper_left_corner, lower_right_corner, draw_list);
	}

	auto Glyph_Editor::draw_border(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                               ImDrawList &draw_list) const -> void
	{
		// AddRect does not draw pixel-perfect. Therefore the coordinates are tweaked so that the border lies
		// exactly on the edges of the bounding box.
		draw_list.AddRect(upper_left_corner + ImVec2{0, BORDER_OFFSET},
		                  lower_right_corner - ImVec2{BORDER_OFFSET, BORDER_OFFSET}, BORDER_COLOR, SHARP_EDGES,
		                  ImDrawFlags_::ImDrawFlags_RoundCornersNone, BORDER_THICKNESS);
	}

	auto Glyph_Editor::draw_pixels(const ImVec2 &upper_left_corner, ImDrawList &draw_list) const -> void
	{
		for (auto row = 0U; row < m_model.font_dimensions().glyph_height; row++)
		{
			for (auto column = 0U; column < m_model.font_dimensions().glyph_width; column++)
			{
				if (m_model.is_set_on_active_glyph(Pixel_Coordinate{.row = row, .column = column}))
				{
					const auto row_pixels{static_cast<float>(row)};
					const auto column_pixels{static_cast<float>(column)};
					const ImVec2 pos_from{upper_left_corner.x + BORDER_THICKNESS +
					                                      (column_pixels * PIXEL_BOX_SIZE),
					                      upper_left_corner.y + BORDER_THICKNESS +
					                                      (row_pixels * PIXEL_BOX_SIZE)};
					const ImVec2 pos_to{upper_left_corner.x + BORDER_THICKNESS +
					                                    ((column_pixels + 1) * PIXEL_BOX_SIZE),
					                    upper_left_corner.y + BORDER_THICKNESS +
					                                    ((row_pixels + 1) * PIXEL_BOX_SIZE)};
					draw_list.AddRectFilled(pos_from, pos_to, PIXEL_SET_COLOR, SHARP_EDGES,
					                        ImDrawFlags_::ImDrawFlags_RoundCornersNone);
				}
			}
		}
	}

	auto Glyph_Editor::draw_grid_overlay(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                                     ImDrawList &draw_list) const -> void
	{
		draw_grid_horizontal_lines(upper_left_corner, lower_right_corner, draw_list);
		draw_grid_vertical_lines(upper_left_corner, lower_right_corner, draw_list);
	}

	auto Glyph_Editor::draw_grid_horizontal_lines(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                                              ImDrawList &draw_list) const -> void
	{
		for (auto row = 1U; row < m_model.font_dimensions().glyph_height; row++)
		{
			const auto row_pixels{static_cast<float>(row)};
			const ImVec2 pos_from{upper_left_corner.x + BORDER_THICKNESS,
			                      upper_left_corner.y + BORDER_THICKNESS + (row_pixels * PIXEL_BOX_SIZE)};
			const ImVec2 pos_to{lower_right_corner.x - BORDER_THICKNESS,
			                    upper_left_corner.y + BORDER_THICKNESS + (row_pixels * PIXEL_BOX_SIZE)};
			draw_list.AddLine(pos_from, pos_to, SEPARATOR_COLOR, SEPARATOR_THICKNESS);
		}
	}

	auto Glyph_Editor::draw_grid_vertical_lines(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                                            ImDrawList &draw_list) const -> void
	{
		for (auto column = 1U; column < m_model.font_dimensions().glyph_width; column++)
		{
			const auto column_pixels{static_cast<float>(column)};
			const ImVec2 pos_from{upper_left_corner.x + BORDER_THICKNESS + (column_pixels * PIXEL_BOX_SIZE),
			                      upper_left_corner.y + BORDER_THICKNESS};
			const ImVec2 pos_to{upper_left_corner.x + BORDER_THICKNESS + (column_pixels * PIXEL_BOX_SIZE),
			                    lower_right_corner.y - BORDER_THICKNESS};
			draw_list.AddLine(pos_from, pos_to, SEPARATOR_COLOR, SEPARATOR_THICKNESS);
		}
	}

	inline auto draw_vertical_cross_hair(const auto &upper_left_corner, const auto &lower_right_corner,
	                                     const float pixel_coord_column, auto &draw_list)
	{
		const ImVec2 pos_from{upper_left_corner.x + BORDER_THICKNESS + (pixel_coord_column * PIXEL_BOX_SIZE),
		                      upper_left_corner.y + BORDER_THICKNESS};
		const ImVec2 pos_to{upper_left_corner.x + BORDER_THICKNESS +
		                                    ((pixel_coord_column + 1) * PIXEL_BOX_SIZE),
		                    lower_right_corner.y - BORDER_THICKNESS};
		draw_list.AddRectFilled(pos_from, pos_to, PIXEL_CROSS_HAIR_COLOR, SHARP_EDGES,
		                        ImDrawFlags_::ImDrawFlags_RoundCornersNone);
	}

	inline auto draw_horizontal_cross_hair(const auto &upper_left_corner, const auto &lower_right_corner,
	                                       const float pixel_coord_row, auto &draw_list)
	{
		const ImVec2 pos_from{upper_left_corner.x + BORDER_THICKNESS,
		                      upper_left_corner.y + BORDER_THICKNESS + (pixel_coord_row * PIXEL_BOX_SIZE)};
		const ImVec2 pos_to{lower_right_corner.x - BORDER_THICKNESS,
		                    upper_left_corner.y + BORDER_THICKNESS + ((pixel_coord_row + 1) * PIXEL_BOX_SIZE)};
		draw_list.AddRectFilled(pos_from, pos_to, PIXEL_CROSS_HAIR_COLOR, SHARP_EDGES,
		                        ImDrawFlags_::ImDrawFlags_RoundCornersNone);
	}

	auto Glyph_Editor::draw_cross_hair(const Pixel_Coordinate &pixel_coordinate, const ImRect &bounding_box,
	                                   ImGuiWindow &window) const -> void
	{
		auto &draw_list{*window.DrawList};
		const ImVec2 upper_left_corner{bounding_box.Min};
		const ImVec2 lower_right_corner{bounding_box.Max};
		draw_vertical_cross_hair(upper_left_corner, lower_right_corner,
		                         static_cast<float>(pixel_coordinate.column), draw_list);
		draw_horizontal_cross_hair(upper_left_corner, lower_right_corner,
		                           static_cast<float>(pixel_coordinate.row), draw_list);
	}

} // namespace fonted
