// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "grid_layout.hpp"
#include "use_stl.hpp"
#include <fmt/core.h>
#include <imgui.h>

namespace fonted
{

	Grid_Layout::Grid_Layout(const ID &id, const Style_Accessor &style, const Surrounding_Border border_padding,
	                         const Individual_Widgets widget_padding)
			: Base_Widget{id, style}, m_border_padding{border_padding}, m_individual_padding{widget_padding}
	{
	}

	//---- Interface methods ------------------------------------------------------

	auto Grid_Layout::widget_size() const -> Size
	{
		const auto padding_width{style().WindowPadding.x};
		const auto padding_height{style().WindowPadding.y};
		const auto padded_child_width{pad_individual_widgets() ? (m_max_child_width + padding_width)
		                                                       : m_max_child_width};
		const auto padded_child_height{pad_individual_widgets() ? (m_max_child_height + padding_height)
		                                                        : m_max_child_height};
		return Size{(padded_child_width * static_cast<float>(m_column_count - 1)) + m_max_child_width +
		                            (pad_border() ? 2 * padding_width : 0),
		            (padded_child_height * static_cast<float>(m_row_count - 1)) + m_max_child_height +
		                            (pad_border() ? 2 * padding_height : 0)};
	}

	auto Grid_Layout::operator()() -> void
	{
		if (ImGui::GetCurrentWindow()->SkipItems)
		{
			return;
		}

#ifdef FONTED_DEBUG_DRAWING
		auto &window{*ImGui::GetCurrentWindow()};
		auto &draw_list{*window.DrawList};
		const ImRect bounding_box{window.DC.CursorPos, window.DC.CursorPos + widget_size()};
		draw_list.AddRectFilled(bounding_box.Min, bounding_box.Max, DEBUG_BACKGROUND_COLOR);
#endif

		ImGui::BeginChild(id().c_str(), widget_size(), NO_BORDER,
		                  pad_border() ? ImGuiWindowFlags_::ImGuiWindowFlags_AlwaysUseWindowPadding
		                               : ImGuiWindowFlags_::ImGuiWindowFlags_None);
		const auto upper_left_x{ImGui::GetCursorPosX()};
		const auto upper_left_y{ImGui::GetCursorPosY()};

		const auto padding_width{style().WindowPadding.x};
		const auto padding_height{style().WindowPadding.y};

		auto y{0.0f};
		for (const auto &row : m_children)
		{
			auto x{0.0f};
			for (const auto &column : row)
			{
				ImGui::SetCursorPosX(upper_left_x +
				                     (x * (m_max_child_width +
				                           (pad_individual_widgets() ? padding_width : 0))));
				ImGui::SetCursorPosY(upper_left_y +
				                     (y * (m_max_child_height +
				                           (pad_individual_widgets() ? padding_height : 0))));
				if (column != nullptr)
				{
					(*column)();
				}
				x++;
			}
			y++;
		}
		ImGui::EndChild();

#ifdef FONTED_DEBUG_DRAWING
		draw_list.AddRect(bounding_box.Min, bounding_box.Max, DEBUG_BORDER_COLOR);
#endif
	}

	auto Grid_Layout::add(const Grid_Row row, const Grid_Column column, std::unique_ptr<Widget> child) -> void
	{
		for (auto i{m_children.size()}; i <= row; i++)
		{
			m_children.emplace_back();
		}
		auto &picked_row{m_children[row]};
		for (auto i{picked_row.size()}; i < column; i++)
		{
			picked_row.emplace_back(nullptr);
		}

		const auto child_size{child->widget_size()};
		m_max_child_width = (child_size.y > m_max_child_width) ? child_size.x : m_max_child_width;
		m_max_child_height = (child_size.y > m_max_child_height) ? child_size.y : m_max_child_height;

		picked_row.emplace_back(std::move(child));

		m_row_count = (row + 1 > m_row_count) ? row + 1 : m_row_count;
		m_column_count = (column + 1 > m_column_count) ? column + 1 : m_column_count;
	}

	auto Grid_Layout::child(const Grid_Row row, const Grid_Column column) const noexcept -> Widget const *
	{
		if (row >= m_children.size())
		{
			return nullptr;
		}
		if (column >= m_children[row].size())
		{
			return nullptr;
		}
		return m_children[row][column].get();
	}

} // namespace fonted
