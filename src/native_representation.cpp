// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "native_representation.hpp"
#include "custom_assert.hpp"
#include "date.h"
#include "fonted_view.hpp"
#include "use_stl.hpp"
#include "version.hpp"
#include <fmt/chrono.h>

namespace fonted::native
{

	//---- Synthesize YAML document -----------------------------------------------

	// Forward declaraions
	auto emit_meta(const Domain_Model &, YAML::Emitter &) -> void;
	auto emit_meta_glyph(const Domain_Model &, YAML::Emitter &) -> void;
	auto emit_meta_font(const Domain_Model &, YAML::Emitter &) -> void;
	auto emit_meta_version(YAML::Emitter &) -> void;
	auto emit_font_data(const Domain_Model &, YAML::Emitter &) -> void;

	auto representation(const Domain_Model &model) -> string
	{
		YAML::Emitter out;
		out << YAML::BeginMap;
		emit_meta(model, out);
		emit_font_data(model, out);
		out << YAML::EndMap;
		return out.c_str();
	}

	auto emit_meta(const Domain_Model &model, YAML::Emitter &out) -> void
	{
		out << YAML::Key << "meta";
		out << YAML::BeginMap;
		emit_meta_version(out);
		emit_meta_glyph(model, out);
		emit_meta_font(model, out);
		out << YAML::EndMap;
	}

	auto emit_meta_glyph(const Domain_Model &model, YAML::Emitter &out) -> void
	{
		out << YAML::Key << "glyph";
		out << YAML::BeginMap;
		out << YAML::Key << "width";
		out << YAML::Value << model.font_dimensions().glyph_width;
		out << YAML::Key << "height";
		out << YAML::Value << model.font_dimensions().glyph_height;
		out << YAML::Key << "count";
		out << YAML::Value << model.font_dimensions().glyph_count;
		out << YAML::EndMap;
	}

	constexpr auto ISO_8601_FORMAT_FOR_WRITING{"{:%FT%T%z}"};

	/**
	 * @brief Need to cut time points to ms resolution, otherwise date.h parse won't parse.
	 */
	auto emit_meta_font(const Domain_Model &model, YAML::Emitter &out) -> void
	{
		out << YAML::Key << "font";
		out << YAML::BeginMap;
		out << YAML::Key << "name";
		out << YAML::Value << model.font_name();
		out << YAML::Key << "author";
		out << YAML::Value << model.author_name();
		out << YAML::Key << "code page";
		out << YAML::Value << model.code_page();
		out << YAML::Key << "description";
		out << YAML::Value << model.description();
		out << YAML::Key << "date of creation";
		out << YAML::Value
		    << fmt::format(ISO_8601_FORMAT_FOR_WRITING,
		                   std::chrono::floor<std::chrono::milliseconds>(model.creation_date()));
		out << YAML::Key << "last updated at";
		out << YAML::Value
		    << fmt::format(ISO_8601_FORMAT_FOR_WRITING,
		                   std::chrono::floor<std::chrono::milliseconds>(model.update_date()));
		out << YAML::Key << "revision count";
		out << YAML::Value << model.revision_count();
		out << YAML::EndMap;
	}

	auto emit_meta_version(YAML::Emitter &out) -> void
	{
		const auto version{(fonted::VERSION_EXTENSION == "")
		                                   ? fmt::format("{}.{}.{}", fonted::VERSION_MAJOR,
		                                                 fonted::VERSION_MINOR, fonted::VERSION_PATCH)
		                                   : fmt::format("{}.{}.{}-{}", fonted::VERSION_MAJOR,
		                                                 fonted::VERSION_MINOR, fonted::VERSION_PATCH,
		                                                 fonted::VERSION_EXTENSION)};
		out << YAML::Key << "version";
		out << YAML::BeginMap;
		out << YAML::Key << "fonted";
		out << YAML::Value << version;
		out << YAML::Key << "file format";
		out << YAML::Value << fonted::FILE_FORMAT_VERSION;
		out << YAML::EndMap;
	}

	auto emit_glyph_data(const Glyph_Model &glyph, YAML::Emitter &out) -> void
	{

		string glyph_representation;
		auto i{0U};
		glyph.for_each_row(
				[&](const auto &row)
				{
					string column_representation;
					row.for_each_column([&](const auto &column)
			                                    { column_representation += column.is_set() ? '@' : '_'; });
					if (i > 0)
					{
						glyph_representation += '\n';
					}
					glyph_representation += column_representation;
					i++;
				});
		out << YAML::BeginMap;
		out << YAML::Key << "glyph";
		out << YAML::Value << YAML::Literal << glyph_representation;
		out << YAML::EndMap;
	}

	auto emit_font_data(const Domain_Model &model, YAML::Emitter &out) -> void
	{
		out << YAML::Key << "font data";
		out << YAML::BeginSeq;
		model.for_each_glyph([&](const auto, const auto &glyph) { emit_glyph_data(glyph, out); });
		out << YAML::EndSeq;
	}

	// ---- Parse YAML document ---------------------------------------------------

	struct Meta final
	{
		struct Glyph final
		{
			Pixel_Count width;
			Pixel_Count height;
			Glyph_Count count;
		};

		struct Font final
		{
			date::sys_time<std::chrono::milliseconds> creation_date;
			date::sys_time<std::chrono::milliseconds> update_date;
			Domain_Model::Revision_Count revision;
			string name;
			string author;
			string code_page;
			string description;
		};

		Glyph glyph;
		Font font;
	};

	// Forward declaraions
	auto parse_meta(const YAML::Node &) -> Meta;
	auto parse_meta_version(const YAML::Node &) -> void;
	auto parse_meta_glyph(const YAML::Node &) -> Meta::Glyph;
	auto parse_meta_font(const YAML::Node &) -> Meta::Font;
	auto parse_date_value(const string &) -> date::sys_time<std::chrono::milliseconds>;
	auto parse_font_data(const YAML::Node &, const Glyph_Count, const Glyph_Width, const Glyph_Height glyph_height,
	                     Domain_Model &) -> void;

	auto font_model(const YAML::Node &yaml_representation, const Font_Model::Now_Function &now)
			-> unique_ptr<Domain_Model>
	{
		const auto meta{parse_meta(yaml_representation)};

		auto model{make_unique<Font_Model>(
				Font_Dimensions{meta.glyph.width, meta.glyph.height, meta.glyph.count}, now,
				meta.font.creation_date, meta.font.update_date, meta.font.revision)};
		model->set_font_name(meta.font.name);
		model->set_author_name(meta.font.author);
		model->set_code_page(meta.font.code_page);
		model->set_description(meta.font.description);

		parse_font_data(yaml_representation, meta.glyph.count, meta.glyph.width, meta.glyph.height, *model);

		return model;
	}

	auto parse_meta(const YAML::Node &yaml_representation) -> Meta
	{
		const auto meta{yaml_representation["meta"]};
		require(meta.IsDefined(), "meta missing");
		require(meta.IsMap(), "meta is expected to be a map");
		parse_meta_version(meta);
		return Meta{
				.glyph = parse_meta_glyph(meta),
				.font = parse_meta_font(meta),
		};
	}

	auto parse_meta_version(const YAML::Node &meta) -> void
	{
		const auto meta_version{meta["version"]};
		if (!meta_version.IsDefined())
		{
			return;
		}
		require(meta_version.IsMap(), "meta/version is expected to be a map");
		const auto meta_version_file{meta_version["file format"]};
		if (!meta_version_file.IsDefined())
		{
			return;
		}
		require(meta_version_file.IsScalar(), "meta/version/file format is expected to be a scalar");
		const auto file_format_version{meta_version_file.as<unsigned int>()};
		require(file_format_version == 1U, "unknown file format version");
	}

	auto parse_meta_glyph(const YAML::Node &meta) -> Meta::Glyph
	{
		const auto meta_glyph{meta["glyph"]};
		require(meta_glyph.IsDefined(), "meta/glyph missing");
		require(meta_glyph.IsMap(), "meta/glyph is expected to be a map");
		const auto meta_glyph_width{meta_glyph["width"]};
		require(meta_glyph_width.IsDefined(), "meta/glyph/width missing");
		require(meta_glyph_width.IsScalar(), "meta/glyph/width is expected to be a scalar");
		const auto glyph_width{meta_glyph_width.as<Pixel_Count>()};
		const auto meta_glyph_height{meta_glyph["height"]};
		require(meta_glyph_height.IsDefined(), "meta/glyph/height missing");
		require(meta_glyph_height.IsScalar(), "meta/glyph/height is expected to be a scalar");
		const auto glyph_height{meta_glyph_height.as<Pixel_Count>()};
		const auto meta_glyph_count{meta_glyph["count"]};
		require(meta_glyph_count.IsDefined(), "meta/glyph/count missing");
		require(meta_glyph_count.IsScalar(), "meta/glyph/count is expected to be a scalar");
		const auto glyph_count{meta_glyph_count.as<Glyph_Count>()};
		return Meta::Glyph{
				.width = glyph_width,
				.height = glyph_height,
				.count = glyph_count,
		};
	}

	auto parse_meta_font(const YAML::Node &meta) -> Meta::Font
	{
		const auto meta_font{meta["font"]};
		if (!meta_font.IsDefined())
		{
			return Meta::Font{};
		}
		require(meta_font.IsMap(), "meta/font is expected to be a map");

		date::sys_time<std::chrono::milliseconds> creation_date;
		const auto meta_font_created{meta_font["date of creation"]};
		if (meta_font_created.IsDefined())
		{
			require(meta_font_created.IsScalar(), "meta/font/date of creation is expected to be a scalar");
			creation_date = parse_date_value(meta_font_created.as<string>());
		}

		date::sys_time<std::chrono::milliseconds> update_date;
		const auto meta_font_updated{meta_font["last updated at"]};
		if (meta_font_updated.IsDefined())
		{
			require(meta_font_updated.IsScalar(), "meta/font/last updated at is expected to be a scalar");
			update_date = parse_date_value(meta_font_updated.as<string>());
		}

		Domain_Model::Revision_Count revision{0U};
		const auto meta_font_revision{meta_font["revision count"]};
		if (meta_font_revision.IsDefined())
		{
			require(meta_font_revision.IsScalar(), "meta/font/revision count is expected to be a scalar");
			revision = meta_font_revision.as<size_t>();
		}

		string font_name;
		const auto meta_font_name{meta_font["name"]};
		if (meta_font_name.IsDefined())
		{
			require(meta_font_name.IsScalar(), "meta/font/name is expected to be a scalar");
			font_name = meta_font_name.as<string>();
		}

		string author_name;
		const auto meta_font_author{meta_font["author"]};
		if (meta_font_author.IsDefined())
		{
			require(meta_font_author.IsScalar(), "meta/font/author is expected to be a scalar");
			author_name = meta_font_author.as<string>();
		}

		string code_page;
		const auto meta_font_code_page{meta_font["code page"]};
		if (meta_font_code_page.IsDefined())
		{
			require(meta_font_code_page.IsScalar(), "meta/font/code page is expected to be a scalar");
			code_page = meta_font_code_page.as<string>();
		}

		string description;
		const auto meta_font_description{meta_font["description"]};
		if (meta_font_description.IsDefined())
		{
			require(meta_font_description.IsScalar(), "meta/font/description is expected to be a scalar");
			description = meta_font_description.as<string>();
		}

		return Meta::Font{
				.creation_date = creation_date,
				.update_date = update_date,
				.revision = revision,
				.name = font_name,
				.author = author_name,
				.code_page = code_page,
				.description = description,
		};
	}

	// constexpr auto ISO_8601_FORMAT_FOR_PARSING{"%FT%T%Ez"};
	constexpr auto ISO_8601_FORMAT_FOR_PARSING{"%FT%T%z"};

	// FIXME my compiler does not fully support C++20, especially chrono::parse.
	// Therefore I am using this open source solution form date.h
	auto parse_date_value(const string &text_representation) -> date::sys_time<std::chrono::milliseconds>
	{
		istringstream date_stream{text_representation};
		date::sys_time<std::chrono::milliseconds> time_point;
		date_stream >> date::parse(ISO_8601_FORMAT_FOR_PARSING, time_point);
		// TODO check for date/time parsing errors
		return date::sys_time<std::chrono::milliseconds>{time_point};
	}

	auto parse_font_data(const YAML::Node &yaml_representation, const Glyph_Count glyph_count,
	                     const Glyph_Width glyph_width, const Glyph_Height glyph_height, Domain_Model &model)
			-> void
	{
		const auto font_data{yaml_representation["font data"]};
		require(font_data.IsDefined(), "font data is missing");
		require(font_data.IsSequence(), "font data is expected to be a sequence");
		require(font_data.size() <= glyph_count, "font data exceeding glyph count");
		require(glyph_count <= font_data.size(), "font data undercutting glyph count");
		for (auto glyph{0U}; glyph < font_data.size(); glyph++)
		{
			const auto glyph_data{font_data[glyph]};
			require(glyph_data.IsDefined(), "glyph data is missing");
			require(glyph_data.IsMap(), "glyph data is expected to be a map");
			const auto glyph_pixels{glyph_data["glyph"]};
			require(glyph_pixels.IsDefined(), "glyph pixel data is missing");
			require(glyph_pixels.IsScalar(), "glyph piel data is expected to be a scalar");
			auto row{0U};
			auto col{0U};
			for (const auto pixel : glyph_pixels.as<string>())
			{
				if (pixel == '\n')
				{
					require(row < glyph_height, "pixels exceeding glyph height");
					row++;
					col = 0U;
				}
				else
				{
					model.glyph(glyph).row(row).column(col).set(pixel == '@');
					require(col < glyph_width, "pixels exceeding glyph width");
					col++;
				}
			}
		}
	}

} // namespace fonted::native
