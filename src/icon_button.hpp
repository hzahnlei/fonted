// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "presentation_model.hpp"
#include "use_stl.hpp"
#include "widget_commons.hpp"
#include <imgui.h>
#include <imgui_internal.h>

namespace fonted
{

	class Icon_Button final : public Base_Widget
	{
	    public:
		enum class Icon
		{
			ARROW_UP,
			ARROW_LEFT,
			ARROW_RIGHT,
			ARROW_DOWN,
			INVERT,
			CLEAR,
			ALL_SET,
			FLIP_HORIZONTALLY,
			FLIP_VERTICALLY,
		};
		using Action = function<void(Presentation_Model &)>;

	    private:
		const Size m_widget_size;
		const Icon m_icon;
		Presentation_Model &m_model;
		const Action m_action;
		enum class Style
		{
			IDLE,
			HIGHLIGHTED,
			PRESSED
		};

	    public:
		Icon_Button(const ID &, const Size, const Icon, Presentation_Model &, const Action &);
		~Icon_Button() override = default;

		[[nodiscard]] auto widget_size() const -> Size override;
		auto operator()() -> void override;

	    private:
		auto draw_pressed(const ImRect &, ImGuiWindow &) const -> void;
		auto draw_highlighted(const ImRect &, ImGuiWindow &) const -> void;
		auto draw_idle(const ImRect &, ImGuiWindow &) const -> void;

		auto draw_background(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner, ImDrawList &,
		                     const ImColor &) const -> void;
		auto draw_border(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner, ImDrawList &,
		                 const ImColor &) const -> void;
		auto draw_icon(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner, ImDrawList &,
		               const ImColor &) const -> void;
	};

	inline auto Icon_Button::widget_size() const -> Size { return m_widget_size; }

} // namespace fonted
