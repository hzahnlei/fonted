// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "use_stl.hpp"
#include "widget_commons.hpp"
#include <imgui.h>

namespace fonted
{

	class Row_Layout final : public Base_Widget
	{
	    public:
		using Children = vector<unique_ptr<Widget>>;

	    private:
		const Surrounding_Border m_border_padding;
		Children m_children;

	    public:
		Row_Layout(const ID &, const Surrounding_Border = Surrounding_Border::PAD);
		~Row_Layout() override = default;

		[[nodiscard]] auto widget_size() const -> Size override;
		auto operator()() -> void override;

		auto add(unique_ptr<Widget>) -> void;

		[[nodiscard]] auto pad_border() const noexcept -> bool;
	};

	inline auto Row_Layout::add(unique_ptr<Widget> child) -> void { m_children.emplace_back(move(child)); }

	inline auto Row_Layout::pad_border() const noexcept -> bool
	{
		return m_border_padding == Surrounding_Border::PAD;
	}

} // namespace fonted
