// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "domain_model.hpp"
#include "use_stl.hpp"
#include <fmt/core.h>

namespace fonted
{

	Font_Dimensions::Font_Dimensions(const Glyph_Width width, const Glyph_Height height, const Glyph_Count count)
			: glyph_width{width}, glyph_height{height}, glyph_count{count}
	{
		if (glyph_width < MIN_GLYPH_WIDTH)
		{
			throw runtime_error{fmt::format("Expect min glyph width of {} pixel but was {}.",
			                                MIN_GLYPH_WIDTH, glyph_width)};
		}
		if (glyph_width > MAX_GLYPH_WIDTH)
		{
			throw runtime_error{fmt::format("Expect max glyph width of {} pixels but was {}.",
			                                MAX_GLYPH_WIDTH, glyph_width)};
		}
		if (glyph_height < MIN_GLYPH_HEIGHT)
		{
			throw runtime_error{fmt::format("Expect min glyph height of {} pixel but was {}.",
			                                MIN_GLYPH_HEIGHT, glyph_height)};
		}
		if (glyph_height > MAX_GLYPH_HEIGHT)
		{
			throw runtime_error{fmt::format("Expect max glyph height of {} pixels but was {}.",
			                                MAX_GLYPH_HEIGHT, glyph_height)};
		}
		if (glyph_count < MIN_GLYPH_COUNT)
		{
			throw runtime_error{fmt::format("Expect min glyph count of {} but was {}.", MIN_GLYPH_COUNT,
			                                glyph_count)};
		}
		if (glyph_count > MAX_GLYPH_COUNT)
		{
			throw runtime_error{fmt::format("Expect max glyph count of {} but was {}.", MAX_GLYPH_COUNT,
			                                glyph_count)};
		}
	}

	auto Column_Model::is_set() const -> bool { return m_value; }

	auto Column_Model::toggle() -> void { m_value = !m_value; }

	auto Column_Model::set(const bool value) -> void { m_value = value; }

	auto Column_Model::clear() -> void { m_value = false; }

	Row_Model::Row_Model(const Pixel_Column column_count)
	{
		for (auto i = 0U; i < column_count; i++)
		{
			m_columns.emplace_back();
		}
	}

	auto Row_Model::column(const Pixel_Column column) -> Column_Model & { return m_columns[column]; }

	auto Row_Model::column(const Pixel_Column column) const -> const Column_Model & { return m_columns[column]; }

	auto Row_Model::scroll_left() -> void
	{
		const auto m_leftmost_column{m_columns.front()};
		m_columns.erase(m_columns.begin());
		m_columns.emplace_back(m_leftmost_column);
	}

	auto Row_Model::scroll_right() -> void
	{
		const auto m_rightmost_column{m_columns.back()};
		m_columns.pop_back();
		m_columns.insert(m_columns.begin(), m_rightmost_column);
	}

	auto Row_Model::invert() -> void
	{
		for (auto &column : m_columns)
		{
			column.toggle();
		}
	}

	auto Row_Model::clear() -> void
	{
		for (auto &column : m_columns)
		{
			column.clear();
		}
	}

	auto Row_Model::set() -> void
	{
		for (auto &column : m_columns)
		{
			column.set();
		}
	}

	auto Row_Model::flip() -> void
	{
		const auto pivot{m_columns.size() / 2};
		for (auto i{0U}; i < pivot; i++)
		{
			exchange_columns(i, static_cast<Pixel_Column>(m_columns.size() - 1 - i));
		}
	}

	auto Row_Model::exchange_columns(const Pixel_Column source, const Pixel_Column destination) -> void
	{
		const auto tmp{m_columns[source].is_set()};
		m_columns[source].set(m_columns[destination].is_set());
		m_columns[destination].set(tmp);
	}

	Glyph_Model::Glyph_Model(const Pixel_Row row_count, const Pixel_Column column_count)
	{
		for (auto i = 0U; i < row_count; i++)
		{
			m_rows.emplace_back(column_count);
		}
	}

	auto Glyph_Model::row(const Pixel_Row row) -> Row_Model & { return m_rows[row]; }

	auto Glyph_Model::row(const Pixel_Row row) const -> const Row_Model & { return m_rows[row]; }

	auto Glyph_Model::scroll_up() -> void
	{
		auto m_top_row{m_rows.front()};
		m_rows.erase(m_rows.begin());
		m_rows.emplace_back(move(m_top_row));
	}

	auto Glyph_Model::scroll_left() -> void
	{
		for (auto &row : m_rows)
		{
			row.scroll_left();
		}
	}

	auto Glyph_Model::scroll_right() -> void
	{
		for (auto &row : m_rows)
		{
			row.scroll_right();
		}
	}

	auto Glyph_Model::scroll_down() -> void
	{
		const auto m_bottom_row{m_rows.back()};
		m_rows.pop_back();
		m_rows.insert(m_rows.begin(), m_bottom_row);
	}

	auto Glyph_Model::invert() -> void
	{
		for (auto &row : m_rows)
		{
			row.invert();
		}
	}

	auto Glyph_Model::copy(const Glyph_Model &src) -> void
	{
		m_rows.clear();
		m_rows = src.m_rows;
	}

	auto Glyph_Model::clear() -> void
	{
		for (auto &row : m_rows)
		{
			row.clear();
		}
	}

	auto Glyph_Model::set() -> void
	{
		for (auto &row : m_rows)
		{
			row.set();
		}
	}

	auto Glyph_Model::flip_horizontally() -> void
	{
		const auto pivot{m_rows.size() / 2};
		for (auto i{0U}; i < pivot; i++)
		{
			exchange_rows(i, static_cast<Pixel_Row>(m_rows.size() - 1 - i));
		}
	}

	auto Glyph_Model::exchange_rows(const Pixel_Row source, const Pixel_Row destination) -> void
	{
		const auto tmp{m_rows[source]};
		m_rows[source] = m_rows[destination];
		m_rows[destination] = tmp;
	}

	auto Glyph_Model::flip_vertically() -> void
	{
		for (auto &row : m_rows)
		{
			row.flip();
		}
	}

	// auto Glyph_Model::operator==(const Glyph_Model &other) const -> bool { return m_rows == other.m_rows; }

	Font_Model::Font_Model(const Font_Dimensions &font_spec, const Now_Function &now)
			: Font_Model{font_spec, now, now(), now(), 1U}
	{
		m_update_date = m_creation_date;
	}

	Font_Model::Font_Model(const Font_Dimensions &font_spec, const Now_Function &now,
	                       const Time_Point &creation_date, const Time_Point &update_date,
	                       const Revision_Count revision)
			: Domain_Model{}, m_font_spec{font_spec}, m_now{now},
			  m_glyphs{font_spec.glyph_count, Glyph_Model{font_spec.glyph_height, font_spec.glyph_width}},
			  m_creation_date{creation_date}, m_update_date{update_date}, m_revision_count{revision}
	{
	}

	auto Font_Model::set_font_name(const string &name) -> void { m_name = name; }

	auto Font_Model::set_author_name(const string &name) -> void { m_author = name; }

	auto Font_Model::set_code_page(const string &cp) -> void { m_code_page = cp; }

	auto Font_Model::set_description(const string &description) -> void { m_description = description; }

	auto Font_Model::set_has_been_updated() -> void
	{
		m_update_date = m_now();
		m_revision_count++;
	}

	auto Font_Model::glyph(const Glyph_ID id) -> Glyph_Model &
	{
		if (id >= m_font_spec.glyph_count)
		{
			throw runtime_error{
					fmt::format("Glyph index out of bounds {}≥{}.", id, m_font_spec.glyph_count)};
		}
		return m_glyphs.at(id);
	}

	auto Font_Model::glyph(const Glyph_ID id) const -> const Glyph_Model &
	{
		if (id >= m_font_spec.glyph_count)
		{
			throw runtime_error{
					fmt::format("Glyph index out of bounds {}≥{}.", id, m_font_spec.glyph_count)};
		}
		return m_glyphs.at(id);
	}

	auto Font_Model::for_each_glyph(const Immutable_Glyph_Fun &f) const -> void
	{
		auto glyph_id{0U};
		for (const auto &immutable_glyph : m_glyphs)
		{
			f(glyph_id, immutable_glyph);
			glyph_id++;
		}
	}

} // namespace fonted
