// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "glyph_edit_controls.hpp"
#include "use_stl.hpp"

namespace fonted
{

	Glyph_Edit_Controls::Glyph_Edit_Controls(const ID &id, Presentation_Model &presentation,
	                                         const Size &button_sizes)
			: Base_Widget{id}, m_button_sizes{button_sizes}, m_grid{id + "layout"}
	{
		//---- Scroll buttons
		m_grid.add(0, 1,
		           make_unique<Icon_Button>(id + " scroll up button", button_sizes, Icon_Button::Icon::ARROW_UP,
		                                    presentation,
		                                    [](Presentation_Model &model) { model.active_glyph_scroll_up(); }));
		m_grid.add(1, 0,
		           make_unique<Icon_Button>(id + " scroll left button", button_sizes,
		                                    Icon_Button::Icon::ARROW_LEFT, presentation,
		                                    [](Presentation_Model &model)
		                                    { model.active_glyph_scroll_left(); }));
		m_grid.add(1, 2,
		           make_unique<Icon_Button>(id + " scroll right button", button_sizes,
		                                    Icon_Button::Icon::ARROW_RIGHT, presentation,
		                                    [](Presentation_Model &model)
		                                    { model.active_glyph_scroll_right(); }));
		m_grid.add(2, 1,
		           make_unique<Icon_Button>(id + " scroll down button", button_sizes,
		                                    Icon_Button::Icon::ARROW_DOWN, presentation,
		                                    [](Presentation_Model &model)
		                                    { model.active_glyph_scroll_down(); }));
		//---- Invert, clear and set
		m_grid.add(3, 0,
		           make_unique<Icon_Button>(id + " invert active glyph", button_sizes,
		                                    Icon_Button::Icon::INVERT, presentation,
		                                    [](Presentation_Model &model) { model.invert_active_glyph(); }));
		m_grid.add(3, 1,
		           make_unique<Icon_Button>(id + " clear active glyph", button_sizes, Icon_Button::Icon::CLEAR,
		                                    presentation,
		                                    [](Presentation_Model &model) { model.clear_active_glyph(); }));
		m_grid.add(3, 2,
		           make_unique<Icon_Button>(id + " fill active glyph", button_sizes, Icon_Button::Icon::ALL_SET,
		                                    presentation,
		                                    [](Presentation_Model &model) { model.set_active_glyph(); }));
		//---- Flip
		m_grid.add(4, 0,
		           make_unique<Icon_Button>(id + " flip horizontally", button_sizes,
		                                    Icon_Button::Icon::FLIP_HORIZONTALLY, presentation,
		                                    [](Presentation_Model &model)
		                                    { model.flip_active_glyph_horizontally(); }));
		m_grid.add(4, 2,
		           make_unique<Icon_Button>(id + " flip vertically", button_sizes,
		                                    Icon_Button::Icon::FLIP_VERTICALLY, presentation,
		                                    [](Presentation_Model &model)
		                                    { model.flip_active_glyph_vertically(); }));
	}

} // namespace fonted
