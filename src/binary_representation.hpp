// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "domain_model.hpp"
#include "use_stl.hpp"

namespace fonted
{
	using Byte = std::byte;
	using Bytes = std::vector<Byte>;
} // namespace fonted

namespace fonted::binary
{

	//---- Externalization ------------------------------------------------

	[[nodiscard]] auto representation(const Domain_Model &) -> Bytes;

	//---- Internalization ------------------------------------------------

	[[nodiscard]] auto font_model(const Bytes &, const Glyph_Width, const Glyph_Height, const optional<Glyph_Count>,
	                              const size_t offset, const Font_Model::Now_Function & = system_clock::now)
			-> unique_ptr<Domain_Model>;

} // namespace fonted::binary
