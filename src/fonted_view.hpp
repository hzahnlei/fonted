// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "presentation_model.hpp"
#include "use_stl.hpp"
#include <imgui_internal.h>

#include <GL/glew.h>

#include <GLFW/glfw3.h> // Include glfw3.h after OpenGL definitions

namespace fonted
{

	class View_Model
	{
	    public:
		//---- Basic types ----------------------------------------------------
		using Width_Pixels = size_t;
		using Height_Pixels = size_t;

		struct Font_Texture_Specification final
		{
			ImTextureID id; // openGL uses GLuint (=unsigned int) while ImGui uses ImTextureID (=void*)
			Width_Pixels width;
			Height_Pixels height;
		};

		//---- Construction/destruction ---------------------------------------
		virtual ~View_Model() = default;

		//---- Interface ------------------------------------------------------
		virtual auto should_close() const -> bool = 0;
		virtual auto begin_frame() -> void = 0;
		virtual auto end_frame() -> void = 0;
		virtual auto font_texture_specification() const noexcept -> const Font_Texture_Specification & = 0;
		virtual auto uv_coordinates(const Glyph_ID) const -> ImRect = 0;

		virtual auto set_window_size(const Width_Pixels, const Height_Pixels) -> void = 0;
	};

	class GL3_View_Model final : public View_Model
	{
	    private:
		Presentation_Model &m_presentation_model;

		std::string m_glsl_version = "";
		GLFWwindow *m_window = nullptr;

		Font_Texture_Specification m_font_texture_spec;

		using Byte = std::byte;
		using Byte_Count = std::size_t;
		Byte_Count m_buffer_size;
		Byte *m_buffer = nullptr;

	    public:
		explicit GL3_View_Model(Presentation_Model &);
		~GL3_View_Model() override;

		auto should_close() const -> bool override;
		auto begin_frame() -> void override;
		auto end_frame() -> void override;
		[[nodiscard]] auto font_texture_specification() const noexcept
				-> const Font_Texture_Specification & override;
		[[nodiscard]] auto uv_coordinates(const Glyph_ID) const -> ImRect override;

		auto set_window_size(const Width_Pixels, const Height_Pixels) -> void override;

	    private:
		auto init_glfw() -> void;
		auto init_glew() -> void;
		auto init_imgui() -> void;
		auto init_font_texture() -> void;
		auto cleanup_font_texture() -> void;
		auto cleanup_imgui() const -> void;
		auto cleanup_glew() -> void;
		auto cleanup_glfw() const -> void;

		auto rebuild_font_texture() -> void;

		auto build_new_font_texture() -> void;
	};

	inline auto GL3_View_Model::font_texture_specification() const noexcept -> const Font_Texture_Specification &
	{
		return m_font_texture_spec;
	}

} // namespace fonted
