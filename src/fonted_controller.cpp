// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "fonted_controller.hpp"
#include "menu_bar.hpp"
#include "widget_commons.hpp"
#include <fmt/core.h>
#include <imgui.h>

namespace fonted
{

	auto menu_items()
	{
#ifdef __APPLE__
		static constexpr auto MOD_KEY_DISPLAY{"CMD"};
#else
		static constexpr auto MOD_KEY_DISPLAY{"CTRL"};
#endif
		return Menu_Items{Menu_Item{.display_name = "F1 - Load",
		                            .key = ImGuiKey_F1,
		                            .action = [](Presentation_Model &presentation) { presentation.load(); }},
		                  Menu_Item{.display_name = "F2 - Save",
		                            .key = ImGuiKey_F2,
		                            .action = [](Presentation_Model &presentation) { presentation.save(); }},
		                  Menu_Item{.display_name = "F3 - Export",
		                            .key = ImGuiKey_F3,
		                            .action = [](Presentation_Model &presentation)
		                            { presentation.export_binary(); }},
		                  Menu_Item{.display_name = fmt::format("{}+C - Copy", MOD_KEY_DISPLAY),
		                            .key = ImGuiKey_C,
		                            .modifier = ImGuiMod_Shortcut, // ImGuiMod_Super on macOS, ImGuiMod_Ctrl on
		                                                           // Linux/Windows
		                            .action = [](Presentation_Model &presentation)
		                            { presentation.copy_active_glyph(); }},
		                  Menu_Item{.display_name = fmt::format("{}+V - Paste", MOD_KEY_DISPLAY),
		                            .key = ImGuiKey_V,
		                            .modifier = ImGuiMod_Shortcut,
		                            .action = [](Presentation_Model &presentation)
		                            { presentation.paste_to_active_glyph(); }}};
	}

	Fonted_Controller::Fonted_Controller(Presentation_Model &presentation, View_Model &view, Stack_Frame &stack)
			: m_presentation_model{presentation}, m_view_model{view}, m_stack{stack}
	{
		ImGui::StyleColorsDark();
		ImGui::GetStyle().AntiAliasedLines = false;
		ImGui::GetStyle().AntiAliasedFill = false;
		init_widgets();
	}

	auto Fonted_Controller::init_widgets() -> void
	{
		auto main_area{make_unique<Column_Layout>("main group", Surrounding_Border::DO_NOT_PAD)};
		auto pick_and_edit_area{make_unique<Row_Layout>("pick and edit group")};
		pick_and_edit_area->add(make_unique<Glyph_Picker>("glyph picker", m_presentation_model, m_view_model));
		auto editing_area{make_unique<Column_Layout>("editing group", Surrounding_Border::DO_NOT_PAD)};
		editing_area->add(make_unique<Glyph_Editor>("glyph editor", m_presentation_model));
		editing_area->add(make_unique<Glyph_Edit_Controls>("scrol controls", m_presentation_model));
		pick_and_edit_area->add(move(editing_area));
		main_area->add(move(pick_and_edit_area));
		main_area->add(make_unique<Menu_Bar>("menu group", m_presentation_model, menu_items(), m_stack));
		m_main_view = move(main_area);
	}

	auto Fonted_Controller::run() -> void
	{
		while (should_run())
		{
			resize_window(); // Might have changed in the meanwhile

			m_view_model.begin_frame();

			const auto viewport{ImGui::GetMainViewport()};
			ImGui::SetNextWindowPos(viewport->Pos);
			ImGui::SetNextWindowSize(viewport->Size);
			ImGui::PushStyleColor(ImGuiCol_WindowBg, BACKGROUND_COLOR);
			ImGui::Begin("Edit", nullptr,
			             ImGuiWindowFlags_::ImGuiWindowFlags_NoDecoration |
			                             ImGuiWindowFlags_::ImGuiWindowFlags_NoResize);
			(*m_main_view)();
			ImGui::End();
			ImGui::PopStyleColor(1);

			m_view_model.end_frame();
		}
	}

} // namespace fonted
