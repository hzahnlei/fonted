// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "domain_model.hpp"
#include <chrono>
#include <memory>
#include <yaml-cpp/yaml.h>

namespace fonted::native
{

	[[nodiscard]] auto representation(const Domain_Model &) -> string;

	[[nodiscard]] auto font_model(const YAML::Node &, const Font_Model::Now_Function & = system_clock::now)
			-> unique_ptr<Domain_Model>;

} // namespace fonted::native
