// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "presentation_model.hpp"
#include "file_io.hpp"
#include "scripting/scripting.hpp"
#include "use_stl.hpp"
#include <fmt/core.h>

namespace fonted
{

	Glyph_Picker_Spec::Glyph_Picker_Spec(const Glyph_Row row, const Glyph_Column col)
			: row_count{row}, column_count{col}
	{
		if (row_count < MIN_ROW_COUNT)
		{
			throw runtime_error{fmt::format("Expect min row count of {} but was {}.", MIN_GLYPH_WIDTH,
			                                row_count)};
		}
		if (row_count > MAX_ROW_COUNT)
		{
			throw runtime_error{fmt::format("Expect max row count of {} but was {}.", MAX_GLYPH_WIDTH,
			                                row_count)};
		}
		if (column_count < MIN_COLUMN_COUNT)
		{
			throw runtime_error{fmt::format("Expect min column_count of {} but was {}.", MIN_GLYPH_HEIGHT,
			                                column_count)};
		}
		if (column_count > MAX_COLUMN_COUNT)
		{
			throw runtime_error{fmt::format("Expect max column_count of {} but was {}.", MAX_GLYPH_HEIGHT,
			                                column_count)};
		}
	}

	auto Delegating_Presentation_Model::glyph_id(const Glyph_Coordinate &glyph_coordinate) const noexcept
			-> Glyph_ID
	{
		return glyph_coordinate.row * m_glyph_picker.column_count + glyph_coordinate.column;
	}

	auto Delegating_Presentation_Model::row_and_column(const Glyph_ID glyph_id) const noexcept -> Glyph_Coordinate
	{
		const auto row{glyph_id / m_glyph_picker.column_count};
		const auto column{glyph_id - (row * m_glyph_picker.column_count)};
		return Glyph_Coordinate{.column = column, .row = row};
	}

	[[nodiscard]] inline auto logical_dimensions(const Glyph_Column_Count required_column_count,
	                                             const Domain_Model &model)
	{
		return Glyph_Picker_Spec{
				static_cast<Glyph_Row>(ceil(static_cast<double>(model.font_dimensions().glyph_count) /
		                                            static_cast<double>(required_column_count))),
				required_column_count};
	}

	Delegating_Presentation_Model::Delegating_Presentation_Model(const Glyph_Column_Count required_column_count,
	                                                             unique_ptr<Domain_Model> model,
	                                                             const string &file_name,
	                                                             const Font_Model::Now_Function &now)
			: Presentation_Model{}, m_domain_model{move(model)}, m_now{now}
	{
		m_glyph_picker = logical_dimensions(required_column_count, *m_domain_model);
		update_presentation_model(file_name);
	}

	//---- Glyph picker -----------------------------------------------------------

	auto Delegating_Presentation_Model::copy_active_glyph() -> void
	{
		m_clip_board = make_unique<Glyph_Model>(m_domain_model->font_dimensions().glyph_width,
		                                        m_domain_model->font_dimensions().glyph_height);
		m_clip_board->copy(m_domain_model->glyph(m_active_glyph_id));
	}

	auto Delegating_Presentation_Model::paste_to_active_glyph() -> void
	{
		if (m_clip_board != nullptr)
		{
			m_domain_model->glyph(m_active_glyph_id).copy(*m_clip_board);
			remember_only_glyphs_have_changed();
		}
	}

	auto Delegating_Presentation_Model::copy_to_active_glyph(const Presentation_Model &source_font,
	                                                         const Glyph_ID source_glyph_id) -> void
	{
		if (source_font.font_dimensions().glyph_width != m_domain_model->font_dimensions().glyph_width)
		{
			throw runtime_error{fmt::format(
					"Failed to copy glyph {} from source font to glyph {} of target font. The "
					"glyphs of the source font are {} pixels wide while the glyphs of the target "
					"font are {} pixels wide.",
					source_font.active_glyph_id(), m_active_glyph_id,
					source_font.font_dimensions().glyph_width,
					m_domain_model->font_dimensions().glyph_width)};
		}
		if (source_font.font_dimensions().glyph_height != m_domain_model->font_dimensions().glyph_height)
		{
			throw runtime_error{fmt::format(
					"Failed to copy glyph {} from source font to glyph {} of target font. The "
					"glyphs of the source font are {} pixels heigh while the glyphs of the target "
					"font are {} pixels heigh.",
					source_font.active_glyph_id(), m_active_glyph_id,
					source_font.font_dimensions().glyph_height,
					m_domain_model->font_dimensions().glyph_height)};
		}
		m_domain_model->glyph(m_active_glyph_id).copy(source_font.glyph(source_glyph_id));
		remember_only_glyphs_have_changed();
	}

	//---- Glyph/font -------------------------------------------------------------

	auto Delegating_Presentation_Model::on_active_glyph_toggle(const Pixel_Coordinate &pixel) -> void
	{
		m_domain_model->glyph(m_active_glyph_id).row(pixel.row).column(pixel.column).toggle();
		remember_only_glyphs_have_changed();
	}

	auto Delegating_Presentation_Model::is_set_on_active_glyph(const Pixel_Coordinate &pixel) const -> bool
	{
		return m_domain_model->glyph(m_active_glyph_id).row(pixel.row).column(pixel.column).is_set();
	}

	auto Delegating_Presentation_Model::set_active_glyph(const Glyph_ID id) -> void
	{
		if (id >= m_domain_model->font_dimensions().glyph_count)
		{
			throw runtime_error{fmt::format("Glyph index out of bounds {}≥{}.", id,
			                                m_domain_model->font_dimensions().glyph_count)};
		}
		m_active_glyph_id = id;
	}

	auto Delegating_Presentation_Model::active_glyph_scroll_up() -> void
	{
		m_domain_model->glyph(m_active_glyph_id).scroll_up();
		remember_only_glyphs_have_changed();
	}

	auto Delegating_Presentation_Model::active_glyph_scroll_left() -> void
	{
		m_domain_model->glyph(m_active_glyph_id).scroll_left();
		remember_only_glyphs_have_changed();
	}

	auto Delegating_Presentation_Model::active_glyph_scroll_right() -> void
	{
		m_domain_model->glyph(m_active_glyph_id).scroll_right();
		remember_only_glyphs_have_changed();
	}

	auto Delegating_Presentation_Model::active_glyph_scroll_down() -> void
	{
		m_domain_model->glyph(m_active_glyph_id).scroll_down();
		remember_only_glyphs_have_changed();
	}

	auto Delegating_Presentation_Model::invert_active_glyph() -> void
	{
		m_domain_model->glyph(m_active_glyph_id).invert();
		remember_only_glyphs_have_changed();
	}

	auto Delegating_Presentation_Model::clear_active_glyph() -> void
	{
		m_domain_model->glyph(m_active_glyph_id).clear();
		remember_only_glyphs_have_changed();
	}

	auto Delegating_Presentation_Model::set_active_glyph() -> void
	{
		m_domain_model->glyph(m_active_glyph_id).set();
		remember_only_glyphs_have_changed();
	}

	auto Delegating_Presentation_Model::flip_active_glyph_horizontally() -> void
	{
		m_domain_model->glyph(m_active_glyph_id).flip_horizontally();
		remember_only_glyphs_have_changed();
	}

	auto Delegating_Presentation_Model::flip_active_glyph_vertically() -> void
	{
		m_domain_model->glyph(m_active_glyph_id).flip_vertically();
		remember_only_glyphs_have_changed();
	}

	//---- File related -----------------------------------------------------------

	auto Delegating_Presentation_Model::save() -> void
	{
		update_domain_model();
		// The update date gets set to current date/time every time we save the model.
		// We are not checking whether there were actualy changes to the model.
		m_domain_model->set_has_been_updated();
		native::store(*m_domain_model, begin(m_file_name));
	}

	//---- Scripting ------------------------------------------------------

	[[nodiscard]] inline auto as_str(const auto &x) { return string{begin(x), strlen(begin(x))}; }

	auto Delegating_Presentation_Model::execute_command(Stack_Frame &stack) -> void
	{
		ostringstream out;
		execute_script(as_str(m_command_input), stack, out);
		strcpy(begin(m_command_output), out.str().c_str());
	}

	auto Delegating_Presentation_Model::load() -> void
	{
		m_clip_board = nullptr; // Glyph in clip board may not match dimensions of font to be loaded.
		const auto file_name{as_str(m_file_name)};
		m_domain_model = native::load(file_name, m_now);
		update_presentation_model(file_name);
		remember_whole_font_has_changed();
		// We always stick to the default column count. However, we recompute the row count depending on the
		// number of glyphs in the font.
		const auto current_column_count{m_glyph_picker.column_count};
		m_glyph_picker = logical_dimensions(current_column_count, *m_domain_model);
	}

	const regex MATCH_FILE_EXTENSION{fmt::format("\\.{}$", FONTED_FILE_EXTENSION)};

	static constexpr auto BINARY_FILE_EXTENSION{".bin"};

	auto binary_file_name(const string &yaml_file_name) -> string
	{
		string bin_file_name{yaml_file_name};
		bin_file_name = regex_replace(bin_file_name, MATCH_FILE_EXTENSION, "");
		bin_file_name += BINARY_FILE_EXTENSION;
		return bin_file_name;
	}

	auto Delegating_Presentation_Model::export_binary() -> void
	{
		binary::store(*m_domain_model, binary_file_name(as_str(m_file_name)));
	}

	auto Delegating_Presentation_Model::update_domain_model() -> void
	{
		m_domain_model->set_font_name(as_str(m_font_name));
		m_domain_model->set_author_name(as_str(m_author_name));
		m_domain_model->set_code_page(as_str(m_code_page));
		m_domain_model->set_description(as_str(m_description));
	}

	auto Delegating_Presentation_Model::update_presentation_model(const string &file_name) -> void
	{
		strcpy(begin(m_file_name), file_name.c_str());
		strcpy(begin(m_font_name), m_domain_model->font_name().c_str());
		strcpy(begin(m_author_name), m_domain_model->author_name().c_str());
		strcpy(begin(m_code_page), m_domain_model->code_page().c_str());
		strcpy(begin(m_description), m_domain_model->description().c_str());
	}

} // namespace fonted
