// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "widget_commons.hpp"

namespace fonted
{

	Base_Widget::Base_Widget(const ID &id, const Style_Accessor &style) : Widget{}, m_id{id}, m_style{style} {}

} // namespace fonted
