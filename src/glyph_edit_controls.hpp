// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "grid_layout.hpp"
#include "icon_button.hpp"
#include "presentation_model.hpp"
#include "widget_commons.hpp"
#include <imgui.h>
#include <imgui_internal.h>

namespace fonted
{

	class Glyph_Edit_Controls final : public Base_Widget
	{
	    private:
		const Size m_button_sizes;
		Grid_Layout m_grid;

	    public:
		static constexpr Size DEFAULT_BUTTON_SIZE{41, 41};

		Glyph_Edit_Controls(const ID &, Presentation_Model &, const Size &button_sizes = DEFAULT_BUTTON_SIZE);
		~Glyph_Edit_Controls() override = default;

		[[nodiscard]] auto widget_size() const -> Size override;
		auto operator()() -> void override;
	};

	//---- Interface methods ------------------------------------------------------

	inline auto Glyph_Edit_Controls::widget_size() const -> Size { return m_grid.widget_size(); }

	inline auto Glyph_Edit_Controls::operator()() -> void { m_grid(); }

} // namespace fonted
