// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "use_stl.hpp"
#include <imgui.h>
#include <imgui_internal.h>

//---- Color scheme -----------------------------------------------------------

namespace fonted::color
{
	constexpr auto LIGHT_GREEN{IM_COL32(0, 255, 0, 255)};
	constexpr auto GREEN{IM_COL32(0, 128, 0, 255)};
	constexpr auto DARK_GREEN{IM_COL32(0, 96, 0, 255)};
	constexpr auto DARK_HIGHLIGHT_GREEN{IM_COL32(0, 96, 0, 96)};
	constexpr auto SUPER_DARK_HIGHLIGHT_GREEN{IM_COL32(0, 96, 0, 64)};
	constexpr auto ALMOST_BLACK_GREEN{IM_COL32(0, 16, 0, 255)};
} // namespace fonted::color

namespace fonted
{

	constexpr auto BACKGROUND_COLOR{color::ALMOST_BLACK_GREEN};
	constexpr auto BORDER_COLOR{color::LIGHT_GREEN};
	constexpr auto SEPARATOR_COLOR{color::DARK_GREEN};
	constexpr auto PIXEL_SET_COLOR{color::GREEN};
	constexpr auto CAPTION_TEXT_COLOR{BORDER_COLOR};
	constexpr auto ASCII_GLYPH_COLOR{color::GREEN};
	constexpr auto ACTIVE_GLYPH_HIGHLIGHT_COLOR{color::DARK_HIGHLIGHT_GREEN};
	constexpr auto PIXEL_CROSS_HAIR_COLOR{color::SUPER_DARK_HIGHLIGHT_GREEN};
	constexpr auto GLYPH_CROSS_HAIR_COLOR{PIXEL_CROSS_HAIR_COLOR};
	constexpr auto ACTIVE_GLYPH_FRAME_COLOR{BORDER_COLOR};

	constexpr auto BUTTON_OUTLINE_COLOR{BORDER_COLOR};
	constexpr auto BUTTON_HIGHLIGHT_COLOR{color::DARK_GREEN};
	constexpr auto BUTTON_PRESSED_COLOR{BACKGROUND_COLOR};

	constexpr auto INPUT_BOX_BACKGROUND_COLOR{color::DARK_HIGHLIGHT_GREEN};

	constexpr auto DEBUG_BACKGROUND_COLOR{IM_COL32(128, 0, 0, 96)};
	constexpr auto DEBUG_BORDER_COLOR{IM_COL32(255, 0, 0, 255)};

	//---- Size scheme ------------------------------------------------------------

	constexpr auto PIXEL_BOX_SIZE{16.0f};
	constexpr auto BORDER_THICKNESS{2.0f};
	constexpr auto BORDER_OFFSET{BORDER_THICKNESS / 2.0f};
	constexpr auto SEPARATOR_THICKNESS{1.0f};

	constexpr auto GLYPH_GRID_WIDTH{32};
	constexpr auto GLYPH_GRID_HEIGHT{24};
	constexpr auto GLYPH_PICKER_CAPTION_HEIGHT{24.0f};
	constexpr auto GLYPH_PICKER_CAPTION_WIDTH{32.0f};
	constexpr auto CAPTION_LEFT_DISTANCE{1.0f};
	constexpr auto ACTIVE_GLYPH_FRAME_THICKNESS{2.0f};

	constexpr auto ICON_LINE_THICKNESS{2.0f}; // Needs to be signed for computations
	constexpr auto ICON_LINE_INDENTATION{ICON_LINE_THICKNESS / 2.0f};

	constexpr auto SHARP_EDGES{0.0f}; // When drawing rectangles

	//---- Constants --------------------------------------------------------------

	constexpr auto NO_BORDER{false};

	enum class Surrounding_Border : bool
	{
		PAD = true,
		DO_NOT_PAD = !PAD
	};

	enum class Individual_Widgets : bool
	{
		PAD = true,
		DO_NOT_PAD = !PAD
	};

	//---- Base widget ------------------------------------------------------------

	class Widget
	{
	    public:
		//---- Basic types ----------------------------------------------------
		using ID = std::string;
		using Width = float;
		using Height = float;
		using Size = ImVec2; // Same as Width × Height

		//---- Construction/destruction ---------------------------------------
		explicit Widget() = default;
		virtual ~Widget() = default;

		//---- Interface ------------------------------------------------------
		virtual auto id() const noexcept -> const ID & = 0;
		virtual auto widget_size() const -> Size = 0;
		virtual auto operator()() -> void = 0;

	    protected:
		virtual auto style() const noexcept -> const ImGuiStyle & = 0;
	};

	class Base_Widget : public Widget
	{
	    public:
		using Style_Accessor = std::function<const ImGuiStyle &()>;

	    private:
		const ID m_id;
		const Style_Accessor m_style;

	    public:
		//---- Construction/destruction ---------------------------------------
		Base_Widget(const ID &, const Style_Accessor & = ImGui::GetStyle);
		~Base_Widget() override = default;

		//---- Interface ------------------------------------------------------
		[[nodiscard]] auto id() const noexcept -> const ID & final;

	    protected:
		[[nodiscard]] auto style() const noexcept -> const ImGuiStyle & final;
	};

	inline auto Base_Widget::id() const noexcept -> const ID & { return m_id; }

	inline auto Base_Widget::style() const noexcept -> const ImGuiStyle & { return m_style(); }

} // namespace fonted
