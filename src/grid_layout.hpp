// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "use_stl.hpp"
#include "widget_commons.hpp"
#include <imgui.h>

namespace fonted
{

	class Grid_Layout final : public Base_Widget
	{
	    public:
		using Children = vector<vector<unique_ptr<Widget>>>;
		using Row_Count = unsigned int;
		using Grid_Row = Row_Count;
		using Column_Count = Row_Count;
		using Grid_Column = Column_Count;

	    private:
		const Surrounding_Border m_border_padding;
		const Individual_Widgets m_individual_padding;
		Children m_children;

		Width m_max_child_width{0.0f};
		Height m_max_child_height{0.0f};
		Row_Count m_row_count{0U};
		Column_Count m_column_count{0U};

	    public:
		Grid_Layout(const ID &, const Style_Accessor & = ImGui::GetStyle,
		            const Surrounding_Border = Surrounding_Border::PAD,
		            const Individual_Widgets = Individual_Widgets::PAD);
		~Grid_Layout() override = default;

		[[nodiscard]] auto widget_size() const -> Size override;
		auto operator()() -> void override;

		auto add(const Grid_Row, const Grid_Column, unique_ptr<Widget>) -> void;
		[[nodiscard]] auto child(const Grid_Row, const Grid_Column) const noexcept -> Widget const *;
		[[nodiscard]] auto effective_row_count() const noexcept -> Row_Count;
		[[nodiscard]] auto effective_column_count() const noexcept -> Column_Count;

		[[nodiscard]] auto pad_border() const noexcept -> bool;
		[[nodiscard]] auto pad_individual_widgets() const noexcept -> bool;
	};

	inline auto Grid_Layout::effective_row_count() const noexcept -> Row_Count { return m_row_count; }

	inline auto Grid_Layout::effective_column_count() const noexcept -> Column_Count { return m_column_count; }

	inline auto Grid_Layout::pad_border() const noexcept -> bool
	{
		return m_border_padding == Surrounding_Border::PAD;
	}

	inline auto Grid_Layout::pad_individual_widgets() const noexcept -> bool
	{
		return m_individual_padding == Individual_Widgets::PAD;
	}

} // namespace fonted
