// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include <array>
#include <chrono>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <fstream>
#include <functional>
#include <iostream>
#include <istream>
#include <memory>
#include <optional>
#include <ostream>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace fonted
{

	using std::array;
	using std::begin;
	using std::byte;
	using std::ceil;
	using std::function;
	using std::ifstream;
	using std::ios;
	using std::istream;
	using std::istringstream;
	using std::make_shared;
	using std::make_unique;
	using std::move;
	using std::nullopt;
	using std::ofstream;
	using std::optional;
	using std::ostream;
	using std::ostringstream;
	using std::regex;
	using std::regex_replace;
	using std::runtime_error;
	using std::shared_ptr;
	using std::size_t;
	using std::strcpy;
	using std::string;
	using std::strlen;
	using std::uint64_t;
	using std::unique_ptr;
	using std::vector;
	using std::chrono::floor;
	using std::chrono::milliseconds;
	using std::chrono::system_clock;

	using namespace std::chrono_literals;

} // namespace fonted
