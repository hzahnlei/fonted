// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "fonted_view.hpp"
#include "presentation_model.hpp"
#include "use_stl.hpp"
#include "widget_commons.hpp"
#include <imgui.h>
#include <imgui_internal.h>

namespace fonted
{

	struct Glyph_Picker_Geometry final
	{
		Pixel_Count column_width;
		Pixel_Count row_height;
		explicit Glyph_Picker_Geometry(const Presentation_Model &);
	};

	class Glyph_Picker final : public Base_Widget
	{
	    public:
		[[nodiscard]] static auto character_caption(const Glyph_ID) -> std::string;
		[[nodiscard]] static auto row_and_column(const Presentation_Model &, const Glyph_Picker_Geometry &,
		                                         const ImVec2 &mouse_pos) -> std::optional<Glyph_Coordinate>;

	    private:
		mutable Glyph_Picker_Geometry m_dimensions;
		Presentation_Model &m_model;
		View_Model &m_view;
		int m_frame_count = 0;

	    public:
		Glyph_Picker(const ID &, Presentation_Model &, View_Model &);
		~Glyph_Picker() override = default;

		[[nodiscard]] auto widget_size() const -> Size override;
		auto operator()() -> void override;

	    private:
		auto draw(const ImRect &, ImGuiWindow &) -> void;
		auto draw_border(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner, ImDrawList &) const
				-> void;
		auto draw_grid_overlay(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
		                       ImDrawList &) const -> void;
		auto draw_row_separators(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
		                         ImDrawList &) const -> void;
		auto draw_column_separators(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
		                            ImDrawList &) const -> void;
		auto draw_row_captions(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
		                       ImDrawList &) const -> void;
		auto draw_column_captions(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
		                          ImDrawList &) const -> void;
		auto draw_glyphs(const ImVec2 &upper_left_corner, ImDrawList &) const -> void;
		auto draw_animations(const ImVec2 &upper_left_corner, ImDrawList &) -> void;
		auto draw_cross_hair(const Glyph_Coordinate &, const ImRect &bounding_box, ImGuiWindow &window) const
				-> void;
	};

} // namespace fonted
