// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "icon_button.hpp"
#include <fmt/core.h>

namespace fonted
{

	Icon_Button::Icon_Button(const ID &id, const Size size, const Icon icon, Presentation_Model &model,
	                         const Action &action)
			: Base_Widget{id}, m_widget_size{size}, m_icon{icon}, m_model{model}, m_action{action}
	{
	}

	//---- Interface methods ------------------------------------------------------

	auto Icon_Button::operator()() -> void
	{
		auto &window{*ImGui::GetCurrentWindow()};
		if (window.SkipItems)
		{
			return;
		}
		const auto imgui_id{window.GetID(id().c_str())};
		const auto effective_size{ImGui::CalcItemSize(m_widget_size, 0.0f, 0.0f)};
		const auto pos{window.DC.CursorPos};
		const ImRect bounding_box{pos, pos + effective_size};

#ifdef FONTED_DEBUG_DRAWING
		auto &draw_list{*window.DrawList};
		draw_list.AddRectFilled(bounding_box.Min, bounding_box.Max, DEBUG_BACKGROUND_COLOR);
#endif

		ImGui::ItemSize(effective_size);
		if (!ImGui::ItemAdd(bounding_box, imgui_id))
		{
			throw std::runtime_error{
					fmt::format("Failed to add icon button widget id='{}' to window.", id())};
		}
		bool hovered;
		bool held;
		const auto pressed{ImGui::ButtonBehavior(bounding_box, imgui_id, &hovered, &held,
		                                         ImGuiButtonFlags_::ImGuiButtonFlags_MouseButtonLeft)};
		const auto &g{*GImGui};
		IMGUI_TEST_ENGINE_ITEM_INFO(id, m_id.c_str(),
		                            g.LastItemData.StatusFlags); // This macro assumes g to be defined!!!
		if (pressed)
		{
			draw_pressed(bounding_box, window);
			m_action(m_model);
		}
		else if (hovered)
		{
			draw_highlighted(bounding_box, window);
		}
		else
		{
			draw_idle(bounding_box, window);
		}

#ifdef FONTED_DEBUG_DRAWING
		draw_list.AddRect(bounding_box.Min, bounding_box.Max, DEBUG_BORDER_COLOR);
#endif
	}

	//---- Helper methods ---------------------------------------------------------

	auto Icon_Button::draw_pressed(const ImRect &bounding_box, ImGuiWindow &window) const -> void
	{
		auto &draw_list{*window.DrawList};
		const ImVec2 upper_left_corner{bounding_box.Min};
		const ImVec2 lower_right_corner{bounding_box.Max};
		draw_background(upper_left_corner, lower_right_corner, draw_list, BUTTON_OUTLINE_COLOR);
		draw_border(upper_left_corner, lower_right_corner, draw_list, BUTTON_PRESSED_COLOR);
		draw_icon(upper_left_corner, lower_right_corner, draw_list, BUTTON_PRESSED_COLOR);
	}

	auto Icon_Button::draw_highlighted(const ImRect &bounding_box, ImGuiWindow &window) const -> void
	{
		auto &draw_list{*window.DrawList};
		const ImVec2 upper_left_corner{bounding_box.Min};
		const ImVec2 lower_right_corner{bounding_box.Max};
		draw_background(upper_left_corner, lower_right_corner, draw_list, BUTTON_HIGHLIGHT_COLOR);
		draw_border(upper_left_corner, lower_right_corner, draw_list, BUTTON_OUTLINE_COLOR);
		draw_icon(upper_left_corner, lower_right_corner, draw_list, BUTTON_OUTLINE_COLOR);
	}

	auto Icon_Button::draw_idle(const ImRect &bounding_box, ImGuiWindow &window) const -> void
	{
		auto &draw_list{*window.DrawList};
		const ImVec2 upper_left_corner{bounding_box.Min};
		const ImVec2 lower_right_corner{bounding_box.Max};
		draw_background(upper_left_corner, lower_right_corner, draw_list, BACKGROUND_COLOR);
		draw_border(upper_left_corner, lower_right_corner, draw_list, BUTTON_OUTLINE_COLOR);
		draw_icon(upper_left_corner, lower_right_corner, draw_list, BUTTON_HIGHLIGHT_COLOR);
	}

	auto Icon_Button::draw_background(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                                  ImDrawList &draw_list, const ImColor &color) const -> void
	{
		// AddRectFilled draws outside the bounding box, therefore adjust the drawing corrdinates.
		draw_list.AddRectFilled(upper_left_corner + ImVec2{BORDER_OFFSET, BORDER_OFFSET},
		                        lower_right_corner - ImVec2{BORDER_OFFSET, BORDER_OFFSET}, color, SHARP_EDGES,
		                        ImDrawFlags_::ImDrawFlags_RoundCornersNone);
	}

	auto Icon_Button::draw_border(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                              ImDrawList &draw_list, const ImColor &color) const -> void
	{
		// AddRect draws outside the bounding box, therefore adjust the drawing corrdinates.
		draw_list.AddRect(upper_left_corner + ImVec2{BORDER_OFFSET, BORDER_OFFSET},
		                  lower_right_corner - ImVec2{BORDER_OFFSET, BORDER_OFFSET}, color, SHARP_EDGES,
		                  ImDrawFlags_::ImDrawFlags_RoundCornersNone, BORDER_THICKNESS);
	}

	auto Icon_Button::draw_icon(const ImVec2 &upper_left_corner, const ImVec2 &lower_right_corner,
	                            ImDrawList &draw_list, const ImColor &color) const -> void
	{
		const auto mid_x{(upper_left_corner.x + lower_right_corner.x) / 2};
		const auto mid_y{(upper_left_corner.y + lower_right_corner.y) / 2};
		switch (m_icon)
		{
		case Icon::ARROW_DOWN:
		{
			const ImVec2 upper_pos{mid_x, upper_left_corner.y + 6.0f};
			const ImVec2 lower_pos{mid_x, lower_right_corner.y - 6.0f};
			draw_list.AddLine(upper_pos, lower_pos, color, ICON_LINE_THICKNESS);
			draw_list.AddLine(lower_pos, lower_pos + ImVec2{-6.0f, -12.0f}, color, ICON_LINE_THICKNESS);
			draw_list.AddLine(lower_pos, lower_pos + ImVec2{+6.0f, -12.0f}, color, ICON_LINE_THICKNESS);
			return;
		}
		case Icon::ARROW_LEFT:
		{
			const ImVec2 left_pos{upper_left_corner.x + 6.0f, mid_y};
			const ImVec2 right_pos{lower_right_corner.x - 6.0f, mid_y};
			draw_list.AddLine(left_pos, right_pos, color, ICON_LINE_THICKNESS);
			draw_list.AddLine(left_pos, left_pos + ImVec2{+12.0f, -6.0f}, color, ICON_LINE_THICKNESS);
			draw_list.AddLine(left_pos, left_pos + ImVec2{+12.0f, +6.0f}, color, ICON_LINE_THICKNESS);
			return;
		}
		case Icon::ARROW_RIGHT:
		{
			const ImVec2 left_pos{upper_left_corner.x + 6.0f, mid_y};
			const ImVec2 right_pos{lower_right_corner.x - 6.0f, mid_y};
			draw_list.AddLine(right_pos, left_pos, color, ICON_LINE_THICKNESS);
			draw_list.AddLine(right_pos, right_pos + ImVec2{-12.0f, -6.0f}, color, ICON_LINE_THICKNESS);
			draw_list.AddLine(right_pos, right_pos + ImVec2{-12.0f, +6.0f}, color, ICON_LINE_THICKNESS);
			return;
		}
		case Icon::ARROW_UP:
		{
			const ImVec2 upper_pos{mid_x, upper_left_corner.y + 6.0f};
			const ImVec2 lower_pos{mid_x, lower_right_corner.y - 6.0f};
			draw_list.AddLine(upper_pos, lower_pos, color, ICON_LINE_THICKNESS);
			draw_list.AddLine(upper_pos, upper_pos + ImVec2{-6.0f, +12.0f}, color, ICON_LINE_THICKNESS);
			draw_list.AddLine(upper_pos, upper_pos + ImVec2{+6.0f, +12.0f}, color, ICON_LINE_THICKNESS);
			return;
		}
		case Icon::INVERT:
		{
			const ImVec2 upper_pos{mid_x, upper_left_corner.y + 6.0f};
			const ImVec2 lower_pos{mid_x, lower_right_corner.y - 6.0f};
			draw_list.AddRectFilled(upper_pos + ImVec2{-12.0f, 0.0f}, lower_pos, color);
			draw_list.AddRect(upper_pos + ImVec2{+12.0f, ICON_LINE_INDENTATION},
			                  lower_pos + ImVec2{0.0f, -ICON_LINE_INDENTATION}, color, 0.0f,
			                  ImDrawFlags_::ImDrawFlags_None, ICON_LINE_THICKNESS);
			return;
		}
		case Icon::CLEAR:
		{
			const ImVec2 upper_pos{mid_x + 12, upper_left_corner.y + 6.0f};
			const ImVec2 lower_pos{mid_x - 12, lower_right_corner.y - 6.0f};
			draw_list.AddRect(upper_pos, lower_pos, color, 0.0f, ImDrawFlags_::ImDrawFlags_None,
			                  ICON_LINE_THICKNESS);
			return;
		}
		case Icon::ALL_SET:
		{
			const ImVec2 upper_pos{mid_x + 12, upper_left_corner.y + 6.0f};
			const ImVec2 lower_pos{mid_x - 12, lower_right_corner.y - 6.0f};
			draw_list.AddRectFilled(upper_pos, lower_pos, color);
			return;
		}
		case Icon::FLIP_HORIZONTALLY:
		{
			const ImVec2 upper_pos{upper_left_corner.x + 12.0f, mid_y};
			const ImVec2 lower_pos{lower_right_corner.x - 12.0f, mid_y};
			draw_list.AddRectFilled(upper_pos, lower_pos + ImVec2{0.0f, -8.0f}, color);
			draw_list.AddRect(upper_pos + ImVec2{ICON_LINE_INDENTATION, +8.0f},
			                  lower_pos + ImVec2{-ICON_LINE_INDENTATION, 0.0f}, color, 0.0f,
			                  ImDrawFlags_::ImDrawFlags_None, ICON_LINE_THICKNESS);
			draw_list.AddLine(upper_pos + ImVec2{-6.0f, 0.0f}, lower_pos + ImVec2{+6.0f, 0.0f}, color,
			                  ICON_LINE_THICKNESS);
			return;
		}
		case Icon::FLIP_VERTICALLY:
		{
			const ImVec2 upper_pos{mid_x, upper_left_corner.y + 12.0f};
			const ImVec2 lower_pos{mid_x, lower_right_corner.y - 12.0f};
			draw_list.AddRectFilled(upper_pos + ImVec2{-8.0f, 0.0f}, lower_pos, color);
			draw_list.AddRect(upper_pos + ImVec2{+8.0f, ICON_LINE_INDENTATION},
			                  lower_pos + ImVec2{0.0f, -ICON_LINE_INDENTATION}, color, 0.0f,
			                  ImDrawFlags_::ImDrawFlags_None, ICON_LINE_THICKNESS);
			draw_list.AddLine(upper_pos + ImVec2{0.0f, -6.0f}, lower_pos + ImVec2{0.0f, +6.0f}, color,
			                  ICON_LINE_THICKNESS);
			return;
		}
		}
	}

} // namespace fonted
