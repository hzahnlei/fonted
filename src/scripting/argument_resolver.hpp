// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using appsl::Expression;

	/**
	 * @brief Checks whether the given argument is a glyph object and returns it, if so. Exception otherwise.
	 */
	auto asserted_glyph_argument(const Expression &argument, const string &function_name,
	                             const string &parameter_name) -> unique_ptr<const Expression>;

	/**
	 * @brief Checks whether the given argument is a glyph object and returns it, if so. Exception otherwise.
	 */
	auto asserted_glyph_argument(const Expression &argument, const string &function_name,
	                             const string &parameter_name, shared_ptr<const Expression> implied_font)
			-> unique_ptr<const Expression>;

} // namespace fonted::scripting
