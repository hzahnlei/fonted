// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "presentation_model.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using appsl::Abstract_Builtin_Function;
	using appsl::Expression;
	using appsl::Stack_Frame;

	class Merge_Fonts_Function final : public Abstract_Builtin_Function
	{
	    private:
		const Font_Model::Now_Function m_clock;

	    public:
		Merge_Fonts_Function(const Fun_Display_Name &, const Font_Model::Now_Function &);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;

	    private:
		auto assert_at_least_one_font_given(const Expression &) const -> void;
		auto assert_all_arguments_are_fonts(const Expression &) const -> void;
		static auto total_glyph_count(const Expression &) -> Glyph_Count;
		auto assert_all_fonts_have_same_dimensions(const Expression &, const Font_Dimensions &) const -> void;
	};

} // namespace fonted::scripting
