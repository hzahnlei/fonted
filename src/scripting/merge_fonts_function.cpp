// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "merge_fonts_function.hpp"
#include "font_object.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace fonted::scripting
{

	using namespace junkbox;
	using namespace appsl;

	Merge_Fonts_Function::Merge_Fonts_Function(const Fun_Display_Name &name, const Font_Model::Now_Function &clock)
			: Abstract_Builtin_Function{name}, m_clock{clock}
	{
	}

	auto Merge_Fonts_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Merge_Fonts_Function const *const>(&other);
	}

	auto Merge_Fonts_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Merge_Fonts_Function>(function_display_name(), m_clock);
	}

	static const string FONTS_PARAM_NAME{"fonts"};

	auto Merge_Fonts_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &fonts{stack.lookup(FONTS_PARAM_NAME)};
		assert_at_least_one_font_given(fonts);
		assert_all_arguments_are_fonts(fonts);
		const auto total_glyph_count{Merge_Fonts_Function::total_glyph_count(fonts)};
		const auto &required_font_dimensions{
				static_cast<const Font_Object &>(fonts.head()).font()->font_dimensions()};
		assert_all_fonts_have_same_dimensions(fonts, required_font_dimensions);
		try
		{
			const auto column_count_doesnt_matter{MAX_COLUMN_COUNT}; // TODO Use shared constant
			const auto file_name_doesnt_matter{""};                  // TODO Use shared constant
			auto target_font{make_shared<Delegating_Presentation_Model>(
					column_count_doesnt_matter,
					make_unique<Font_Model>(Font_Dimensions{required_font_dimensions.glyph_width,
			                                                        required_font_dimensions.glyph_height,
			                                                        total_glyph_count},
			                                        m_clock),
					file_name_doesnt_matter, m_clock)};

			auto total{0U};
			expr::for_each(fonts,
			               [&](const auto &font)
			               {
					       const auto &source_font{static_cast<const Font_Object &>(font).font()};
					       for (auto glyph_id{0U};
				                    glyph_id < source_font->font_dimensions().glyph_count;
				                    glyph_id++, total++)
					       {
						       target_font->set_active_glyph(total);
						       target_font->copy_to_active_glyph(*source_font, glyph_id);
					       }
				       });
			return make_shared<Font_Object>(target_font);
		}
		catch (const Base_Exception &cause)
		{
			return error_from(fmt::format("Error while merging fonts {}. {}",
			                              text::single_quoted(fonts.stringify()), cause.pretty_message()));
		}
		catch (const runtime_error &cause)
		{
			return error_from(fmt::format("Error while merging fonts {}. {}",
			                              text::single_quoted(fonts.stringify()), cause.what()));
		}
		catch (...)
		{
			return error_from(fmt::format("Unknown error while merging fonts {}.",
			                              text::single_quoted(fonts.stringify())));
		}
	}

	static const Variadic_Parameter PARAMETER{
			Token{Token::Type::ELLIPSIS, "...", Token::NO_POS},
			make_shared<Name_Expression>(Token{Token::Type::NAME, FONTS_PARAM_NAME, Token::NO_POS})};

	auto Merge_Fonts_Function::parameter_list() const -> const Expression & { return PARAMETER; }

	auto Merge_Fonts_Function::assert_at_least_one_font_given(const Expression &fonts_arguments) const -> void
	{
		if (fonts_arguments.is_nil())
		{
			throw appsl::Runtime_Error{fmt::format(
					"The {} argument to the {} function must not be empty. Pass in at least one "
					"font. Eventhough passing more than one font makes the most sense.",
					text::single_quoted(FONTS_PARAM_NAME),
					text::single_quoted(function_display_name()),
					text::single_quoted(fonts_arguments.stringify()))};
		}
	}
	auto Merge_Fonts_Function::assert_all_arguments_are_fonts(const Expression &fonts_arguments) const -> void
	{
		auto arg_pos{1U};
		expr::for_each(fonts_arguments,
		               [&](const auto &font)
		               {
				       if (!is_font(font))
				       {
					       throw appsl::Runtime_Error{fmt::format(
							       "The expression {} passed as the {} argument "
							       "(position {}) to the {} function is not a font.",
							       text::single_quoted(font.stringify()),
							       text::single_quoted(FONTS_PARAM_NAME), arg_pos,
							       text::single_quoted(function_display_name()))};
				       }
				       arg_pos++;
			       });
	}

	auto Merge_Fonts_Function::total_glyph_count(const Expression &fonts_arguments) -> Glyph_Count
	{
		auto total_glyph_count{0U};
		expr::for_each(fonts_arguments,
		               [&](const auto &font)
		               {
				       const auto &font_dimensions{static_cast<const Font_Object &>(font)
			                                                           .font()
			                                                           ->font_dimensions()};
				       total_glyph_count += font_dimensions.glyph_count;
			       });
		return total_glyph_count;
	}

	auto Merge_Fonts_Function::assert_all_fonts_have_same_dimensions(
			const Expression &fonts_arguments, const Font_Dimensions &required_font_dimensions) const
			-> void
	{
		auto arg_pos{1U};
		expr::for_each(fonts_arguments,
		               [&](const auto &font)
		               {
				       const auto &font_dimensions{static_cast<const Font_Object &>(font)
			                                                           .font()
			                                                           ->font_dimensions()};
				       if (font_dimensions.glyph_height != required_font_dimensions.glyph_height ||
			                   font_dimensions.glyph_width != required_font_dimensions.glyph_width)
				       {
					       throw appsl::Runtime_Error{fmt::format(
							       "The font passed as the {} argument (position {}) to "
							       "the {} function has wrong dimentions. Expected "
							       "dimensions are height={} and width={} but font "
							       "actually has height={} and width={}.",
							       text::single_quoted(FONTS_PARAM_NAME), arg_pos,
							       text::single_quoted(function_display_name()),
							       required_font_dimensions.glyph_height,
							       required_font_dimensions.glyph_width,
							       font_dimensions.glyph_height,
							       font_dimensions.glyph_width)};
				       }
				       arg_pos++;
			       });
	}

} // namespace fonted::scripting
