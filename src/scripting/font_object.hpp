// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "presentation_model.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using appsl::Custom_Object_Expression;
	using appsl::Expression;
	using appsl::Name_Expression;

	class Font_Object final : public Custom_Object_Expression
	{
	    private:
		shared_ptr<Presentation_Model> m_font;
		shared_ptr<const Expression> m_glyphs;

	    public:
		static const shared_ptr<Name_Expression> FONT_TYPE_TAG;

		explicit Font_Object(shared_ptr<Presentation_Model>);

		auto clone() const -> unique_ptr<Expression> override;

		auto head() const -> const Expression & override;
		auto tail() const -> const Expression & override;

		[[nodiscard]] auto tag() const -> const Expression & override;
		[[nodiscard]] auto font() const noexcept -> shared_ptr<Presentation_Model>;
	};

	inline auto Font_Object::tag() const -> const Expression & { return *FONT_TYPE_TAG; }

	inline auto Font_Object::font() const noexcept -> shared_ptr<Presentation_Model> { return m_font; }

	[[nodiscard]] inline auto is_font(const Expression &expression)
	{
		return expression.is_custom_object() && expression.tag() == *Font_Object::FONT_TYPE_TAG;
	}

} // namespace fonted::scripting
