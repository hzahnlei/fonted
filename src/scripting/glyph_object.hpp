// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "domain_model.hpp"
#include "presentation_model.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using appsl::Custom_Object_Expression;
	using appsl::Name_Expression;

	class Glyph_Object final : public Custom_Object_Expression
	{
	    private:
		shared_ptr<Presentation_Model> m_font;
		const Glyph_ID m_glyph_code;

	    public:
		static const shared_ptr<Name_Expression> GLYPH_TYPE_TAG;
		static const shared_ptr<Name_Expression> FONT_FIELD_NAME;
		static const shared_ptr<Name_Expression> GLYPH_ID_FIELD_NAME;

		Glyph_Object(shared_ptr<Presentation_Model>, const Glyph_ID);

		auto clone() const -> unique_ptr<Expression> override;

		[[nodiscard]] auto tag() const -> const Expression & override;

		[[nodiscard]] auto font() const noexcept -> shared_ptr<Presentation_Model>;
		[[nodiscard]] auto code() const noexcept -> Glyph_ID;
	};

	inline auto Glyph_Object::tag() const -> const Expression & { return *GLYPH_TYPE_TAG; }

	inline auto Glyph_Object::font() const noexcept -> shared_ptr<Presentation_Model> { return m_font; }

	inline auto Glyph_Object::code() const noexcept -> Glyph_ID { return m_glyph_code; }

} // namespace fonted::scripting
