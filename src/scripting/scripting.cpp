// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "scripting/scripting.hpp"
#include "glyph_editing_functions.hpp"
#include "scripting/copy_glyph_function.hpp"
#include "scripting/export_binary_font_function.hpp"
#include "scripting/font_object.hpp"
#include "scripting/load_editor_font_function.hpp"
#include "scripting/load_font_function.hpp"
#include "scripting/merge_fonts_function.hpp"
#include "scripting/new_font_function.hpp"
#include "scripting/prelude.hpp"
#include "scripting/save_editor_font_function.hpp"
#include "scripting/save_font_function.hpp"
#include "use_stl.hpp"

namespace fonted::scripting
{

	using namespace appsl;

	auto make_scripting_environment_headless(const Font_Model::Now_Function &clock) -> shared_ptr<Stack_Frame>
	{
		auto runtime{make_runtime()};

		runtime->define_local_var("new_font", make_unique<New_Font_Function>("new_font", clock));
		runtime->define_local_var("load_font!", make_unique<Load_Font_Function>("load_font!", clock));
		runtime->define_local_var("save_font!", make_unique<Save_Font_Function>("save_font!"));
		runtime->define_local_var("flip_glyph_vertically!",
		                          flip_glyph_vertically_function("flip_glyph_vertically!"));
		runtime->define_local_var("flip_glyph_horizontally!",
		                          flip_glyph_horizontally_function("flip_glyph_horizontally!"));
		runtime->define_local_var("invert_glyph!", glyph_inverting_function("invert_glyph!"));
		runtime->define_local_var("clear_glyph!", clear_glyph_function("clear_glyph!"));
		runtime->define_local_var("set_glyph!", set_glyph_function("set_glyph!"));
		runtime->define_local_var("scroll_glyph_left!", scroll_glyph_left_function("scroll_glyph_left!"));
		runtime->define_local_var("scroll_glyph_right!", scroll_glyph_right_function("scroll_glyph_right!"));
		runtime->define_local_var("scroll_glyph_up!", scroll_glyph_up_function("scroll_glyph_up!"));
		runtime->define_local_var("scroll_glyph_down!", scroll_glyph_down_function("scroll_glyph_down!"));
		runtime->define_local_var("copy_glyph!", make_unique<Copy_Glyph_Function>("copy_glyph!", clock));
		runtime->define_local_var("merged_font", make_unique<Merge_Fonts_Function>("merged_font", clock));
		runtime->define_local_var("export_binary_font!",
		                          make_unique<Export_Binary_Font_Function>("export_binary_font!"));

		execute_script(PRELUDE, *runtime);

		return runtime;
	}

	auto make_scripting_environment_gui(shared_ptr<Presentation_Model> font) -> shared_ptr<Stack_Frame>
	{
		auto runtime{make_scripting_environment_headless(font->clock())};
		runtime->define_local_var("_font", make_shared<Font_Object>(font));
		runtime->define_local_var("load!", make_unique<Load_Editor_Font_Function>("load!", font));
		runtime->define_local_var("save!", make_unique<Save_Editor_Font_Function>("save!", font));
		return runtime;
	}

} // namespace fonted::scripting
