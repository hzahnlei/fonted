// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "copy_glyph_function.hpp"
#include "font_object.hpp"
#include "scripting/argument_resolver.hpp"
#include "scripting/scripting.hpp"
#include "use_stl.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace fonted::scripting
{

	using namespace junkbox;
	using namespace appsl;

	Copy_Glyph_Function::Copy_Glyph_Function(const Fun_Display_Name &name, const Font_Model::Now_Function &clock)
			: Abstract_Builtin_Function{name}, m_clock{clock}
	{
	}

	auto Copy_Glyph_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Copy_Glyph_Function const *const>(&other);
	}

	auto Copy_Glyph_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Copy_Glyph_Function>(function_display_name(), m_clock);
	}

	static const string FROM_PARAM_NAME{"from"};
	static const string TO_PARAM_NAME{"to"};

	auto Copy_Glyph_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto from_argument{asserted_from_argument(stack)};
		auto source_font{static_cast<const Glyph_Object &>(*from_argument).font()};
		const auto source_font_object{make_shared<Font_Object>(source_font)};
		auto to_argument{asserted_to_argument(stack, source_font_object)};
		try
		{
			const auto source_glyph_id{static_cast<const Glyph_Object &>(*from_argument).code()};
			auto target_font{static_cast<const Glyph_Object &>(*to_argument).font()};
			const auto target_glyph_id{static_cast<const Glyph_Object &>(*to_argument).code()};
			target_font->set_active_glyph(target_glyph_id);
			target_font->copy_to_active_glyph(*source_font, source_glyph_id);
			return to_argument;
		}
		catch (const Base_Exception &cause)
		{
			return error_from(fmt::format("Failed to copy glyph. {}", cause.pretty_message()));
		}
		catch (const runtime_error &cause)
		{
			return error_from(fmt::format("Error while copying glpyh. {}", cause.what()));
		}
		catch (...)
		{
			return error_from("Unknown error while copying glyph.");
		}
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_shared<Name_Expression>(Token{Token::Type::NAME, FROM_PARAM_NAME, Token::NO_POS}),
			make_shared<Name_Expression>(Token{Token::Type::NAME, TO_PARAM_NAME, Token::NO_POS})};

	auto Copy_Glyph_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

	auto Copy_Glyph_Function::asserted_from_argument(const Stack_Frame &stack) const -> unique_ptr<const Expression>
	{
		const auto &from_argument{stack.lookup(FROM_PARAM_NAME)};
		return asserted_glyph_argument(from_argument, function_display_name(), FROM_PARAM_NAME);
	}

	auto Copy_Glyph_Function::asserted_to_argument(const Stack_Frame &stack,
	                                               shared_ptr<const Expression> font) const
			-> unique_ptr<const Expression>
	{
		const auto &to_argument{stack.lookup(TO_PARAM_NAME)};
		return asserted_glyph_argument(to_argument, function_display_name(), TO_PARAM_NAME, font);
	}

} // namespace fonted::scripting
