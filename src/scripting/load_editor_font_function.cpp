// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "load_editor_font_function.hpp"
#include "font_object.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace fonted::scripting
{

	using namespace junkbox;
	using namespace appsl;

	Load_Editor_Font_Function::Load_Editor_Font_Function(const Fun_Display_Name &name,
	                                                     shared_ptr<Presentation_Model> font)
			: Abstract_Builtin_Function{name}, m_font{font}
	{
	}

	auto Load_Editor_Font_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Load_Editor_Font_Function const *const>(&other);
	}

	auto Load_Editor_Font_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Load_Editor_Font_Function>(function_display_name(), m_font);
	}

	static const string FILE_NAME_PARAM_NAME{"file_name"};

	auto Load_Editor_Font_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &file_name{stack.lookup(FILE_NAME_PARAM_NAME)};
		if (!file_name.is_text())
		{
			return error_from(
					fmt::format("The {} argument to {} function must be a text literal. This {} is "
			                            "no text literal.",
			                            text::single_quoted(FILE_NAME_PARAM_NAME),
			                            text::single_quoted(function_display_name()),
			                            text::single_quoted(file_name.stringify())));
		}
		if (file_name.text_value().length() > Presentation_Model::MAX_FILE_NAME_SIZE)
		{
			return error_from(fmt::format(
					"File name too long. The {} argument to {} function must be no "
					"longer than {} characters. These are more than {} characters: {}",
					text::single_quoted(FILE_NAME_PARAM_NAME),
					text::single_quoted(function_display_name()),
					Presentation_Model::MAX_FILE_NAME_SIZE, Presentation_Model::MAX_FILE_NAME_SIZE,
					text::single_quoted(file_name.stringify())));
		}
		try
		{
			// ImGui works with char*, therefore this antiquated char copy. And the length check!
			strcpy(m_font->file_name(), file_name.text_value().c_str());
			m_font->load();
			// // FIXME redefine global var
			// stack.define_local_var("_font", result);
			// auto result{make_shared<Font_Object>(m_font)};
			// return result;
			return make_shared<Font_Object>(m_font);
		}
		catch (const Base_Exception &cause)
		{
			return error_from(fmt::format("Error while loading font file {}. {}",
			                              text::single_quoted(file_name.stringify()),
			                              cause.pretty_message()));
		}
		catch (const runtime_error &cause)
		{
			return error_from(fmt::format("Error while loading font file {}. {}",
			                              text::single_quoted(file_name.stringify()), cause.what()));
		}
		catch (...)
		{
			return error_from(fmt::format("Unknown error while loading font file {}.",
			                              text::single_quoted(file_name.stringify())));
		}
	}

	static const Name_Expression PARAMETER{Token{Token::Type::NAME, FILE_NAME_PARAM_NAME, Token::NO_POS}};

	auto Load_Editor_Font_Function::parameter_list() const -> const Expression & { return PARAMETER; }

} // namespace fonted::scripting
