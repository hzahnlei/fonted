// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "glyph_object.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using namespace appsl;

	const shared_ptr<Name_Expression> Glyph_Object::GLYPH_TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Fonted_Glyph", Token::NO_POS})};
	const shared_ptr<Name_Expression> Glyph_Object::FONT_FIELD_NAME{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "font", Token::NO_POS})};
	const shared_ptr<Name_Expression> Glyph_Object::GLYPH_ID_FIELD_NAME{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "id", Token::NO_POS})};

	auto properties(const Glyph_ID &id)
	{
		auto glyph_id{make_shared<Name_Value_Pair_Expression>(
				Token::NONE, Glyph_Object::GLYPH_ID_FIELD_NAME,
				make_shared<Integer_Literal_Expression>(
						Token{Token::Type::INTEGER_LITERAL, to_string(id), Token::NO_POS},
						id))};
		return make_shared<Record_Expression>(Token::NONE, Record_Expression::Name_Value_List{glyph_id});
	}

	Glyph_Object::Glyph_Object(shared_ptr<Presentation_Model> font, const Glyph_ID glyph_code)
			: Custom_Object_Expression{Token::NONE, GLYPH_TYPE_TAG, properties(glyph_code)}, m_font{font},
			  m_glyph_code{glyph_code}
	{
	}

	auto Glyph_Object::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Glyph_Object>(m_font, m_glyph_code);
	}

} // namespace fonted::scripting
