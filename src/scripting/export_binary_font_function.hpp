// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "presentation_model.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using appsl::Abstract_Builtin_Function;
	using appsl::Expression;
	using appsl::Stack_Frame;

	class Export_Binary_Font_Function final : public Abstract_Builtin_Function
	{
	    public:
		Export_Binary_Font_Function(const Fun_Display_Name &);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;
	};

} // namespace fonted::scripting
