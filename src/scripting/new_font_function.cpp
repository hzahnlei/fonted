// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "new_font_function.hpp"
#include "font_object.hpp"
#include "scripting/scripting.hpp"
#include "use_stl.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace fonted::scripting
{

	using namespace junkbox;
	using namespace appsl;

	New_Font_Function::New_Font_Function(const Fun_Display_Name &name, const Font_Model::Now_Function &clock)
			: Abstract_Builtin_Function{name}, m_clock{clock}
	{
	}

	auto New_Font_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<New_Font_Function const *const>(&other);
	}

	auto New_Font_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<New_Font_Function>(function_display_name(), m_clock);
	}

	static const string GLYPH_COUNT_PARAM_NAME{"glyph_count"};
	static const string GLYPH_WIDTH_PARAM_NAME{"glyph_width"};
	static const string GLYPH_HEIGHT_PARAM_NAME{"glyph_height"};
	static const string FONT_NAME_PARAM_NAME{"name"};
	static const string AUTHOR_PARAM_NAME{"author"};
	static const string CODEPAGE_PARAM_NAME{"codepage"};
	static const string DESCRIPTION_PARAM_NAME{"description"};

	auto New_Font_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		try
		{
			const auto glyph_count{asserted_glyph_count_argument(stack)};
			const auto glyph_width{asserted_glyph_width_argument(stack)};
			const auto glyph_height{asserted_glyph_height_argument(stack)};
			const auto font_name{asserted_font_name_argument(stack)};
			const auto author{asserted_author_argument(stack)};
			const auto codepage{asserted_codepage_argument(stack)};
			const auto description{asserted_description_argument(stack)};

			auto domain_model{make_unique<Font_Model>(
					Font_Dimensions{glyph_width, glyph_height, glyph_count}, m_clock)};
			domain_model->set_font_name(font_name);
			domain_model->set_author_name(author);
			domain_model->set_code_page(codepage);
			domain_model->set_description(description);
			const auto column_count_doesnt_matter{1}; // TODO Use shared constant
			const auto file_name_doesnt_matter{""};
			auto presentation_model{make_shared<Delegating_Presentation_Model>(
					column_count_doesnt_matter, move(domain_model), file_name_doesnt_matter,
					m_clock)};

			return make_shared<Font_Object>(presentation_model);
		}
		catch (const Base_Exception &cause)
		{
			return error_from(fmt::format("Failed to create new font. {}", cause.pretty_message()));
		}
		catch (const runtime_error &cause)
		{
			return error_from(fmt::format("Failed to create new font. {}", cause.what()));
		}
		catch (...)
		{
			return error_from("Unknown error while creating new font.");
		}
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_shared<Name_Expression>(Token{Token::Type::NAME, GLYPH_COUNT_PARAM_NAME, Token::NO_POS}),
			make_shared<List_Expression>(
					Token::NONE,
					make_shared<Name_Expression>(Token{Token::Type::NAME, GLYPH_WIDTH_PARAM_NAME,
	                                                                   Token::NO_POS}),
					make_shared<List_Expression>(
							Token::NONE,
							make_shared<Name_Expression>(Token{Token::Type::NAME,
	                                                                                   GLYPH_HEIGHT_PARAM_NAME,
	                                                                                   Token::NO_POS}),
							make_shared<List_Expression>(
									Token::NONE,
									make_shared<Name_Expression>(
											Token{Token::Type::NAME,
	                                                                                      FONT_NAME_PARAM_NAME,
	                                                                                      Token::NO_POS}),
									make_shared<List_Expression>(
											Token::NONE,
											make_shared<Name_Expression>(Token{
													Token::Type::NAME,
													AUTHOR_PARAM_NAME,
													Token::NO_POS}),
											make_shared<List_Expression>(
													Token::NONE,
													make_shared<Name_Expression>(Token{
															Token::Type::NAME,
															CODEPAGE_PARAM_NAME,
															Token::NO_POS}),
													make_shared<Name_Expression>(Token{
															Token::Type::NAME,
															DESCRIPTION_PARAM_NAME,
															Token::NO_POS}))))))};

	auto New_Font_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

	auto New_Font_Function::asserted_glyph_count_argument(const Stack_Frame &stack) const -> Glyph_Count
	{
		const auto &glyph_count_expr{stack.lookup(GLYPH_COUNT_PARAM_NAME)};
		if (!glyph_count_expr.is_integer())
		{
			throw appsl::Runtime_Error(fmt::format(
					"The {} argument to {} function must be an integer literal. This {} is "
					"no integer literal.",
					text::single_quoted(GLYPH_COUNT_PARAM_NAME),
					text::single_quoted(function_display_name()),
					text::single_quoted(glyph_count_expr.stringify())));
		}
		const auto glyph_count{glyph_count_expr.int_value()};
		if (glyph_count < MIN_GLYPH_COUNT || glyph_count > MAX_GLYPH_COUNT)
		{
			throw appsl::Runtime_Error(fmt::format(
					"The {} = {} argument to {} function is out of bounds ({} ≤ {} ≤ {}).",
					text::single_quoted(GLYPH_COUNT_PARAM_NAME), glyph_count,
					text::single_quoted(function_display_name()), MIN_GLYPH_COUNT,
					GLYPH_COUNT_PARAM_NAME, MAX_GLYPH_COUNT));
		}
		return static_cast<Glyph_Count>(glyph_count);
	}

	auto New_Font_Function::asserted_glyph_width_argument(const Stack_Frame &stack) const -> Glyph_Width
	{
		const auto &glyph_width_expr{stack.lookup(GLYPH_WIDTH_PARAM_NAME)};
		if (!glyph_width_expr.is_integer())
		{
			throw appsl::Runtime_Error(fmt::format(
					"The {} argument to {} function must be an integer literal. This {} is "
					"no integer literal.",
					text::single_quoted(GLYPH_WIDTH_PARAM_NAME),
					text::single_quoted(function_display_name()),
					text::single_quoted(glyph_width_expr.stringify())));
		}
		const auto glyph_width{glyph_width_expr.int_value()};
		if (glyph_width < MIN_GLYPH_WIDTH || glyph_width > MAX_GLYPH_WIDTH)
		{
			throw appsl::Runtime_Error(fmt::format(
					"The {} = {} argument to {} function is out of bounds ({} ≤ {} ≤ {}).",
					text::single_quoted(GLYPH_WIDTH_PARAM_NAME), glyph_width,
					text::single_quoted(function_display_name()), MIN_GLYPH_WIDTH,
					GLYPH_WIDTH_PARAM_NAME, MAX_GLYPH_WIDTH));
		}
		return static_cast<Glyph_Width>(glyph_width);
	}

	auto New_Font_Function::asserted_glyph_height_argument(const Stack_Frame &stack) const -> Glyph_Height
	{
		const auto &glyph_height_expr{stack.lookup(GLYPH_HEIGHT_PARAM_NAME)};
		if (!glyph_height_expr.is_integer())
		{
			throw appsl::Runtime_Error(fmt::format(
					"The {} argument to {} function must be an integer literal. This {} is "
					"no integer literal.",
					text::single_quoted(GLYPH_WIDTH_PARAM_NAME),
					text::single_quoted(function_display_name()),
					text::single_quoted(glyph_height_expr.stringify())));
		}
		const auto glyph_height{glyph_height_expr.int_value()};
		if (glyph_height < MIN_GLYPH_HEIGHT || glyph_height > MAX_GLYPH_HEIGHT)
		{
			throw appsl::Runtime_Error(fmt::format(
					"The {} = {} argument to {} function is out of bounds ({} ≤ {} ≤ {}).",
					text::single_quoted(GLYPH_WIDTH_PARAM_NAME), glyph_height,
					text::single_quoted(function_display_name()), MIN_GLYPH_HEIGHT,
					GLYPH_WIDTH_PARAM_NAME, MAX_GLYPH_HEIGHT));
		}
		return static_cast<Glyph_Height>(glyph_height);
	}

	auto New_Font_Function::asserted_font_name_argument(const Stack_Frame &stack) const -> string
	{
		const auto &font_name_expr{stack.lookup(FONT_NAME_PARAM_NAME)};
		if (!font_name_expr.is_text())
		{
			throw appsl::Runtime_Error(
					fmt::format("The {} argument to {} function must be a text literal. This {} is "
			                            "no text literal.",
			                            text::single_quoted(FONT_NAME_PARAM_NAME),
			                            text::single_quoted(function_display_name()),
			                            text::single_quoted(font_name_expr.stringify())));
		}
		return font_name_expr.text_value();
	}

	auto New_Font_Function::asserted_author_argument(const Stack_Frame &stack) const -> string
	{
		const auto &author_name_expr{stack.lookup(AUTHOR_PARAM_NAME)};
		if (!author_name_expr.is_text())
		{
			throw appsl::Runtime_Error(
					fmt::format("The {} argument to {} function must be a text literal. This {} is "
			                            "no text literal.",
			                            text::single_quoted(AUTHOR_PARAM_NAME),
			                            text::single_quoted(function_display_name()),
			                            text::single_quoted(author_name_expr.stringify())));
		}
		return author_name_expr.text_value();
	}

	auto New_Font_Function::asserted_codepage_argument(const Stack_Frame &stack) const -> string
	{
		const auto &codepage_expr{stack.lookup(CODEPAGE_PARAM_NAME)};
		if (!codepage_expr.is_text())
		{
			throw appsl::Runtime_Error(
					fmt::format("The {} argument to {} function must be a text literal. This {} is "
			                            "no text literal.",
			                            text::single_quoted(CODEPAGE_PARAM_NAME),
			                            text::single_quoted(function_display_name()),
			                            text::single_quoted(codepage_expr.stringify())));
		}
		return codepage_expr.text_value();
	}

	auto New_Font_Function::asserted_description_argument(const Stack_Frame &stack) const -> string
	{
		const auto &description_expr{stack.lookup(DESCRIPTION_PARAM_NAME)};
		if (!description_expr.is_text())
		{
			throw appsl::Runtime_Error(
					fmt::format("The {} argument to {} function must be a text literal. This {} is "
			                            "no text literal.",
			                            text::single_quoted(DESCRIPTION_PARAM_NAME),
			                            text::single_quoted(function_display_name()),
			                            text::single_quoted(description_expr.stringify())));
		}
		return description_expr.text_value();
	}

} // namespace fonted::scripting
