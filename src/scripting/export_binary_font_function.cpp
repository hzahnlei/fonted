// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "export_binary_font_function.hpp"
#include "font_object.hpp"
#include "use_stl.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace fonted::scripting
{

	using namespace junkbox;
	using namespace appsl;

	Export_Binary_Font_Function::Export_Binary_Font_Function(const Fun_Display_Name &name)
			: Abstract_Builtin_Function{name}
	{
	}

	auto Export_Binary_Font_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Export_Binary_Font_Function const *const>(&other);
	}

	auto Export_Binary_Font_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Export_Binary_Font_Function>(function_display_name());
	}

	static const string FONT_PARAM_NAME{"font"};
	static const string FILE_NAME_PARAM_NAME{"file_name"};

	auto Export_Binary_Font_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &font{stack.lookup(FONT_PARAM_NAME)};
		if (font.custom_object_type() != *Font_Object::FONT_TYPE_TAG)
		{
			return error_from(fmt::format(
					"The {} argument to the {} function has to be a font. This {} is no font.",
					text::single_quoted(FONT_PARAM_NAME),
					text::single_quoted(function_display_name()),
					text::single_quoted(font.stringify())));
		}
		const auto &file_name{stack.lookup(FILE_NAME_PARAM_NAME)};
		if (!file_name.is_text())
		{
			return error_from(
					fmt::format("The {} argument to {} function must be a text literal. This {} is "
			                            "no text literal.",
			                            text::single_quoted(FILE_NAME_PARAM_NAME),
			                            text::single_quoted(function_display_name()),
			                            text::single_quoted(file_name.stringify())));
		}
		if (file_name.text_value().length() > 255)
		{
			return error_from(fmt::format(
					"File name too long. The {} argument to {} function must be no "
					"longer than {} characters. These are more than {} characters: {}",
					text::single_quoted(FILE_NAME_PARAM_NAME),
					text::single_quoted(function_display_name()),
					Presentation_Model::MAX_FILE_NAME_SIZE, Presentation_Model::MAX_FILE_NAME_SIZE,
					text::single_quoted(file_name.stringify())));
		}
		try
		{
			// ImGui works with char*, therefore this antiquated char copy. And the length check!
			const auto &font_expression{static_cast<const Font_Object &>(font)};
			auto unboxed_font{font_expression.font()};
			strcpy(unboxed_font->file_name(), file_name.text_value().c_str());
			unboxed_font->export_binary();
			return stack.boolean_literal_from(true);
		}
		catch (const Base_Exception &cause)
		{
			return error_from(fmt::format("Error while exporting binary font file {}. {}",
			                              text::single_quoted(file_name.stringify()),
			                              cause.pretty_message()));
		}
		catch (const runtime_error &cause)
		{
			return error_from(fmt::format("Error while exporting binary font file {}. {}",
			                              text::single_quoted(file_name.stringify()), cause.what()));
		}
		catch (...)
		{
			return error_from(fmt::format("Unknown error exporting binary storing font file {}.",
			                              text::single_quoted(file_name.stringify())));
		}
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_shared<Name_Expression>(Token{Token::Type::NAME, FONT_PARAM_NAME, Token::NO_POS}),
			make_shared<Name_Expression>(Token{Token::Type::NAME, FILE_NAME_PARAM_NAME, Token::NO_POS})};

	auto Export_Binary_Font_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace fonted::scripting
