// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "font_object.hpp"
#include "glyph_object.hpp"
#include "use_stl.hpp"

namespace fonted::scripting
{

	using namespace appsl;

	const shared_ptr<Name_Expression> Font_Object::FONT_TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Fonted_Font", Token::NO_POS})};

	auto properties(Presentation_Model &font)
	{
		auto name{make_shared<Name_Value_Pair_Expression>(
				Token::NONE,
				make_shared<Name_Expression>(Token{Token::Type::NAME, "name", Token::NO_POS}),
				make_shared<Text_Literal_Expression>(
						Token{Token::Type::TEXT_LITERAL, font.font_name(), Token::NO_POS}))};
		auto author{make_shared<Name_Value_Pair_Expression>(
				Token::NONE,
				make_shared<Name_Expression>(Token{Token::Type::NAME, "author", Token::NO_POS}),
				make_shared<Text_Literal_Expression>(
						Token{Token::Type::TEXT_LITERAL, font.author_name(), Token::NO_POS}))};
		auto code_page{make_shared<Name_Value_Pair_Expression>(
				Token::NONE,
				make_shared<Name_Expression>(Token{Token::Type::NAME, "code_page", Token::NO_POS}),
				make_shared<Text_Literal_Expression>(Token{Token::Type::TEXT_LITERAL,
		                                                           font.code_page_name(), Token::NO_POS}))};
		auto description{make_shared<Name_Value_Pair_Expression>(
				Token::NONE,
				make_shared<Name_Expression>(Token{Token::Type::NAME, "description", Token::NO_POS}),
				make_shared<Text_Literal_Expression>(
						Token{Token::Type::TEXT_LITERAL, font.description(), Token::NO_POS}))};
		return make_shared<Record_Expression>(
				Token::NONE, Record_Expression::Name_Value_List{name, author, code_page, description});
	}

	auto remaining_glyph_enumeration(shared_ptr<Presentation_Model> font, const Glyph_ID curr_id)
			-> shared_ptr<const Expression>
	{
		if (curr_id >= font->font_dimensions().glyph_count)
		{
			return Nil_Expression::NIL;
		}
		return make_shared<List_Expression>(Token::NONE, make_shared<Glyph_Object>(font, curr_id),
		                                    remaining_glyph_enumeration(font, curr_id + 1));
	}

	inline auto glyph_enumeration(shared_ptr<Presentation_Model> font) -> shared_ptr<const Expression>
	{
		return remaining_glyph_enumeration(font, 0);
	}

	Font_Object::Font_Object(shared_ptr<Presentation_Model> font)
			: Custom_Object_Expression{Token::NONE, FONT_TYPE_TAG, properties(*font)}, m_font{font}
	{
		// This is my brutal way to iterate over the glyphs. I am building a list expression from all glyphs so
		// that the interpreter can iterate them. However, this might become a problem for big fonts with many
		// glyphs. A better way would be to use a generator.
		m_glyphs = glyph_enumeration(m_font);
	}

	auto Font_Object::clone() const -> unique_ptr<Expression> { return make_unique<Font_Object>(m_font); }

	auto Font_Object::head() const -> const Expression & { return m_glyphs->head(); }

	auto Font_Object::tail() const -> const Expression & { return m_glyphs->tail(); }

} // namespace fonted::scripting
