// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "presentation_model.hpp"
#include "scripting/glyph_object.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using appsl::Abstract_Builtin_Function;
	using appsl::Expression;
	using appsl::Stack_Frame;

	class Copy_Glyph_Function final : public Abstract_Builtin_Function
	{
	    private:
		const Font_Model::Now_Function m_clock;

	    public:
		explicit Copy_Glyph_Function(const Fun_Display_Name &, const Font_Model::Now_Function &);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;

	    private:
		auto asserted_from_argument(const Stack_Frame &) const -> unique_ptr<const Expression>;
		auto asserted_to_argument(const Stack_Frame &, shared_ptr<const Expression> font) const
				-> unique_ptr<const Expression>;
	};

} // namespace fonted::scripting
