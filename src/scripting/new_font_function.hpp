// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "presentation_model.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using appsl::Abstract_Builtin_Function;
	using appsl::Expression;
	using appsl::Stack_Frame;

	class New_Font_Function final : public Abstract_Builtin_Function
	{
	    private:
		const Font_Model::Now_Function m_clock;

	    public:
		explicit New_Font_Function(const Fun_Display_Name &, const Font_Model::Now_Function &);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;

	    private:
		auto asserted_glyph_count_argument(const Stack_Frame &) const -> Glyph_Count;
		auto asserted_glyph_width_argument(const Stack_Frame &) const -> Glyph_Width;
		auto asserted_glyph_height_argument(const Stack_Frame &) const -> Glyph_Height;
		auto asserted_font_name_argument(const Stack_Frame &) const -> string;
		auto asserted_author_argument(const Stack_Frame &) const -> string;
		auto asserted_codepage_argument(const Stack_Frame &) const -> string;
		auto asserted_description_argument(const Stack_Frame &) const -> string;
	};

} // namespace fonted::scripting
