// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "glyph_editing_functions.hpp"
#include "argument_resolver.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace fonted::scripting
{

	using namespace junkbox;
	using namespace appsl;

	Unary_Glyph_Editing_Function::Unary_Glyph_Editing_Function(const Fun_Display_Name &name,
	                                                           const string &transformation_name,
	                                                           const Transform_Function &transform)
			: Abstract_Builtin_Function{name}, m_transformation_name{transformation_name},
			  m_transform{transform}
	{
	}

	auto Unary_Glyph_Editing_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Unary_Glyph_Editing_Function const *const>(&other);
	}

	auto Unary_Glyph_Editing_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(function_display_name(), m_transformation_name,
		                                                 m_transform);
	}

	static const string GLYPH_PARAM_NAME{"glyph"};

	auto Unary_Glyph_Editing_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &glyph_argument{stack.lookup(GLYPH_PARAM_NAME)};
		auto glyph{asserted_glyph_argument(glyph_argument, function_display_name(), GLYPH_PARAM_NAME)};
		try
		{
			m_transform(static_cast<const Glyph_Object &>(*glyph));
			return glyph;
		}
		catch (const Base_Exception &cause)
		{
			return error_from(fmt::format("Error while {} glyph {}. {}", m_transformation_name,
			                              text::single_quoted(glyph_argument.stringify()),
			                              cause.pretty_message()));
		}
		catch (const runtime_error &cause)
		{
			return error_from(fmt::format("Error while {} glpyh {}. {}", m_transformation_name,
			                              text::single_quoted(glyph_argument.stringify()), cause.what()));
		}
		catch (...)
		{
			return error_from(fmt::format("Unknown error while inverting glyph {}.",
			                              text::single_quoted(glyph_argument.stringify())));
		}
	}

	static const Name_Expression PARAMETER{Token{Token::Type::NAME, GLYPH_PARAM_NAME, Token::NO_POS}};

	auto Unary_Glyph_Editing_Function::parameter_list() const -> const Expression & { return PARAMETER; }

	auto glyph_inverting_function(const Abstract_Builtin_Function::Fun_Display_Name &name)
			-> unique_ptr<const Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(name, "inverting",
		                                                 [](const auto &glyph_expression)
		                                                 {
									 auto font{glyph_expression.font()};
									 font->set_active_glyph(
											 glyph_expression.code());
									 font->invert_active_glyph();
								 });
	}

	auto flip_glyph_vertically_function(const Abstract_Builtin_Function::Fun_Display_Name &name)
			-> unique_ptr<const Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(name, "vertically flipping",
		                                                 [](const auto &glyph_expression)
		                                                 {
									 auto font{glyph_expression.font()};
									 font->set_active_glyph(
											 glyph_expression.code());
									 font->flip_active_glyph_vertically();
								 });
	}

	auto flip_glyph_horizontally_function(const Abstract_Builtin_Function::Fun_Display_Name &name)
			-> unique_ptr<const Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(name, "hotizontally flipping",
		                                                 [](const auto &glyph_expression)
		                                                 {
									 auto font{glyph_expression.font()};
									 font->set_active_glyph(
											 glyph_expression.code());
									 font->flip_active_glyph_horizontally();
								 });
	}

	auto clear_glyph_function(const Abstract_Builtin_Function::Fun_Display_Name &name)
			-> unique_ptr<const Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(name, "clearing",
		                                                 [](const auto &glyph_expression)
		                                                 {
									 auto font{glyph_expression.font()};
									 font->set_active_glyph(
											 glyph_expression.code());
									 font->clear_active_glyph();
								 });
	}

	auto set_glyph_function(const Abstract_Builtin_Function::Fun_Display_Name &name) -> unique_ptr<const Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(name, "setting",
		                                                 [](const auto &glyph_expression)
		                                                 {
									 auto font{glyph_expression.font()};
									 font->set_active_glyph(
											 glyph_expression.code());
									 font->set_active_glyph();
								 });
	}

	auto scroll_glyph_left_function(const Abstract_Builtin_Function::Fun_Display_Name &name)
			-> unique_ptr<const Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(name, "left scrolling",
		                                                 [](const auto &glyph_expression)
		                                                 {
									 auto font{glyph_expression.font()};
									 font->set_active_glyph(
											 glyph_expression.code());
									 font->active_glyph_scroll_left();
								 });
		;
	}

	auto scroll_glyph_right_function(const Abstract_Builtin_Function::Fun_Display_Name &name)
			-> unique_ptr<const Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(name, "right scrolling",
		                                                 [](const auto &glyph_expression)
		                                                 {
									 auto font{glyph_expression.font()};
									 font->set_active_glyph(
											 glyph_expression.code());
									 font->active_glyph_scroll_right();
								 });
	}

	auto scroll_glyph_up_function(const Abstract_Builtin_Function::Fun_Display_Name &name)
			-> unique_ptr<const Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(name, "up scrolling",
		                                                 [](const auto &glyph_expression)
		                                                 {
									 auto font{glyph_expression.font()};
									 font->set_active_glyph(
											 glyph_expression.code());
									 font->active_glyph_scroll_up();
								 });
	}

	auto scroll_glyph_down_function(const Abstract_Builtin_Function::Fun_Display_Name &name)
			-> unique_ptr<const Expression>
	{
		return make_unique<Unary_Glyph_Editing_Function>(name, "down scrolling",
		                                                 [](const auto &glyph_expression)
		                                                 {
									 auto font{glyph_expression.font()};
									 font->set_active_glyph(
											 glyph_expression.code());
									 font->active_glyph_scroll_down();
								 });
	}

} // namespace fonted::scripting
