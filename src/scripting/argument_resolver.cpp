// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "scripting/argument_resolver.hpp"
#include "domain_model.hpp"
#include "presentation_model.hpp"
#include "scripting/font_object.hpp"
#include "scripting/glyph_object.hpp"
#include "scripting/scripting.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace fonted::scripting
{

	using namespace appsl;
	using namespace junkbox;

	// Forward declarations
	auto asserted_custom_glyph_argument(const Expression &) -> unique_ptr<const Expression>;
	auto asserted_glyph_index_argument(const Expression &, shared_ptr<const Expression>)
			-> unique_ptr<const Expression>;
	auto asserted_tagged_argument(const Expression &) -> unique_ptr<const Expression>;
	auto asserted_glyph_record_argument(const Expression &) -> unique_ptr<const Expression>;
	auto asserted_glyph_pair_argument(const Expression &) -> unique_ptr<const Expression>;

	//---- Public functions -----------------------------------------------

	auto asserted_glyph_argument(const Expression &argument, const string &function_name,
	                             const string &parameter_name) -> unique_ptr<const Expression>
	{
		return asserted_glyph_argument(argument, function_name, parameter_name, Nil_Expression::NIL);
	}

	auto asserted_glyph_argument(const Expression &argument, const string &function_name,
	                             const string &parameter_name, shared_ptr<const Expression> implied_font)
			-> unique_ptr<const Expression>
	{
		try
		{
			// Conditions are prioritized, most specific first least specific last
			if (argument.is_custom_object())
			{
				return asserted_custom_glyph_argument(argument);
			}
			if (argument.is_integer())
			{
				return asserted_glyph_index_argument(argument, implied_font);
			}
			if (argument.is_tagged())
			{
				return asserted_tagged_argument(argument);
			}
			if (argument.is_record())
			{
				return asserted_glyph_record_argument(argument);
			}
			if (argument.is_iterable())
			{
				return asserted_glyph_pair_argument(argument);
			}
			throw "argument has incompatible type of expression."s;
		}
		catch (const string &cause)
		{
			throw appsl::Runtime_Error(fmt::format(
					"The {} argument to the {} function has to be an expression of custom type {}. "
					"At least it must be assignment compatible. That is a list with two elements "
					"(font and glyph ID) or a record with fields {} and {}. In some cases the font "
					"is implied. In such a case even a lone integer (in conjunction with the "
					"implied font) can be cast to type {}. This {} however cannot be cast to {} "
					"because {}",
					text::single_quoted(parameter_name), text::single_quoted(function_name),
					text::single_quoted(Glyph_Object::GLYPH_TYPE_TAG->name()),
					text::single_quoted(Glyph_Object::FONT_FIELD_NAME->name()),
					text::single_quoted(Glyph_Object::GLYPH_ID_FIELD_NAME->name()),
					text::single_quoted(Glyph_Object::GLYPH_TYPE_TAG->name()),
					text::single_quoted(argument.stringify()),
					text::single_quoted(Glyph_Object::GLYPH_TYPE_TAG->name()), cause));
		}
	}

	//---- Helper functions -----------------------------------------------

	auto asserted_custom_glyph_argument(const Expression &argument) -> unique_ptr<const Expression>
	{
		if (argument.tag() != *Glyph_Object::GLYPH_TYPE_TAG)
		{
			throw fmt::format("custom object {} has incompatible type {}.",
			                  text::single_quoted(argument.stringify()),
			                  text::single_quoted(argument.tag().name()));
		}
		return argument.clone();
	}

	auto asserted_glyph_index_argument(const Expression &argument, shared_ptr<const Expression> implied_font)
			-> unique_ptr<const Expression>
	{
		if (implied_font == nullptr || implied_font->is_nil())
		{
			throw fmt::format("the integer {} can be accepted as a glyph ID only in conjunction with an "
			                  "implied font. However, no implied font given.",
			                  text::single_quoted(argument.stringify()));
		}
		const auto &font_object{static_cast<const Font_Object &>(*implied_font)};
		const auto glyph_id{static_cast<const Glyph_ID>(argument.int_value())};
		return make_unique<Glyph_Object>(font_object.font(), glyph_id);
	}

	auto asserted_tagged_argument(const Expression &argument) -> unique_ptr<const Expression>
	{
		if (argument.tag() == *Glyph_Object::GLYPH_TYPE_TAG)
		{
			return argument.tagged_expression().clone();
		}
		if (argument.tagged_expression().is_record())
		{
			return asserted_glyph_record_argument(argument.tagged_expression());
		}
		throw fmt::format("tagged expression {} of type {} is no record.",
		                  text::single_quoted(argument.stringify()),
		                  text::single_quoted(argument.tag().name()));
	}

	auto asserted_glyph_record_argument(const Expression &argument) -> unique_ptr<const Expression>
	{
		if (!argument.has_property(Glyph_Object::FONT_FIELD_NAME->name()))
		{
			throw fmt::format("record {} has no field {}.", text::single_quoted(argument.stringify()),
			                  text::single_quoted(Glyph_Object::FONT_FIELD_NAME->name()));
		}
		const auto &font_expr{argument.by_name(Glyph_Object::FONT_FIELD_NAME->name())};
		if (font_expr.is_nil())
		{
			throw fmt::format("record {} has no value for field {}.",
			                  text::single_quoted(argument.stringify()),
			                  text::single_quoted(Glyph_Object::FONT_FIELD_NAME->name()));
		}
		if (font_expr.tag() != *Font_Object::FONT_TYPE_TAG)
		{
			throw fmt::format("the value of field {} has to be a font, but {} is no font.",
			                  text::single_quoted(Glyph_Object::FONT_FIELD_NAME->name()),
			                  text::single_quoted(argument.stringify()));
		}
		if (!argument.has_property(Glyph_Object::GLYPH_ID_FIELD_NAME->name()))
		{
			throw fmt::format("record {} has no field {}.", text::single_quoted(argument.stringify()),
			                  text::single_quoted(Glyph_Object::GLYPH_ID_FIELD_NAME->name()));
		}
		const auto &glyph_id_expr{argument.by_name(Glyph_Object::GLYPH_ID_FIELD_NAME->name())};
		if (glyph_id_expr.is_nil())
		{
			throw fmt::format("record {} has no value for field {}.",
			                  text::single_quoted(argument.stringify()),
			                  text::single_quoted(Glyph_Object::GLYPH_ID_FIELD_NAME->name()));
		}
		if (!glyph_id_expr.is_integer())
		{
			throw fmt::format("{} is no integer number to be used as a glyph ID.",
			                  text::single_quoted(argument.stringify()));
		}
		const auto &font_object{static_cast<const Font_Object &>(font_expr.tagged_expression())};
		const auto glyph_id{static_cast<Glyph_ID>(glyph_id_expr.int_value())};
		return make_unique<Glyph_Object>(font_object.font(), glyph_id);
	}

	auto asserted_glyph_pair_argument(const Expression &argument_list) -> unique_ptr<const Expression>
	{
		const auto element_count{expr::count(argument_list)};
		if (element_count != 2)
		{
			throw fmt::format("this list {} does not fit. It is supposed to hold 2 (font and glyph ID) "
			                  "elements but actually holds {}.",
			                  text::single_quoted(argument_list.stringify()), element_count);
		}
		const auto &first_element{argument_list.head()};
		if (first_element.tag() != *Font_Object::FONT_TYPE_TAG)
		{
			throw fmt::format("the first element of the list {} has to be a font, but {} is no font.",
			                  text::single_quoted(argument_list.stringify()),
			                  text::single_quoted(first_element.stringify()));
		}
		const auto &second_element{argument_list.tail().head()};
		if (!second_element.is_integer())
		{
			throw fmt::format("the second element of the list {} has to be a glyph ID (integer), but {} is "
			                  "no integer.",
			                  text::single_quoted(argument_list.stringify()),
			                  text::single_quoted(second_element.stringify()));
		}
		const auto &font_object{static_cast<const Font_Object &>(first_element)};
		const auto glyph_id{(Glyph_ID)second_element.int_value()};
		return make_unique<Glyph_Object>(font_object.font(), glyph_id);
	}

} // namespace fonted::scripting
