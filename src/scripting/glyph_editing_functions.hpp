// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "glyph_object.hpp"
#include "presentation_model.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using appsl::Abstract_Builtin_Function;
	using appsl::Expression;
	using appsl::Stack_Frame;

	/**
	 * @brief Glyph editing functions that expect only one argument - the glyph.
	 */
	class Unary_Glyph_Editing_Function final : public Abstract_Builtin_Function
	{
	    public:
		using Transform_Function = function<void(const Glyph_Object &)>;

	    private:
		const string m_transformation_name;
		const Transform_Function m_transform;

	    public:
		Unary_Glyph_Editing_Function(const Fun_Display_Name &, const string &transformation_name,
		                             const Transform_Function &);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;
	};

	auto glyph_inverting_function(const Abstract_Builtin_Function::Fun_Display_Name &)
			-> unique_ptr<const Expression>;

	auto flip_glyph_vertically_function(const Abstract_Builtin_Function::Fun_Display_Name &)
			-> unique_ptr<const Expression>;

	auto flip_glyph_horizontally_function(const Abstract_Builtin_Function::Fun_Display_Name &)
			-> unique_ptr<const Expression>;

	auto clear_glyph_function(const Abstract_Builtin_Function::Fun_Display_Name &) -> unique_ptr<const Expression>;

	auto set_glyph_function(const Abstract_Builtin_Function::Fun_Display_Name &) -> unique_ptr<const Expression>;

	auto scroll_glyph_left_function(const Abstract_Builtin_Function::Fun_Display_Name &)
			-> unique_ptr<const Expression>;

	auto scroll_glyph_right_function(const Abstract_Builtin_Function::Fun_Display_Name &)
			-> unique_ptr<const Expression>;

	auto scroll_glyph_up_function(const Abstract_Builtin_Function::Fun_Display_Name &)
			-> unique_ptr<const Expression>;

	auto scroll_glyph_down_function(const Abstract_Builtin_Function::Fun_Display_Name &)
			-> unique_ptr<const Expression>;

} // namespace fonted::scripting
