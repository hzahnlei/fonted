// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "presentation_model.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>

namespace fonted::scripting
{

	using appsl::Expression;

	auto make_scripting_environment_headless(const Font_Model::Now_Function &) -> shared_ptr<Stack_Frame>;

	auto make_scripting_environment_gui(shared_ptr<Presentation_Model>) -> shared_ptr<Stack_Frame>;

} // namespace fonted::scripting
