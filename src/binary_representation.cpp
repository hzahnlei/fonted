// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "binary_representation.hpp"

#include "custom_assert.hpp"
#include "use_stl.hpp"
#include <fmt/core.h>

namespace fonted::binary
{

	//---- Externalize ----------------------------------------------------

	// The number of pixels is restricted to 64 in our serialization/deserialization. This is OK as our model is
	// also restricted to 64 pixels.
	using Binary_Row_Representation = std::uint64_t;

	auto row_representation(const Row_Model &row)
	{
		Binary_Row_Representation value{0U};
		row.for_each_column(
				[&](const Column_Model &column)
				{
					value <<= 1;
					value += (column.is_set()) ? 1 : 0;
				});
		return value;
	}

	using Binary_Glyph_Representation = std::vector<Binary_Row_Representation>;

	auto glyph_representation(const Glyph_Model &glyph)
	{
		Binary_Glyph_Representation values;
		glyph.for_each_row([&](const Row_Model &row) { values.emplace_back(row_representation(row)); });
		return values;
	}

	[[nodiscard]] inline auto required_bytes_per_row(const Glyph_Width &glyph_width)
	{
		return static_cast<size_t>(std::ceil(glyph_width / 8));
	}

	auto representation(const Domain_Model &font) -> Bytes
	{
		const auto bytes_per_row{required_bytes_per_row(font.font_dimensions().glyph_width)};
		Bytes font_data;
		font.for_each_glyph(
				[&](const auto, const auto &glyph)
				{
					const auto glyph_data{glyph_representation(glyph)};
					for (const auto bin_row : glyph_data)
					{
						for (auto i = 0U; i < bytes_per_row; i++)
						{
							font_data.push_back(static_cast<Byte const *const>(
									static_cast<void const *const>(&bin_row))[i]);
						}
					}
				});
		return font_data;
	}

	//---- Internalize ----------------------------------------------------

	auto font_model(const Bytes &font_data, const Glyph_Width glyph_width, const Glyph_Height glyph_height,
	                const std::optional<Glyph_Count> glyph_count, const size_t offset,
	                const Font_Model::Now_Function &now) -> std::unique_ptr<Domain_Model>
	{
		const auto bytes_per_row{required_bytes_per_row(glyph_width)};
		const auto total_actual_bytes{font_data.size()};
		const auto effective_glyph_count{
				glyph_count.value_or(total_actual_bytes / bytes_per_row / glyph_height)};

		const auto total_expected_bytes{effective_glyph_count * glyph_height * bytes_per_row};
		require(total_expected_bytes <= (total_actual_bytes - offset),
		        fmt::format("Font data is undercutting font dimensions. Expect {} bytes but are {}.",
		                    total_expected_bytes, total_actual_bytes)
		                        .c_str());
		require((total_actual_bytes - offset) <= total_expected_bytes,
		        fmt::format("Font data exceeds font dimensions. Expect {} bytes but are {}.",
		                    total_expected_bytes, total_actual_bytes)
		                        .c_str());

		const Font_Dimensions font_dimensions{glyph_width, glyph_height, effective_glyph_count};
		auto font{std::make_unique<Font_Model>(font_dimensions, now)};
		for (auto glyph{0U}; glyph < effective_glyph_count; glyph++)
		{
			for (auto row{0U}; row < glyph_height; row++)
			{
				for (auto col{glyph_width}; col > 0; col--)
				{
					const auto byte_index{(glyph * glyph_height * bytes_per_row) + row +
					                      ((glyph_width - col) / 8) + offset};
					const auto pixels{font_data[byte_index]};
					const auto bit_index{((glyph_width - col) % 8)};
					const auto bit_mask{1 << bit_index};
					const bool value{(pixels & (Byte)bit_mask) != (Byte)0U};
					font->glyph(glyph).row(row).column(col - 1).set(value);
				}
			}
		}
		return font;
	}

} // namespace fonted::binary
