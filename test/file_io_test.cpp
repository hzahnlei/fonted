// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "file_io.hpp"
#include "test_tools/mini_font.hpp"
#include <catch2/catch_test_macros.hpp>
#include <chrono>

using namespace fonted;
using namespace std::chrono;
using namespace std::chrono_literals;

auto require_is_equal_ymd(const Domain_Model::Time_Point &actual, const Domain_Model::Time_Point &expected)
{
	const std::chrono::year_month_day actual_ymd{std::chrono::floor<std::chrono::days>(actual)};
	const std::chrono::year_month_day expected_ymd{std::chrono::floor<std::chrono::days>(expected)};
	REQUIRE(static_cast<int>(actual_ymd.year()) == static_cast<int>(expected_ymd.year()));
	REQUIRE(static_cast<unsigned>(actual_ymd.month()) == static_cast<unsigned>(expected_ymd.month()));
	REQUIRE(static_cast<unsigned>(actual_ymd.day()) == static_cast<unsigned>(expected_ymd.day()));
}

SCENARIO("Save font in native file format and load back", "[fast]")
{
	GIVEN("an 8x8 font with 2 glyphs")
	{
		const auto creation_timestamp{std::chrono::system_clock::now()};
		const auto update_timestamp{creation_timestamp + 1s};
		const auto now_fun{[&]() { return update_timestamp + 1s; }};
		const auto revision{2U};
		const auto font{minifont::instance(now_fun, creation_timestamp, update_timestamp, revision)};
		WHEN("font is saved in native file format")
		{
			native::store(font, "/tmp/test-font.yaml");
			THEN("file can be loaded back")
			{
				const auto loaded_font{native::load("/tmp/test-font.yaml", now_fun)};
				REQUIRE(loaded_font->font_dimensions().glyph_count == 2);
				REQUIRE(loaded_font->font_dimensions().glyph_height == 8);
				REQUIRE(loaded_font->font_dimensions().glyph_width == 8);

				REQUIRE(loaded_font->author_name() == "some author");
				REQUIRE(loaded_font->code_page() == "some code page");
				REQUIRE(loaded_font->description() == "some description");
				REQUIRE(loaded_font->font_name() == "some font name");

				require_is_equal_ymd(loaded_font->creation_date(), creation_timestamp);
				require_is_equal_ymd(loaded_font->update_date(), update_timestamp);
				REQUIRE(loaded_font->revision_count() == revision);

				REQUIRE(loaded_font->glyph(0) == font.glyph(0));
				REQUIRE(loaded_font->glyph(1) == font.glyph(1));
			}
		}
	}
}

SCENARIO("Save font in binary file format and load back", "[fast]")
{
	GIVEN("an 8x8 font with 2 glyphs")
	{
		const auto creation_timestamp{std::chrono::system_clock::now()};
		const auto update_timestamp{creation_timestamp + 1s};
		const auto now_fun{[&]() { return update_timestamp + 1s; }};
		const auto revision{2U};
		const auto font{minifont::instance(now_fun, creation_timestamp, update_timestamp, revision)};
		WHEN("font is saved in binary file format")
		{
			binary::store(font, "/tmp/test-font.bin");
			THEN("file can be loaded back (but meta is lost)")
			{
				const auto loaded_font{binary::load(
						"/tmp/test-font.bin", font.font_dimensions().glyph_width,
						font.font_dimensions().glyph_height, std::nullopt, 0, now_fun)};
				REQUIRE(loaded_font->font_dimensions().glyph_count == 2);
				REQUIRE(loaded_font->font_dimensions().glyph_height == 8);
				REQUIRE(loaded_font->font_dimensions().glyph_width == 8);

				REQUIRE(loaded_font->author_name() == "");
				REQUIRE(loaded_font->code_page() == "");
				REQUIRE(loaded_font->description() == "");
				REQUIRE(loaded_font->font_name() == "");

				require_is_equal_ymd(loaded_font->creation_date(), creation_timestamp);
				require_is_equal_ymd(loaded_font->update_date(), creation_timestamp);
				REQUIRE(loaded_font->revision_count() == 1);
				REQUIRE(loaded_font->revision_count() != revision);

				REQUIRE(loaded_font->glyph(0) == font.glyph(0));
				REQUIRE(loaded_font->glyph(1) == font.glyph(1));
			}
		}
	}
}
