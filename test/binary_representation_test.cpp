// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "binary_representation.hpp"
#include "test_tools/mini_font.hpp"
#include <catch2/catch_test_macros.hpp>
#include <chrono>

using namespace std::chrono_literals;
using namespace fonted;
using std::chrono::system_clock;

SCENARIO("Convert a complete font to binary representation", "[fast]")
{
	GIVEN("an 8x8 font with 2 glyphs")
	{
		const auto creation_timestamp{system_clock::now()};
		const auto update_timestamp{creation_timestamp + 1h};
		const auto now_fun{[&]() { return update_timestamp; }};
		const auto revision{1U};
		const auto font{minifont::instance(now_fun, creation_timestamp, update_timestamp, revision)};
		WHEN("font is converted to binary")
		{
			const auto bin_values{binary::representation(font)};
			THEN("the binary representation will be made up of 16 bytes")
			{
				REQUIRE(bin_values.size() == 16);
			}
			AND_THEN("the bytes will be like so")
			{
				// Glyph "A"
				REQUIRE(bin_values[0] == (Byte)0b00111000);
				REQUIRE(bin_values[1] == (Byte)0b01000100);
				REQUIRE(bin_values[2] == (Byte)0b10000010);
				REQUIRE(bin_values[3] == (Byte)0b10000010);
				REQUIRE(bin_values[4] == (Byte)0b11111110);
				REQUIRE(bin_values[5] == (Byte)0b10000010);
				REQUIRE(bin_values[6] == (Byte)0b10000010);
				REQUIRE(bin_values[7] == (Byte)0b00000000);
				// Glyph "g"
				REQUIRE(bin_values[8] == (Byte)0b00000000);
				REQUIRE(bin_values[9] == (Byte)0b00000000);
				REQUIRE(bin_values[10] == (Byte)0b00111111);
				REQUIRE(bin_values[11] == (Byte)0b01000001);
				REQUIRE(bin_values[12] == (Byte)0b01000001);
				REQUIRE(bin_values[13] == (Byte)0b00111111);
				REQUIRE(bin_values[14] == (Byte)0b00000001);
				REQUIRE(bin_values[15] == (Byte)0b01111110);
			}
		}
	}
}
