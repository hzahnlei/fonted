// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#pragma once

#include "domain_model.hpp"
#include <catch2/catch_test_macros.hpp>
#include <string>

namespace minifont
{

	using fonted::Domain_Model;
	using fonted::Font_Dimensions;
	using fonted::Font_Model;
	using std::string;

	constexpr auto PIXELS_WIDTH{8U};
	constexpr auto PIXELS_HEIGHT{8U};
	constexpr auto GLYPH_COUNT{2U};
	const Font_Dimensions FONT_DIMENSIONS{PIXELS_WIDTH, PIXELS_HEIGHT, GLYPH_COUNT};

	auto instance(const Font_Model::Now_Function &, const Domain_Model::Time_Point &creation_date,
	              const Domain_Model::Time_Point &updated, const Domain_Model::Revision_Count) -> Font_Model;

	static const string FONT_AS_YAML{"\
meta:\n\
  version:\n\
    fonted: 1.0.0\n\
    file format: 1\n\
  glyph:\n\
    width: 8\n\
    height: 8\n\
    count: 8\n\
  font:\n\
    name: minifont\n\
    author: Holger Zahnleiter\n\
    code page: freeform\n\
    description: some small font for testing\n\
    date of creation: 2023-11-01T09:46:27.873+0000\n\
    last updated at: 2023-11-02T09:49:12.325+0000\n\
    revision count: 2\n\
font data:\n\
  - glyph: |\n\
      ___@____\n\
      __@_@___\n\
      _@___@__\n\
      _@___@__\n\
      _@@@@@__\n\
      _@___@__\n\
      _@___@__\n\
      ________\n\
  - glyph: |\n\
      _@@@@___\n\
      _@___@__\n\
      _@___@__\n\
      _@@@@___\n\
      _@___@__\n\
      _@___@__\n\
      _@@@@___\n\
      ________\n\
  - glyph: |\n\
      __@@@___\n\
      _@___@__\n\
      _@______\n\
      _@______\n\
      _@______\n\
      _@___@__\n\
      __@@@___\n\
      ________\n\
  - glyph: |\n\
      _@@@@___\n\
      _@___@__\n\
      _@___@__\n\
      _@___@__\n\
      _@___@__\n\
      _@___@__\n\
      _@@@@___\n\
      ________\n\
  - glyph: |\n\
      _@@@@@__\n\
      _@______\n\
      _@______\n\
      _@@@@___\n\
      _@______\n\
      _@______\n\
      _@@@@@__\n\
      ________\n\
  - glyph: |\n\
      _@@@@@__\n\
      _@______\n\
      _@______\n\
      _@@@@___\n\
      _@______\n\
      _@______\n\
      _@______\n\
      ________\n\
  - glyph: |\n\
      __@@@___\n\
      _@___@__\n\
      _@______\n\
      _@_@@@__\n\
      _@___@__\n\
      _@___@__\n\
      __@@@___\n\
      ________\n\
  - glyph: |\n\
      _@___@__\n\
      _@___@__\n\
      _@___@__\n\
      _@@@@@__\n\
      _@___@__\n\
      _@___@__\n\
      _@___@__\n\
      ________\n\
\n\
"};

} // namespace minifont
