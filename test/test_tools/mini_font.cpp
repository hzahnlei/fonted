// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "mini_font.hpp"

namespace minifont
{

	auto instance(const Font_Model::Now_Function &now, const Domain_Model::Time_Point &creation_date,
	              const Domain_Model::Time_Point &updated, const Domain_Model::Revision_Count revision)
			-> Font_Model
	{
		Font_Model font{FONT_DIMENSIONS, now, creation_date, updated, revision};

		auto &glyph_A{font.glyph(0U)};
		glyph_A.row(0).column(2).set();
		glyph_A.row(0).column(3).set();
		glyph_A.row(0).column(4).set();
		glyph_A.row(1).column(1).set();
		glyph_A.row(1).column(5).set();
		glyph_A.row(2).column(0).set();
		glyph_A.row(2).column(6).set();
		glyph_A.row(3).column(0).set();
		glyph_A.row(3).column(6).set();
		glyph_A.row(4).column(0).set();
		glyph_A.row(4).column(1).set();
		glyph_A.row(4).column(2).set();
		glyph_A.row(4).column(3).set();
		glyph_A.row(4).column(4).set();
		glyph_A.row(4).column(5).set();
		glyph_A.row(4).column(6).set();
		glyph_A.row(5).column(0).set();
		glyph_A.row(5).column(6).set();
		glyph_A.row(6).column(0).set();
		glyph_A.row(6).column(6).set();

		auto &glyph_g{font.glyph(1U)};
		glyph_g.row(2).column(2).set();
		glyph_g.row(2).column(3).set();
		glyph_g.row(2).column(4).set();
		glyph_g.row(2).column(5).set();
		glyph_g.row(2).column(6).set();
		glyph_g.row(2).column(7).set();
		glyph_g.row(3).column(1).set();
		glyph_g.row(3).column(7).set();
		glyph_g.row(4).column(1).set();
		glyph_g.row(4).column(7).set();
		glyph_g.row(5).column(2).set();
		glyph_g.row(5).column(3).set();
		glyph_g.row(5).column(4).set();
		glyph_g.row(5).column(5).set();
		glyph_g.row(5).column(6).set();
		glyph_g.row(5).column(7).set();
		glyph_g.row(6).column(7).set();
		glyph_g.row(7).column(1).set();
		glyph_g.row(7).column(2).set();
		glyph_g.row(7).column(3).set();
		glyph_g.row(7).column(4).set();
		glyph_g.row(7).column(5).set();
		glyph_g.row(7).column(6).set();

		font.set_author_name("some author");
		font.set_code_page("some code page");
		font.set_description("some description");
		font.set_font_name("some font name");

		return font;
	}

} // namespace minifont
