// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "presentation_model.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace fonted;

SCENARIO("Deriving binary file name from native font file name", "[fast]")
{

	GIVEN("valid font file name with .fonted extension")
	{
		const auto font_file_name{"my_font.fonted"};
		WHEN("file name for binary file is derived from it")
		{
			const auto actual{binary_file_name(font_file_name)};
			THEN("file name is taken over")
			{
				AND_THEN("extendion .fonted is replaced by .bin") { REQUIRE(actual == "my_font.bin"); }
			}
		}
	}

	GIVEN("valid font file name with some other extension")
	{
		const auto font_file_name{"my_font.fonted.some_other_extension"};
		WHEN("file name for binary file is derived from it")
		{
			const auto actual{binary_file_name(font_file_name)};
			THEN("file name is taken over including file name extension")
			{
				AND_THEN("extension .bin is added to the name")
				{
					REQUIRE(actual == "my_font.fonted.some_other_extension.bin");
				}
			}
		}
	}

	GIVEN("invalid font file name")
	{
		const auto font_file_name{"my_font."};
		WHEN("file name for binary file is derived from it")
		{
			const auto actual{binary_file_name(font_file_name)};
			THEN("that file name might be incalid too") { REQUIRE(actual == "my_font..bin"); }
		}
	}
}
