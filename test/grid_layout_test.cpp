// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "grid_layout.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace fonted;

static const ImGuiStyle DEFAULT_IMGUI_STYLE{};

class Mock_Widget final : public Widget
{
    private:
	ID m_id;

    public:
	explicit Mock_Widget(const ID &id) : Widget{}, m_id{id} {}

	[[nodiscard]] auto id() const noexcept -> const ID & override { return m_id; }
	[[nodiscard]] auto widget_size() const -> Size override { return Size{16, 12}; }
	auto operator()() -> void override
	{
		// Intentionally left blank
	}

    protected:
	auto style() const noexcept -> const ImGuiStyle & override { return DEFAULT_IMGUI_STYLE; }
};

auto f() -> const ImGuiStyle & { return DEFAULT_IMGUI_STYLE; }

SCENARIO("Adding widgets to a grid layout ", "[fast]")
{
	//
	//  ( ) (*) ( )
	//  (*) ( ) (*)
	//  ( ) (*) ( )
	//
	GIVEN("a blank grid layout")
	{
		Grid_Layout layout{"some id", f};
		WHEN("we add widgets arranged in a cross patterns (likw the scroll controls)")
		{
			layout.add(0, 1, std::make_unique<Mock_Widget>("0,1"));
			layout.add(1, 0, std::make_unique<Mock_Widget>("1,0"));
			layout.add(1, 2, std::make_unique<Mock_Widget>("1,2"));
			layout.add(2, 1, std::make_unique<Mock_Widget>("2,1"));
			THEN("the widgets should actually be arranged in the desired pattern")
			{
				REQUIRE(layout.child(0, 0) == nullptr);
				REQUIRE(layout.child(0, 1)->id() == "0,1");
				REQUIRE(layout.child(0, 2) == nullptr);

				REQUIRE(layout.child(1, 0)->id() == "1,0");
				REQUIRE(layout.child(1, 1) == nullptr);
				REQUIRE(layout.child(1, 2)->id() == "1,2");

				REQUIRE(layout.child(2, 0) == nullptr);
				REQUIRE(layout.child(2, 1)->id() == "2,1");
				REQUIRE(layout.child(2, 2) == nullptr);

				REQUIRE(layout.effective_column_count() == 3);
				REQUIRE(layout.effective_row_count() == 3);

				REQUIRE(layout.widget_size().x == 80.0f);
				REQUIRE(layout.widget_size().y == 68.0f);
			}
		}
	}
}
