// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "domain_model.hpp"
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

using namespace fonted;

TEST_CASE("Cannot construct font model with invalid dimensions", "[fast]")
{
	REQUIRE_THROWS_WITH(Font_Dimensions(0, 1, 1), "Expect min glyph width of 1 pixel but was 0.");
	REQUIRE_THROWS_WITH(Font_Dimensions(65, 1, 1), "Expect max glyph width of 64 pixels but was 65.");
	REQUIRE_THROWS_WITH(Font_Dimensions(1, 0, 1), "Expect min glyph height of 1 pixel but was 0.");
	REQUIRE_THROWS_WITH(Font_Dimensions(1, 65, 1), "Expect max glyph height of 64 pixels but was 65.");
	REQUIRE_THROWS_WITH(Font_Dimensions(1, 1, 0), "Expect min glyph count of 1 but was 0.");
	REQUIRE_THROWS_WITH(Font_Dimensions(1, 1, 65537), "Expect max glyph count of 65536 but was 65537.");
}
