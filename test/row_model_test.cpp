// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "domain_model.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace fonted;

TEST_CASE("Column of constant row can be inspected (read only, no mutation)", "[fast]")
{
	const Row_Model row(1);
	REQUIRE_FALSE(row.column(0).is_set());
}

SCENARIO("Scrolling rows left/right", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("A certain bit pattern 1100")
		{
			row.column(0).set();
			row.column(1).set();
			row.column(2).clear();
			row.column(3).clear();
			WHEN("the row is scolled to the left")
			{
				row.scroll_left();
				THEN("the patterns should be scrolled left 1001")
				{
					CHECK(row.column(0).is_set());
					CHECK_FALSE(row.column(1).is_set());
					CHECK_FALSE(row.column(2).is_set());
					CHECK(row.column(3).is_set());
				}
			}
		}
		GIVEN("A certain bit pattern 1001")
		{
			row.column(0).set();
			row.column(1).clear();
			row.column(2).clear();
			row.column(3).set();
			WHEN("the row is scolled to the right")
			{
				row.scroll_right();
				THEN("the patterns should be scrolled right 1100")
				{
					CHECK(row.column(0).is_set());
					CHECK(row.column(1).is_set());
					CHECK_FALSE(row.column(2).is_set());
					CHECK_FALSE(row.column(3).is_set());
				}
			}
		}
	}
}

SCENARIO("Inverting a row of pixels", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("A certain bit pattern 1100")
		{
			row.column(0).set();
			row.column(1).set();
			row.column(2).clear();
			row.column(3).clear();
			WHEN("the row is inverted")
			{
				row.invert();
				THEN("the row shows the inverted pattern 0011")
				{
					CHECK_FALSE(row.column(0).is_set());
					CHECK_FALSE(row.column(1).is_set());
					CHECK(row.column(2).is_set());
					CHECK(row.column(3).is_set());
				}
			}
		}
	}
}

SCENARIO("Bijectivity of inverting a row of pixels", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("A certain bit pattern 1010")
		{
			row.column(0).set();
			row.column(1).clear();
			row.column(2).set();
			row.column(3).clear();
			WHEN("the row is inverted twice")
			{
				row.invert();
				row.invert();
				THEN("the original pixel pattern has been restored")
				{
					CHECK(row.column(0).is_set());
					CHECK_FALSE(row.column(1).is_set());
					CHECK(row.column(2).is_set());
					CHECK_FALSE(row.column(3).is_set());
				}
			}
			WHEN("the row is inverted a third time")
			{
				row.invert();
				THEN("the row is finally inverted")
				{
					CHECK_FALSE(row.column(0).is_set());
					CHECK(row.column(1).is_set());
					CHECK_FALSE(row.column(2).is_set());
					CHECK(row.column(3).is_set());
				}
			}
		}
	}
}

SCENARIO("Clearing all pixels of a row", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("Some pixels set 0101")
		{
			row.column(0).set(false);
			row.column(1).set(true);
			row.column(2).set(false);
			row.column(3).set(true);
			WHEN("all pixles are cleared")
			{
				row.clear();
				THEN("all pixels should be set to zero")
				{
					CHECK_FALSE(row.column(0).is_set());
					CHECK_FALSE(row.column(1).is_set());
					CHECK_FALSE(row.column(2).is_set());
					CHECK_FALSE(row.column(3).is_set());
				}
			}
		}
	}
}

SCENARIO("Idempotency of clearing all pixels of a row", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("Some pixels set 0101")
		{
			row.column(0).set(false);
			row.column(1).set(true);
			row.column(2).set(false);
			row.column(3).set(true);
			WHEN("all pixles are cleared twice")
			{
				row.clear();
				row.clear();
				THEN("all pixels should be set to zero")
				{
					CHECK_FALSE(row.column(0).is_set());
					CHECK_FALSE(row.column(1).is_set());
					CHECK_FALSE(row.column(2).is_set());
					CHECK_FALSE(row.column(3).is_set());
				}
			}
		}
	}
}

SCENARIO("Setting all pixels of a row", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("Some pixels cleared 0101")
		{
			row.column(0).set(false);
			row.column(1).set(true);
			row.column(2).set(false);
			row.column(3).set(true);
			WHEN("all pixles are set")
			{
				row.set();
				THEN("all pixels should be set to one")
				{
					CHECK(row.column(0).is_set());
					CHECK(row.column(1).is_set());
					CHECK(row.column(2).is_set());
					CHECK(row.column(3).is_set());
				}
			}
		}
	}
}

SCENARIO("Idempotency of setting all pixels of a row", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("Some pixels cleared 0101")
		{
			row.column(0).set(false);
			row.column(1).set(true);
			row.column(2).set(false);
			row.column(3).set(true);
			WHEN("all pixles are set twice")
			{
				row.set();
				row.set();
				THEN("all pixels should be set to one")
				{
					CHECK(row.column(0).is_set());
					CHECK(row.column(1).is_set());
					CHECK(row.column(2).is_set());
					CHECK(row.column(3).is_set());
				}
			}
		}
	}
}

SCENARIO("Flipping pixels of a row also works for an even number of bits", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("Some pixel pattern 1101")
		{
			row.column(0).set(true);
			row.column(1).set(true);
			row.column(2).set(false);
			row.column(3).set(true);
			WHEN("pixels are flipped")
			{
				row.flip();
				THEN("the pixel pattern has been flipped 1011")
				{
					CHECK(row.column(0).is_set());
					CHECK_FALSE(row.column(1).is_set());
					CHECK(row.column(2).is_set());
					CHECK(row.column(3).is_set());
				}
			}
		}
	}
}

SCENARIO("Flipping pixels of a row also works for a odd number of bits", "[fast]")
{
	GIVEN("A row of 5 pixels")
	{
		Row_Model row(5);
		GIVEN("Some pixel pattern 11010")
		{
			row.column(0).set(true);
			row.column(1).set(true);
			row.column(2).set(false);
			row.column(3).set(true);
			row.column(4).set(false);
			WHEN("pixels are flipped")
			{
				row.flip();
				THEN("the pixel pattern has been flipped 01011")
				{
					CHECK_FALSE(row.column(0).is_set());
					CHECK(row.column(1).is_set());
					CHECK(row.column(3).is_set());
					CHECK(row.column(4).is_set());
				}
				THEN("especially the pixel in the middle stays unchanged")
				{
					CHECK_FALSE(row.column(2).is_set());
				}
			}
		}
	}
}

SCENARIO("Bijectivity of flipping pixels of a row", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("Some pixel pattern 1101")
		{
			row.column(0).set(true);
			row.column(1).set(true);
			row.column(2).set(false);
			row.column(3).set(true);
			WHEN("pixels are flipped twice")
			{
				row.flip();
				row.flip();
				THEN("the original pixel pattern has been restored")
				{
					CHECK(row.column(0).is_set());
					CHECK(row.column(1).is_set());
					CHECK_FALSE(row.column(2).is_set());
					CHECK(row.column(3).is_set());
				}
			}
		}
	}
}

SCENARIO("Exchanging pixels of a row", "[fast]")
{
	GIVEN("A row of 4 pixels")
	{
		Row_Model row(4);
		GIVEN("Some pixel pattern 0101")
		{
			row.column(0).set(false);
			row.column(1).set(true);
			row.column(2).set(false);
			row.column(3).set(true);
			WHEN("pixels 0 and 1 are exchanged")
			{
				const auto old_val_of_pixel_0{row.column(0).is_set()};
				const auto old_val_of_pixel_1{row.column(1).is_set()};
				row.exchange_columns(0, 1);
				THEN("pixel 0 should have the old value of pixel 1")
				{
					CHECK(row.column(0).is_set() == old_val_of_pixel_1);
				}
				THEN("pixel 1 should have the old value of pixel 0")
				{
					CHECK(row.column(1).is_set() == old_val_of_pixel_0);
				}
				THEN("all other pixels stay unchanged")
				{
					CHECK_FALSE(row.column(2).is_set());
					CHECK(row.column(3).is_set());
				}
			}
		}
	}
}
