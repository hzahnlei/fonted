// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "domain_model.hpp"
#include "scripting/scripting.hpp"
#include "use_stl.hpp"
#include <appsl/appsl.hpp>
#include <catch2/catch_test_macros.hpp>
#include <fmt/core.h>

using namespace fonted;
using namespace appsl;

// Forward declarations
auto set_all_pixels(Presentation_Model &, const Glyph_ID) -> void;
auto all_pixels_set(Presentation_Model &, const Glyph_ID) -> bool;
auto no_pixels_set(Presentation_Model &, const Glyph_ID) -> bool;

static const Font_Dimensions FONT_DIMENSIONS_DO_NOT_MATTER{};
static constexpr auto FONT_FILE_NAME_DOES_NOT_MATTER{""};

//---- Tests ------------------------------------------------------------------

TEST_CASE("copy_glyph! function accepts glyphs described by records", "[fast]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	const auto source_glyph_id{0};
	const auto target_glyph_id{1};
	set_all_pixels(*font, source_glyph_id);
	REQUIRE(all_pixels_set(*font, source_glyph_id));
	REQUIRE(no_pixels_set(*font, target_glyph_id));
	// ---- WHEN ----
	Scanner scanner{fmt::format("(copy_glyph! from: (font: _font id: {})"
	                            "             to: (font: _font id: {}) )",
	                            source_glyph_id, target_glyph_id)};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "(tagged (id: 1) with: Fonted_Glyph)");
	CHECK_FALSE(parser.has_more_to_parse());
	CHECK(all_pixels_set(*font, source_glyph_id));
}

TEST_CASE("copy_glyph! function accepts glyphs described by lists", "[fast]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	const auto source_glyph_id{10};
	const auto target_glyph_id{12};
	set_all_pixels(*font, source_glyph_id);
	REQUIRE(all_pixels_set(*font, source_glyph_id));
	REQUIRE(no_pixels_set(*font, target_glyph_id));
	// ---- WHEN ----
	Scanner scanner{fmt::format("(copy_glyph! from: [_font {}]" // shorter than record syntax
	                            "             to: _font, {} )", // even shorter
	                            source_glyph_id, target_glyph_id)};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "(tagged (id: 12) with: Fonted_Glyph)");
	CHECK_FALSE(parser.has_more_to_parse());
	CHECK(all_pixels_set(*font, target_glyph_id));
}

TEST_CASE("copy_glyph! function accepts glyphs described by index into implied font", "[fast]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	const auto source_glyph_id{48};
	const auto target_glyph_id{64};
	set_all_pixels(*font, source_glyph_id);
	REQUIRE(all_pixels_set(*font, source_glyph_id));
	REQUIRE(no_pixels_set(*font, target_glyph_id));
	// ---- WHEN ----
	Scanner scanner{fmt::format("(copy_glyph! from: (font: _font id: {})"
	                            "             to: {} )", // Here we imply _font
	                            source_glyph_id, target_glyph_id)};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "(tagged (id: 64) with: Fonted_Glyph)");
	CHECK_FALSE(parser.has_more_to_parse());
	CHECK(all_pixels_set(*font, target_glyph_id));
}

TEST_CASE("Copy whole range of glyphs within one font", "[fast]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	// ---- WHEN ----
	// Same as: (range 0 9) | (map (fun i (copy_glyph! [_font (+ i 48)] (+ i 64))))
	Scanner scanner{"(map (range 0 9) (fun i (copy_glyph! [_font (+ i 48)] (+ i 64))))"};
	// TODO use (enumerated)
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "(tagged (id: 64) with: Fonted_Glyph)\n"
	                             "(tagged (id: 65) with: Fonted_Glyph)\n"
	                             "(tagged (id: 66) with: Fonted_Glyph)\n"
	                             "(tagged (id: 67) with: Fonted_Glyph)\n"
	                             "(tagged (id: 68) with: Fonted_Glyph)\n"
	                             "(tagged (id: 69) with: Fonted_Glyph)\n"
	                             "(tagged (id: 70) with: Fonted_Glyph)\n"
	                             "(tagged (id: 71) with: Fonted_Glyph)\n"
	                             "(tagged (id: 72) with: Fonted_Glyph)\n"
	                             "(tagged (id: 73) with: Fonted_Glyph)");
}

TEST_CASE("invert_glyph! function can be applied to list of glyphs", "[fast]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	REQUIRE(no_pixels_set(*font, 47));
	REQUIRE(no_pixels_set(*font, 48));
	REQUIRE(no_pixels_set(*font, 49));
	REQUIRE(no_pixels_set(*font, 50));
	REQUIRE(no_pixels_set(*font, 51));
	REQUIRE(no_pixels_set(*font, 52));
	REQUIRE(no_pixels_set(*font, 53));
	REQUIRE(no_pixels_set(*font, 54));
	REQUIRE(no_pixels_set(*font, 55));
	REQUIRE(no_pixels_set(*font, 56));
	REQUIRE(no_pixels_set(*font, 57));
	REQUIRE(no_pixels_set(*font, 58));
	// ---- WHEN ----
	Scanner scanner{"_font | (filter ascii_num?) | (map invert_glyph!)"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "(tagged (id: 48) with: Fonted_Glyph)\n"
	                             "(tagged (id: 49) with: Fonted_Glyph)\n"
	                             "(tagged (id: 50) with: Fonted_Glyph)\n"
	                             "(tagged (id: 51) with: Fonted_Glyph)\n"
	                             "(tagged (id: 52) with: Fonted_Glyph)\n"
	                             "(tagged (id: 53) with: Fonted_Glyph)\n"
	                             "(tagged (id: 54) with: Fonted_Glyph)\n"
	                             "(tagged (id: 55) with: Fonted_Glyph)\n"
	                             "(tagged (id: 56) with: Fonted_Glyph)\n"
	                             "(tagged (id: 57) with: Fonted_Glyph)");
	CHECK_FALSE(parser.has_more_to_parse());
	CHECK(no_pixels_set(*font, 47));
	CHECK(all_pixels_set(*font, 48));
	CHECK(all_pixels_set(*font, 49));
	CHECK(all_pixels_set(*font, 50));
	CHECK(all_pixels_set(*font, 51));
	CHECK(all_pixels_set(*font, 52));
	CHECK(all_pixels_set(*font, 53));
	CHECK(all_pixels_set(*font, 54));
	CHECK(all_pixels_set(*font, 55));
	CHECK(all_pixels_set(*font, 56));
	CHECK(all_pixels_set(*font, 57));
	CHECK(no_pixels_set(*font, 58));
}

TEST_CASE("glyph? function accepts only glyphs", "[fast]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	// ---- WHEN ----
	Scanner scanner{" _font | (filter ascii_num?) | (map glyph?)"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "true\n"
	                             "true\n"
	                             "true\n"
	                             "true\n"
	                             "true\n"
	                             "true\n"
	                             "true\n"
	                             "true\n"
	                             "true\n"
	                             "true");
	CHECK_FALSE(parser.has_more_to_parse());
}

TEST_CASE("glyph? function accepts only glyphs, no integers", "[fast]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	// ---- WHEN ----
	Scanner scanner{" [1 2 3] | (map glyph?)"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "nil\n"
	                             "nil\n"
	                             "nil");
	CHECK_FALSE(parser.has_more_to_parse());
}

TEST_CASE("range function accepts wide variety of inputs (plain integers)", "[fast]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	// ---- WHEN ----
	Scanner scanner{"(glyph_in_range? 3 1 10)"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "true");
	CHECK_FALSE(parser.has_more_to_parse());
}

TEST_CASE("range function accepts wide variety of inputs (glyph objects)", "[fast]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	// ---- WHEN ----
	Scanner scanner{"_font | (filter (fun glyph (glyph_in_range? glyph 1 10)))"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "(tagged (id: 1) with: Fonted_Glyph)\n"
	                             "(tagged (id: 2) with: Fonted_Glyph)\n"
	                             "(tagged (id: 3) with: Fonted_Glyph)\n"
	                             "(tagged (id: 4) with: Fonted_Glyph)\n"
	                             "(tagged (id: 5) with: Fonted_Glyph)\n"
	                             "(tagged (id: 6) with: Fonted_Glyph)\n"
	                             "(tagged (id: 7) with: Fonted_Glyph)\n"
	                             "(tagged (id: 8) with: Fonted_Glyph)\n"
	                             "(tagged (id: 9) with: Fonted_Glyph)\n"
	                             "(tagged (id: 10) with: Fonted_Glyph)");
	CHECK_FALSE(parser.has_more_to_parse());
}

TEST_CASE("A bug in app/SL prevented this to be executed due to name clash", "[fast, bug]")
{
	// ---- GIVEN ----
	auto font{make_shared<Delegating_Presentation_Model>(
			1, make_unique<Font_Model>(FONT_DIMENSIONS_DO_NOT_MATTER, system_clock::now),
			FONT_FILE_NAME_DOES_NOT_MATTER, system_clock::now)};
	auto scripting_env{scripting::make_scripting_environment_gui(font)};
	Scanner scanner{R"( (def! COPY (fun [name src_font]                                                      )"
	                R"(                 (do (def! dest_font (new_font glyph_count: 256                       )"
	                R"(                                               glyph_width: 8 glyph_height: 16        )"
	                R"(                                               name: name author: "Holger Zahnleiter" )"
	                R"(                                               codepage: "ASCII" description: name )) )"
	                R"(                     (map (range FIRST_ASCII_NUM LAST_ASCII_NUM)                      )"
	                R"(                          (fun glyph (copy_glyph! from: [src_font glyph]              )"
	                R"(                                                  to: [dest_font glyph] )) ) ) ))     )"
	                R"( (COPY "My new font" _font)                                                           )"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	// The definition of COPY
	parser.parse()->evaluate(*scripting_env);
	REQUIRE(parser.has_more_to_parse());
	// ---- WHEN ----
	// The application of COPY
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*scripting_env)};
	// ---- THEN ----
	CHECK(result->stringify() == "(tagged (id: 48) with: Fonted_Glyph)\n"
	                             "(tagged (id: 49) with: Fonted_Glyph)\n"
	                             "(tagged (id: 50) with: Fonted_Glyph)\n"
	                             "(tagged (id: 51) with: Fonted_Glyph)\n"
	                             "(tagged (id: 52) with: Fonted_Glyph)\n"
	                             "(tagged (id: 53) with: Fonted_Glyph)\n"
	                             "(tagged (id: 54) with: Fonted_Glyph)\n"
	                             "(tagged (id: 55) with: Fonted_Glyph)\n"
	                             "(tagged (id: 56) with: Fonted_Glyph)\n"
	                             "(tagged (id: 57) with: Fonted_Glyph)");
	CHECK_FALSE(parser.has_more_to_parse());
}

//---- Helper functions -------------------------------------------------------

auto set_all_pixels(Presentation_Model &font, const Glyph_ID glyph) -> void
{
	font.set_active_glyph(glyph); // Pick a glyph to be the active one.
	font.set_active_glyph();      // On the active glyph set all pixels.
}

auto all_pixels_set(Presentation_Model &font, const Glyph_ID glyph) -> bool
{
	font.set_active_glyph(glyph);
	for (auto row{0U}; row < font.font_dimensions().glyph_height; row++)
	{
		for (auto column{0U}; column < font.font_dimensions().glyph_width; column++)
		{
			if (!font.is_set_on_active_glyph(Pixel_Coordinate{.row = row, .column = column}))
			{
				return false;
			}
		}
	}
	return true;
}

auto no_pixels_set(Presentation_Model &font, const Glyph_ID glyph) -> bool
{
	font.set_active_glyph(glyph);
	for (auto row{0U}; row < font.font_dimensions().glyph_height; row++)
	{
		for (auto column{0U}; column < font.font_dimensions().glyph_width; column++)
		{
			if (font.is_set_on_active_glyph(Pixel_Coordinate{.row = row, .column = column}))
			{
				return false;
			}
		}
	}
	return true;
}
