// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "domain_model.hpp"
#include "use_stl.hpp"
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

using namespace fonted;
using namespace std::chrono_literals;

SCENARIO("Default font model", "[fast]")
{
	GIVEN("font model constructed without specific parameters")
	{
		const Font_Dimensions font_spec{};
		const Font_Model model{font_spec};
		THEN("all properties should be set to default values")
		{
			REQUIRE(model.font_dimensions().glyph_width == 8);
			REQUIRE(model.font_dimensions().glyph_height == 16);
			REQUIRE(model.font_dimensions().glyph_count == 256);
		}
	}
}

TEST_CASE("Font can be marked as 'has been updated'", "[fast]")
{
	// GIVEN font model
	//       default dimensions
	//       specific timestamps and revision
	const Font_Dimensions font_spec{};
	const auto creation_timestamp{std::chrono::system_clock::now()};
	const auto update_timestamp{creation_timestamp + 1h};
	const auto revision{1U};
	Font_Model font{font_spec, [&]() { return update_timestamp; }, creation_timestamp, creation_timestamp,
	                revision};
	CHECK(font.creation_date() == creation_timestamp);
	CHECK(font.update_date() == creation_timestamp);
	CHECK(font.revision_count() == 1);
	// WHEN marking the font as "has been updated"
	font.set_has_been_updated();
	// THEN update timespamp and revision should have been increased
	CHECK(font.creation_date() == creation_timestamp);
	CHECK(font.update_date() == update_timestamp);
	CHECK(font.revision_count() == 2);
}

TEST_CASE("Non-const glyphs within mutable font can be picked randomly", "[fast]")
{
	// GIVEN font model with N glyphs
	const auto glyph_width{8U};
	const auto glyph_height{8U};
	const auto glyph_count{128U};
	const Font_Dimensions font_spec{glyph_width, glyph_height, glyph_count};
	const auto timestamp{std::chrono::system_clock::now()};
	Font_Model font{font_spec, [&]() { return timestamp; }};
	// WHEN picking a glyph within the font
	// THEN no exception occurrs
	CHECK_NOTHROW(font.glyph(100));
}

TEST_CASE("Cannot pick non-const glyphs outside mutable font", "[fast]")
{
	// GIVEN font model with N glyphs
	const auto glyph_width{8U};
	const auto glyph_height{8U};
	const auto glyph_count{128U};
	const Font_Dimensions font_spec{glyph_width, glyph_height, glyph_count};
	const auto timestamp{std::chrono::system_clock::now()};
	Font_Model font{font_spec, [&]() { return timestamp; }};
	// WHEN picking a glyph outside the font
	// THEN no exception occurrs
	CHECK_THROWS_WITH(font.glyph(glyph_count + 1), "Glyph index out of bounds 129≥128.");
}

TEST_CASE("Const glyphs within immutable font can be picked randomly", "[fast]")
{
	// GIVEN font model with N glyphs
	const auto glyph_width{8U};
	const auto glyph_height{8U};
	const auto glyph_count{128U};
	const Font_Dimensions font_spec{glyph_width, glyph_height, glyph_count};
	const auto timestamp{std::chrono::system_clock::now()};
	const Font_Model font{font_spec, [&]() { return timestamp; }};
	// WHEN picking a glyph within the font
	// THEN no exception occurrs
	CHECK_NOTHROW(font.glyph(100));
}

TEST_CASE("Cannot pick const glyphs outside immutable font", "[fast]")
{
	// GIVEN font model with N glyphs
	const auto glyph_width{8U};
	const auto glyph_height{8U};
	const auto glyph_count{128U};
	const Font_Dimensions font_spec{glyph_width, glyph_height, glyph_count};
	const auto timestamp{std::chrono::system_clock::now()};
	const Font_Model font{font_spec, [&]() { return timestamp; }};
	// WHEN picking a glyph outside the font
	// THEN no exception occurrs
	CHECK_THROWS_WITH(font.glyph(glyph_count + 1), "Glyph index out of bounds 129≥128.");
}
