// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "domain_model.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace fonted;

TEST_CASE("A pixel is cleard at construction", "[fast]")
{
	Column_Model model;
	REQUIRE_FALSE(model.is_set());
}

SCENARIO("A pixel can be toggled", "[fast]")
{
	GIVEN("columm model with pixel not set")
	{
		Column_Model model;
		model.clear();
		REQUIRE_FALSE(model.is_set());
		WHEN("pixel is toggled")
		{
			model.toggle();
			THEN("pixel is set") { REQUIRE(model.is_set()); }
		}
	}
	GIVEN("columm model with pixel set")
	{
		Column_Model model;
		model.set();
		REQUIRE(model.is_set());
		WHEN("pixel is toggled")
		{
			model.toggle();
			THEN("pixel is cleard") { REQUIRE_FALSE(model.is_set()); }
		}
	}
}
