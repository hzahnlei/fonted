// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "domain_model.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace fonted;

TEST_CASE("Row of constant glyph can be inspected (read only, no mutation)", "[fast]")
{
	const Glyph_Model glyph(7, 12);
	REQUIRE(glyph.row(0).column_count() == 12);
}

// X--
// -X-
// --X
auto glpyh_3x3_with_diagonal_pattern()
{
	Glyph_Model glyph(3, 3);
	glyph.row(0).column(0).set();
	glyph.row(1).column(1).set();
	glyph.row(2).column(2).set();
	return glyph;
}

TEST_CASE("Scroll glyph upwards", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// --X
	auto glyph{glpyh_3x3_with_diagonal_pattern()};
	// WHEN
	glyph.scroll_up();
	// THEN
	// -X-
	// --X
	// X--
	CHECK_FALSE(glyph.row(0).column(0).is_set());
	CHECK(glyph.row(0).column(1).is_set());
	CHECK_FALSE(glyph.row(0).column(2).is_set());

	CHECK_FALSE(glyph.row(1).column(0).is_set());
	CHECK_FALSE(glyph.row(1).column(1).is_set());
	CHECK(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK_FALSE(glyph.row(2).column(1).is_set());
	CHECK_FALSE(glyph.row(2).column(2).is_set());
}

TEST_CASE("Scroll glyph to the left", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// --X
	auto glyph{glpyh_3x3_with_diagonal_pattern()};
	// WHEN
	glyph.scroll_left();
	// THEN
	// --X
	// X--
	// -X-
	CHECK_FALSE(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK(glyph.row(0).column(2).is_set());

	CHECK(glyph.row(1).column(0).is_set());
	CHECK_FALSE(glyph.row(1).column(1).is_set());
	CHECK_FALSE(glyph.row(1).column(2).is_set());

	CHECK_FALSE(glyph.row(2).column(0).is_set());
	CHECK(glyph.row(2).column(1).is_set());
	CHECK_FALSE(glyph.row(2).column(2).is_set());
}

TEST_CASE("Scroll glyph to the right", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// --X
	auto glyph{glpyh_3x3_with_diagonal_pattern()};
	// WHEN
	glyph.scroll_right();
	// THEN
	// -X-
	// --X
	// X--
	CHECK_FALSE(glyph.row(0).column(0).is_set());
	CHECK(glyph.row(0).column(1).is_set());
	CHECK_FALSE(glyph.row(0).column(2).is_set());

	CHECK_FALSE(glyph.row(1).column(0).is_set());
	CHECK_FALSE(glyph.row(1).column(1).is_set());
	CHECK(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK_FALSE(glyph.row(2).column(1).is_set());
	CHECK_FALSE(glyph.row(2).column(2).is_set());
}

TEST_CASE("Scroll glyph downwards", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// --X
	auto glyph{glpyh_3x3_with_diagonal_pattern()};
	// WHEN
	glyph.scroll_down();
	// THEN
	// --X
	// X--
	// -X-
	CHECK_FALSE(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK(glyph.row(0).column(2).is_set());

	CHECK(glyph.row(1).column(0).is_set());
	CHECK_FALSE(glyph.row(1).column(1).is_set());
	CHECK_FALSE(glyph.row(1).column(2).is_set());

	CHECK_FALSE(glyph.row(2).column(0).is_set());
	CHECK(glyph.row(2).column(1).is_set());
	CHECK_FALSE(glyph.row(2).column(2).is_set());
}

TEST_CASE("Invert glyph", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// --X
	auto glyph{glpyh_3x3_with_diagonal_pattern()};
	// WHEN
	glyph.invert();
	// THEN
	// -XX
	// X-X
	// XX-
	CHECK_FALSE(glyph.row(0).column(0).is_set());
	CHECK(glyph.row(0).column(1).is_set());
	CHECK(glyph.row(0).column(2).is_set());

	CHECK(glyph.row(1).column(0).is_set());
	CHECK_FALSE(glyph.row(1).column(1).is_set());
	CHECK(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK(glyph.row(2).column(1).is_set());
	CHECK_FALSE(glyph.row(2).column(2).is_set());
}

TEST_CASE("Bijectivity of inverting glyph", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// --X
	auto glyph{glpyh_3x3_with_diagonal_pattern()};
	// WHEN
	glyph.invert();
	glyph.invert();
	// THEN
	// X--
	// -X-
	// --X
	CHECK(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK_FALSE(glyph.row(0).column(2).is_set());

	CHECK_FALSE(glyph.row(1).column(0).is_set());
	CHECK(glyph.row(1).column(1).is_set());
	CHECK_FALSE(glyph.row(1).column(2).is_set());

	CHECK_FALSE(glyph.row(2).column(0).is_set());
	CHECK_FALSE(glyph.row(2).column(1).is_set());
	CHECK(glyph.row(2).column(2).is_set());
	// WHEN
	glyph.invert();
	// THEN
	// -XX
	// X-X
	// XX-
	CHECK_FALSE(glyph.row(0).column(0).is_set());
	CHECK(glyph.row(0).column(1).is_set());
	CHECK(glyph.row(0).column(2).is_set());

	CHECK(glyph.row(1).column(0).is_set());
	CHECK_FALSE(glyph.row(1).column(1).is_set());
	CHECK(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK(glyph.row(2).column(1).is_set());
	CHECK_FALSE(glyph.row(2).column(2).is_set());
}

// --X
// -X-
// X--
auto glpyh_3x3_with_counter_diagonal_pattern()
{
	Glyph_Model glyph(3, 3);
	glyph.row(0).column(2).set();
	glyph.row(1).column(1).set();
	glyph.row(2).column(0).set();
	return glyph;
}

TEST_CASE("Copy glyph", "[fast]")
{
	GIVEN("glyphs A and B")
	{
		auto glyph_a{glpyh_3x3_with_diagonal_pattern()};
		auto glyph_b{glpyh_3x3_with_counter_diagonal_pattern()};
		WHEN("copying A to B")
		{
			glyph_b.copy(glyph_a);
			THEN("B is equal to A") { CHECK(glyph_a == glyph_b); }
			THEN("A stays unchanged") { CHECK(glyph_a == glpyh_3x3_with_diagonal_pattern()); }
		}
	}
}

// X-X
// -X-
// X-X
auto glpyh_3x3_with_some_pattern()
{
	Glyph_Model glyph(3, 3);
	glyph.row(0).column(0).set();
	glyph.row(0).column(2).set();
	glyph.row(1).column(1).set();
	glyph.row(2).column(0).set();
	glyph.row(2).column(2).set();
	return glyph;
}

TEST_CASE("Clearing all pixels of a glyph", "[fast]")
{
	GIVEN("a 3×3 glyph with some pattern")
	{
		auto glyph{glpyh_3x3_with_some_pattern()};
		WHEN("clearing all pixels")
		{
			glyph.clear();
			THEN("all pixels are cleared")
			{
				CHECK_FALSE(glyph.row(0).column(0).is_set());
				CHECK_FALSE(glyph.row(0).column(1).is_set());
				CHECK_FALSE(glyph.row(0).column(2).is_set());

				CHECK_FALSE(glyph.row(1).column(0).is_set());
				CHECK_FALSE(glyph.row(1).column(1).is_set());
				CHECK_FALSE(glyph.row(1).column(2).is_set());

				CHECK_FALSE(glyph.row(2).column(0).is_set());
				CHECK_FALSE(glyph.row(2).column(1).is_set());
				CHECK_FALSE(glyph.row(2).column(2).is_set());
			}
		}
	}
}

TEST_CASE("Setting all pixels of a glyph", "[fast]")
{
	GIVEN("a 3×3 glyph with some pattern")
	{
		auto glyph{glpyh_3x3_with_some_pattern()};
		WHEN("setting all pixels")
		{
			glyph.set();
			THEN("all pixels are set")
			{
				CHECK(glyph.row(0).column(0).is_set());
				CHECK(glyph.row(0).column(1).is_set());
				CHECK(glyph.row(0).column(2).is_set());

				CHECK(glyph.row(1).column(0).is_set());
				CHECK(glyph.row(1).column(1).is_set());
				CHECK(glyph.row(1).column(2).is_set());

				CHECK(glyph.row(2).column(0).is_set());
				CHECK(glyph.row(2).column(1).is_set());
				CHECK(glyph.row(2).column(2).is_set());
			}
		}
	}
}

// X--
// -X-
// X-X
auto glpyh_3x3_with_asymetric_pattern()
{
	Glyph_Model glyph(3, 3);
	glyph.row(0).column(0).set();
	glyph.row(1).column(1).set();
	glyph.row(2).column(0).set();
	glyph.row(2).column(2).set();
	return glyph;
}

TEST_CASE("Flipping glyph horizontally", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// X-X
	auto glyph{glpyh_3x3_with_asymetric_pattern()};
	// WHEN
	glyph.flip_horizontally();
	// THEN
	// X-X
	// -X-
	// X--
	CHECK(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK(glyph.row(0).column(2).is_set());

	CHECK_FALSE(glyph.row(1).column(0).is_set());
	CHECK(glyph.row(1).column(1).is_set());
	CHECK_FALSE(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK_FALSE(glyph.row(2).column(1).is_set());
	CHECK_FALSE(glyph.row(2).column(2).is_set());
}

TEST_CASE("Flipping glyph horizontally is bijective", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// X-X
	auto glyph{glpyh_3x3_with_asymetric_pattern()};
	// WHEN
	glyph.flip_horizontally();
	glyph.flip_horizontally();
	// THEN
	// X--
	// -X-
	// X-X
	CHECK(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK_FALSE(glyph.row(0).column(2).is_set());

	CHECK_FALSE(glyph.row(1).column(0).is_set());
	CHECK(glyph.row(1).column(1).is_set());
	CHECK_FALSE(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK_FALSE(glyph.row(2).column(1).is_set());
	CHECK(glyph.row(2).column(2).is_set());
	// WHEN
	glyph.flip_horizontally();
	// THEN
	// X-X
	// -X-
	// X--
	CHECK(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK(glyph.row(0).column(2).is_set());

	CHECK_FALSE(glyph.row(1).column(0).is_set());
	CHECK(glyph.row(1).column(1).is_set());
	CHECK_FALSE(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK_FALSE(glyph.row(2).column(1).is_set());
	CHECK_FALSE(glyph.row(2).column(2).is_set());
}

TEST_CASE("Flipping glyph vertically", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// X-X
	auto glyph{glpyh_3x3_with_asymetric_pattern()};
	// WHEN
	glyph.flip_vertically();
	// THEN
	// --X
	// -X-
	// X-X
	CHECK_FALSE(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK(glyph.row(0).column(2).is_set());

	CHECK_FALSE(glyph.row(1).column(0).is_set());
	CHECK(glyph.row(1).column(1).is_set());
	CHECK_FALSE(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK_FALSE(glyph.row(2).column(1).is_set());
	CHECK(glyph.row(2).column(2).is_set());
}

TEST_CASE("Flipping glyph vertically is bijective", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// X-X
	auto glyph{glpyh_3x3_with_asymetric_pattern()};
	// WHEN
	glyph.flip_vertically();
	glyph.flip_vertically();
	// THEN
	// X--
	// -X-
	// X-X
	CHECK(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK_FALSE(glyph.row(0).column(2).is_set());

	CHECK_FALSE(glyph.row(1).column(0).is_set());
	CHECK(glyph.row(1).column(1).is_set());
	CHECK_FALSE(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK_FALSE(glyph.row(2).column(1).is_set());
	CHECK(glyph.row(2).column(2).is_set());
	// WHEN
	glyph.flip_vertically();
	// THEN
	// --X
	// -X-
	// X-X
	CHECK_FALSE(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK(glyph.row(0).column(2).is_set());

	CHECK_FALSE(glyph.row(1).column(0).is_set());
	CHECK(glyph.row(1).column(1).is_set());
	CHECK_FALSE(glyph.row(1).column(2).is_set());

	CHECK(glyph.row(2).column(0).is_set());
	CHECK_FALSE(glyph.row(2).column(1).is_set());
	CHECK(glyph.row(2).column(2).is_set());
}

TEST_CASE("Exchanging complete rows of a glyph", "[fast]")
{
	// GIVEN
	// X--
	// -X-
	// X-X
	auto glyph{glpyh_3x3_with_asymetric_pattern()};
	// WHEN
	glyph.exchange_rows(1, 2);
	// THEN
	// X--
	// X-X
	// -X-
	CHECK(glyph.row(0).column(0).is_set());
	CHECK_FALSE(glyph.row(0).column(1).is_set());
	CHECK_FALSE(glyph.row(0).column(2).is_set());

	CHECK(glyph.row(1).column(0).is_set());
	CHECK_FALSE(glyph.row(1).column(1).is_set());
	CHECK(glyph.row(1).column(2).is_set());

	CHECK_FALSE(glyph.row(2).column(0).is_set());
	CHECK(glyph.row(2).column(1).is_set());
	CHECK_FALSE(glyph.row(2).column(2).is_set());
}
