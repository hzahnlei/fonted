// ****************************************************************************
//
//     ////////  |  fonted - Bitmap Font Editor
//    //         |
//   //////      |  Create your own bitmap fonts for vintage computers.
//  //           |
// //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "native_representation.hpp"
#include "test_tools/mini_font.hpp"
#include <catch2/catch_test_macros.hpp>
#include <yaml-cpp/yaml.h>

using namespace fonted;

SCENARIO("Internalizing fonted's native YAML format.", "[fast]")
{
	GIVEN("a valid font in fonted's native YAML representation")
	{
		const auto font_as_yaml{YAML::Load(minifont::FONT_AS_YAML)};
		WHEN("parsing that YAML")
		{
			const auto font{native::font_model(font_as_yaml)}; // A 64 bit value
			THEN("the resulting font model should reflect the information from that YAML")
			{
				REQUIRE(font->author_name() == "Holger Zahnleiter");
				REQUIRE(font->code_page() == "freeform");
				const std::chrono::year_month_day created_ymd{
						std::chrono::floor<std::chrono::days>(font->creation_date())};
				REQUIRE(static_cast<int>(created_ymd.year()) == 2023);
				REQUIRE(static_cast<unsigned>(created_ymd.month()) == 11);
				REQUIRE(static_cast<unsigned>(created_ymd.day()) == 1);
				// It is 2023 and no simple C++ way to check hh:mm:ss :-(
				REQUIRE(font->description() == "some small font for testing");
				REQUIRE(font->font_name() == "minifont");
				REQUIRE(font->font_dimensions().glyph_count == 8);
				REQUIRE(font->font_dimensions().glyph_height == 8);
				REQUIRE(font->font_dimensions().glyph_width == 8);
				REQUIRE(font->revision_count() == 2);
				const std::chrono::year_month_day updates_ymd{
						std::chrono::floor<std::chrono::days>(font->update_date())};
				REQUIRE(static_cast<int>(updates_ymd.year()) == 2023);
				REQUIRE(static_cast<unsigned>(updates_ymd.month()) == 11);
				REQUIRE(static_cast<unsigned>(updates_ymd.day()) == 2);

				// Checking some random glyphs
				REQUIRE_FALSE(font->glyph(1).row(3).column(0).is_set());
				REQUIRE(font->glyph(1).row(3).column(1).is_set());
				REQUIRE(font->glyph(1).row(3).column(2).is_set());
				REQUIRE(font->glyph(1).row(3).column(3).is_set());
				REQUIRE(font->glyph(1).row(3).column(4).is_set());
				REQUIRE_FALSE(font->glyph(1).row(3).column(5).is_set());
				REQUIRE_FALSE(font->glyph(1).row(3).column(6).is_set());
				REQUIRE_FALSE(font->glyph(1).row(3).column(7).is_set());
			}
		}
	}
}
