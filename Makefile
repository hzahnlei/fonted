# *****************************************************************************
#
#     ////////  |  fonted - Bitmap Font Editor
#    //         |
#   //////      |  Create your own bitmap fonts for vintage computers.
#  //           |
# //            |  (c) 2023, 2024, Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

.PHONY: package dependencies code clean build release-build test run run-release coverage

# Version is set here manually. In the future, the version should be taken from
# a Git tag when building and packaging the application (automatically in CI).
export VERSION_MAJOR:=1
export VERSION_MINOR:=2
export VERSION_PATCH:=1
export VERSION_EXTENSION:=
export FULLY_QUALIFIED_VERSION:=${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}
export FILE_FORMAT_VERSION:=1

# I am defining dependencies as an environment variables, so that I can share a
# single point of truth between conanfile.py and packaging/conanfile.py.
export SOURCE_DEPENDENCIES:=( \
        "imgui/1.89.8", \
        "glfw/3.3.8", \
        "glew/2.2.0", \
        "fmt/10.1.1", \
        "yaml-cpp/0.8.0", \
        "yacl/1.8.0", \
	"appsl/1.5.0", \
)
export TEST_DEPENDENCIES:=("catch2/3.4.0")

# -----------------------------------------------------------------------------
# Build and test the Conan package
# -----------------------------------------------------------------------------
PACKAGE_BUILD_DIR:=build_package

package:
	rm -drf ${PACKAGE_BUILD_DIR}
	mkdir -p ${PACKAGE_BUILD_DIR}
	cp -r src ${PACKAGE_BUILD_DIR}
	cp -r models ${PACKAGE_BUILD_DIR}
	cp -r templates ${PACKAGE_BUILD_DIR}
	# cp behaves differently on Linux and BSD!!!
	# See https://dev.to/ackshaey/macos-vs-linux-the-cp-command-will-trip-you-up-2p00
	cp -r packaging/. ${PACKAGE_BUILD_DIR}/.
	conan create ${PACKAGE_BUILD_DIR} --build=missing \
	                                  --settings compiler.cppstd=gnu20

# -----------------------------------------------------------------------------
# Download and build dependencies, if neccessary.
# -----------------------------------------------------------------------------
dependencies:
	conan install . --build=missing --settings build_type=Debug \
	                                --settings compiler.cppstd=gnu20
	
# -----------------------------------------------------------------------------
# Pick the debug preset and start VS Code.  Code will be configured using CMake
# and Conan dependencies implicitely. Test can be run from within Code. Same is
# true for debugging sessions.
# -----------------------------------------------------------------------------
code: dependencies
	cmake --preset conan-debug 
	code .

# -----------------------------------------------------------------------------
# Delete the build folder for a fresh and clean build.
# -----------------------------------------------------------------------------
clean:
	rm -drf build
	rm -drf generated
	rm -f CMakeUserPresets.json
	rm -drf documentation
	rm -drf bindings

# -----------------------------------------------------------------------------
# Build on command line. For example for automated CI builds.  We are using the
# debug preset because we want a coverage report.
# -----------------------------------------------------------------------------
build:
	cmake --preset conan-debug
	cmake --build --preset conan-debug

# No debug symbols, production version, for use, not for development.
release-build:
	conan install . --build=missing --settings build_type=Release \
	                                --settings compiler.cppstd=gnu20
	cmake --preset conan-release
	cmake --build --preset conan-release

# -----------------------------------------------------------------------------
# Run from command line. For example for automated CI builds.  We are using the
# debug preset because we want a coverage report.
# -----------------------------------------------------------------------------
test:
	ctest --preset conan-debug --output-on-failure

# -----------------------------------------------------------------------------
# Run from command line. For example for automated CI builds.  We are using the
# debug preset because we want a coverage report.
# -----------------------------------------------------------------------------
run:
	build/Debug/src/fonted

run-release: release-build
	build/Release/src/fonted

# Export 4 fonts to one binary file so that they can be transferred to an EPROM
# (character ROM).
rom:
	build/Debug/src/fonted export --format binary \
	                              --source fonts/holgers_serif_cp850_08x16.fonted \
				      --destination fonts/1.bin
	build/Debug/src/fonted export --format binary \
	                              --source fonts/holgers_sans_serif_cp850_08x16.fonted \
				      --destination fonts/2.bin
	build/Debug/src/fonted export --format binary \
	                              --source fonts/holgers_computer_cp850_08x16.fonted \
				      --destination fonts/3.bin
	build/Debug/src/fonted export --format binary \
	                              --source fonts/holgers_template_cp850_08x16.fonted \
				      --destination fonts/4.bin
	cat fonts/1.bin fonts/2.bin fonts/3.bin fonts/4.bin > fonts/all.bin

# -----------------------------------------------------------------------------
# Determin code coverage. Uses the Makefile generated by CMake.
# -----------------------------------------------------------------------------
coverage:
	cmake --preset conan-debug
	cd build/Debug && make coverage

# -----------------------------------------------------------------------------
# Generate source documentation. Assuming local Doxygen installation.
# -----------------------------------------------------------------------------
docs:
	doxygen Doxyfile


# -----------------------------------------------------------------------------
# Manually build and push container image with openGL included.
# -----------------------------------------------------------------------------
fedora:
	docker login registry.gitlab.com
	docker build -f Dockerfile.fedora \
	             -t registry.gitlab.com/hzahnlei/fonted/fedora38-opengl:1.0.0 .
	docker push registry.gitlab.com/hzahnlei/fonted/fedora38-opengl:1.0.0


# -----------------------------------------------------------------------------
# Launch a (local) container with the source code mapped as a folder. This sup-
# ports  compiling and  debugging  the code on another  OS (Linux) and compiler
# (GCC) than available on my development machine (macOS/Clang).
# -----------------------------------------------------------------------------

# Run this to start a Linux/GCC development environment.
container:
	docker run --volume "$(shell pwd):/fonted" \
	           --interactive --tty \
	           registry.gitlab.com/hzahnlei/fonted/fedora38-opengl:1.0.0 \
		   bash

# Run this to install dependencies on the Linux/GCC development environment. CD
# into fonted and run make prepare-container.
prepare-container:
	pushd / && \
	git clone https://gitlab.com/hzahnlei/cpp-junk-box.git && \
	pushd cpp-junk-box && \
	git checkout 1.7.3 && \
	make package && \
	popd && \
	git clone https://gitlab.com/hzahnlei/cpp-yacl.git && \
	pushd cpp-yacl && \
	git checkout 1.8.0 && \
	make package && \
	popd && \
	git clone https://gitlab.com/hzahnlei/appsl.git && \
	pushd appsl && \
	git checkout 1.1.0 && \
	make package && \
	popd
